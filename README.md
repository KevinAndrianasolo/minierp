# MiniERP (Gestion Mr Tovo)

## Fonctionnalités : 

### Gestion d'achat 
* Liste Et Création de fournisseurs
* Liste des documents d'achats
* Nouveau document d'achat : Bon de commande, Bon de réception

### Gestion de vente 
* Liste Et Création de Client
* Liste des documents de vente
* Nouveau document de vente : Proformat, Bon de livraison, Facture, Facture d'avoir, Facture de retour

### GEstion des Articles
* Liste et Créations des articles : Choix du type de Gestion de stock à utiliser par article (CUMP, LIFO, FIFO)

### Gestion de stock
* Entrée de Stock
* Sortie de Stock
* Etat de Stock (Liste des articles : Qté, Valeur en stock, PU => Détails des mouvements de stock)
