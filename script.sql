CREATE DATABASE minierp;

USE minierp;

CREATE TABLE TVA (
    idTVA INT AUTO_INCREMENT NOT NULL,
    Value NUMERIC(6, 3) NOT NULL,
    LastUpdate DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL,
    PRIMARY KEY (idTVA)
);

CREATE TABLE TypeMouvement (
    idTypeMouvement INT AUTO_INCREMENT NOT NULL,
    TypeMouvement VARCHAR(50) NOT NULL,
    PRIMARY KEY (idTypeMouvement)
);

CREATE TABLE SaleStatus (
    idSaleStatus INT AUTO_INCREMENT NOT NULL,
    Status VARCHAR(25) NOT NULL,
    PRIMARY KEY (idSaleStatus)
);

CREATE TABLE PurchaseStatus (
    idPurchaseStatus INT AUTO_INCREMENT NOT NULL,
    Status VARCHAR(25) NOT NULL,
    PRIMARY KEY (idPurchaseStatus)
);

CREATE TABLE Sale (
    idSale INT AUTO_INCREMENT NOT NULL,
    idSaleStatus INT DEFAULT 1,
    SaleDocument DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL,
    Reference VARCHAR(10) NOT NULL,
    SaleDate DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL,
    PRIMARY KEY (idSale)
);

CREATE TABLE Service (
    idService INT AUTO_INCREMENT NOT NULL,
    ServiceName VARCHAR(100) NOT NULL,
    PRIMARY KEY (idService)
);

CREATE TABLE Purchase (
    idPurchase INT AUTO_INCREMENT NOT NULL,
    idPurchaseStatus INT DEFAULT 1,
    idService INT NOT NULL,
    Reference VARCHAR(10) NOT NULL,
    PurchaseDate DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL,
    PRIMARY KEY (idPurchase)
);

CREATE TABLE PurchaseDocumentType (
    idPurchaseDocumentType INT AUTO_INCREMENT NOT NULL,
    Prefix VARCHAR(3) NOT NULL,
    Name VARCHAR(100) NOT NULL,
    PRIMARY KEY (idPurchaseDocumentType)
);

CREATE TABLE SalesDocumentType (
    idSalesDocumentType INT AUTO_INCREMENT NOT NULL,
    Prefix VARCHAR(3) NOT NULL,
    Name VARCHAR(100) NOT NULL,
    PRIMARY KEY (idSalesDocumentType)
);

CREATE TABLE StockManagementMethod (
    idStockManagementMethod INT AUTO_INCREMENT NOT NULL,
    Name VARCHAR(25) NOT NULL,
    PRIMARY KEY (idStockManagementMethod)
);

CREATE TABLE Item (
    idItem INT AUTO_INCREMENT NOT NULL,
    Reference VARCHAR(10) NOT NULL,
    Designation VARCHAR(250) NOT NULL,
    PRIMARY KEY (idItem)
);

CREATE TABLE MouvementStock (
    idMouvementStock INT AUTO_INCREMENT NOT NULL,
    idTypeMouvement INT NOT NULL,
    idItem INT NOT NULL,
    Quantity NUMERIC(10, 3) NOT NULL,
    UnitPrice NUMERIC(10, 3),
    DateMouvement DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL,
    PRIMARY KEY (idMouvementStock)
);

CREATE TABLE SaleItem (
    idSaleItem INT AUTO_INCREMENT NOT NULL,
    idSale INT NOT NULL,
    idItem INT NOT NULL,
    Quantity NUMERIC(10, 3) NOT NULL,
    PRIMARY KEY (idSaleItem)
);

CREATE TABLE PurchaseItem (
    idPurchaseItem INT AUTO_INCREMENT NOT NULL,
    idPurchase INT NOT NULL,
    idItem INT NOT NULL,
    Quantity NUMERIC(10, 3) NOT NULL,
    PRIMARY KEY (idPurchaseItem)
);

CREATE TABLE Customer (
    idCustomer INT AUTO_INCREMENT NOT NULL,
    Reference VARCHAR(10) NOT NULL,
    Name VARCHAR(100) NOT NULL,
    Address VARCHAR(250) NOT NULL,
    Phone VARCHAR(250) NOT NULL,
    Email VARCHAR(250) NOT NULL,
    LastUpdate DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL,
    PRIMARY KEY (idCustomer)
);

CREATE TABLE Unit (
    idUnit INT AUTO_INCREMENT NOT NULL,
    Unit VARCHAR(100) NOT NULL,
    PRIMARY KEY (idUnit)
);

CREATE TABLE Provider (
    idProvider INT AUTO_INCREMENT NOT NULL,
    Reference VARCHAR(10) NOT NULL,
    Name VARCHAR(100) NOT NULL,
    Address VARCHAR(250) NOT NULL,
    Phone VARCHAR(250) NOT NULL,
    LeaderName VARCHAR(250) NOT NULL,
    Email VARCHAR(250) NOT NULL,
    Description VARCHAR(250),
    LastUpdate DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL,
    PRIMARY KEY (idProvider)
);

CREATE TABLE EntrepriseInformation (
    idEntrepriseInformation INT AUTO_INCREMENT NOT NULL,
    EntrepriseName VARCHAR(100) NOT NULL,
    EntrepriseAcronym VARCHAR(25) NOT NULL,
    Address VARCHAR(250) NOT NULL,
    Phone VARCHAR(250) NOT NULL,
    LogoPath VARCHAR(250) NOT NULL,
    NIF VARCHAR(250),
    STAT VARCHAR(250),
    CIF VARCHAR(250),
    RCS VARCHAR (250),
    LastUpdate DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL,
    PRIMARY KEY (idEntrepriseInformation)
);

CREATE TABLE Currency (
    idCurrency INT AUTO_INCREMENT NOT NULL,
    Currency VARCHAR(50) NOT NULL,
    Symbol VARCHAR(25) NOT NULL,
    CodeISO VARCHAR(3) NOT NULL,
    PRIMARY KEY (idCurrency)
);

CREATE TABLE PurchaseDocument (
    idPurchaseDocument INT AUTO_INCREMENT NOT NULL,
    idPurchase INT,
    idPurchaseDocumentType INT NOT NULL,
    idProvider INT NOT NULL,
    idCurrency INT NOT NULL,
    Reference VARCHAR(10) NOT NULL,
    GlobalDiscount NUMERIC(5, 3) NOT NULL,
    DocumentDate DATE NOT NULL,
    isRejected BOOLEAN DEFAULT FALSE NOT NULL,
    LastUpdate DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL,
    PRIMARY KEY (idPurchaseDocument)
);

CREATE TABLE PurchaseDocumentItem (
    idPurchaseDocumentItem INT AUTO_INCREMENT NOT NULL,
    idPurchaseDocument INT NOT NULL,
    idUnit INT NOT NULL,
    Quantity NUMERIC(25, 3) NOT NULL,
    UnitPrice NUMERIC(50, 3) NOT NULL,
    Reduction NUMERIC(5, 3) NOT NULL,
    LastUpdate DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL,
    idItem INT NOT NULL,
    PRIMARY KEY (idPurchaseDocumentItem)
);

CREATE TABLE SalesDocument (
    idSalesDocument INT AUTO_INCREMENT NOT NULL,
    idSale INT,
    idSalesDocumentType INT NOT NULL,
    idCustomer INT NOT NULL,
    idCurrency INT NOT NULL,
    Reference VARCHAR(10) NOT NULL,
    GlobalDiscount NUMERIC(5, 3) NOT NULL,
    DocumentDate DATE NOT NULL,
    isRejected BOOLEAN DEFAULT FALSE NOT NULL,
    LastUpdate DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL,
    PRIMARY KEY (idSalesDocument)
);

CREATE TABLE SalesDocumentItem (
    idSalesDocumentItem INT AUTO_INCREMENT NOT NULL,
    idSalesDocument INT NOT NULL,
    idItem INT NOT NULL,
    idUnit INT NOT NULL,
    Quantity NUMERIC(25, 3) NOT NULL,
    UnitPrice NUMERIC(50, 3) NOT NULL,
    Reduction NUMERIC(5, 3) NOT NULL,
    LastUpdate DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL,
    PRIMARY KEY (idSalesDocumentItem)
);

ALTER TABLE
    MouvementStock
ADD
    CONSTRAINT typemouvement_mouvementstock_fk FOREIGN KEY (idTypeMouvement) REFERENCES TypeMouvement (idTypeMouvement) ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE
    Sale
ADD
    CONSTRAINT salestatus_sale_fk FOREIGN KEY (idSaleStatus) REFERENCES SaleStatus (idSaleStatus) ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE
    Purchase
ADD
    CONSTRAINT purchasestatus_purchase_fk FOREIGN KEY (idPurchaseStatus) REFERENCES PurchaseStatus (idPurchaseStatus) ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE
    SaleItem
ADD
    CONSTRAINT sale_saleitem_fk FOREIGN KEY (idSale) REFERENCES Sale (idSale) ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE
    SalesDocument
ADD
    CONSTRAINT sale_salesdocument_fk FOREIGN KEY (idSale) REFERENCES Sale (idSale) ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE
    Purchase
ADD
    CONSTRAINT service_purchase_fk FOREIGN KEY (idService) REFERENCES Service (idService) ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE
    PurchaseItem
ADD
    CONSTRAINT purchase_purchaseitem_fk FOREIGN KEY (idPurchase) REFERENCES Purchase (idPurchase) ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE
    PurchaseDocument
ADD
    CONSTRAINT purchase_purchasedocument_fk FOREIGN KEY (idPurchase) REFERENCES Purchase (idPurchase) ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE
    PurchaseDocument
ADD
    CONSTRAINT purchasedocumenttype_purchasedocument_fk FOREIGN KEY (idPurchaseDocumentType) REFERENCES PurchaseDocumentType (idPurchaseDocumentType) ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE
    SalesDocument
ADD
    CONSTRAINT salesdocumenttype_salesdocument_fk FOREIGN KEY (idSalesDocumentType) REFERENCES SalesDocumentType (idSalesDocumentType) ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE
    SalesDocumentItem
ADD
    CONSTRAINT item_salesdocumentitem_fk FOREIGN KEY (idItem) REFERENCES Item (idItem) ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE
    PurchaseDocumentItem
ADD
    CONSTRAINT item_purchasedocumentitem_fk FOREIGN KEY (idItem) REFERENCES Item (idItem) ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE
    PurchaseItem
ADD
    CONSTRAINT item_purchaseitem_fk FOREIGN KEY (idItem) REFERENCES Item (idItem) ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE
    SaleItem
ADD
    CONSTRAINT item_saleitem_fk FOREIGN KEY (idItem) REFERENCES Item (idItem) ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE
    MouvementStock
ADD
    CONSTRAINT item_mouvementstock_fk FOREIGN KEY (idItem) REFERENCES Item (idItem) ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE
    SalesDocument
ADD
    CONSTRAINT customer_salesdocument_fk FOREIGN KEY (idCustomer) REFERENCES Customer (idCustomer) ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE
    SalesDocumentItem
ADD
    CONSTRAINT unit_salesdocumentitem_fk FOREIGN KEY (idUnit) REFERENCES Unit (idUnit) ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE
    PurchaseDocumentItem
ADD
    CONSTRAINT unit_purchasedocumentitem_fk FOREIGN KEY (idUnit) REFERENCES Unit (idUnit) ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE
    PurchaseDocument
ADD
    CONSTRAINT provider_purchasedocument_fk FOREIGN KEY (idProvider) REFERENCES Provider (idProvider) ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE
    SalesDocument
ADD
    CONSTRAINT currency_salesdocument_fk FOREIGN KEY (idCurrency) REFERENCES Currency (idCurrency) ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE
    PurchaseDocument
ADD
    CONSTRAINT currency_purchasedocument_fk FOREIGN KEY (idCurrency) REFERENCES Currency (idCurrency) ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE
    PurchaseDocumentItem
ADD
    CONSTRAINT purchasedocument_purchasedocumentitem_fk FOREIGN KEY (idPurchaseDocument) REFERENCES PurchaseDocument (idPurchaseDocument) ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE
    SalesDocumentItem
ADD
    CONSTRAINT salesdocument_salesdocumentitem_fk FOREIGN KEY (idSalesDocument) REFERENCES SalesDocument (idSalesDocument) ON DELETE NO ACTION ON UPDATE NO ACTION;

DELIMITER / / CREATE FUNCTION CreateReference(prefix VARCHAR(6), id INT) RETURNS VARCHAR(10) BEGIN RETURN CONCAT(prefix, RIGHT(CONCAT("000000", id), 6));

END;

/ / DELIMITER;

INSERT INTO
    EntrepriseInformation(
        EntrepriseName,
        EntrepriseAcronym,
        Address,
        Phone,
        LogoPath
    )
VALUES
    (
        "Kevin Corporation",
        "K_Corp.",
        "Ankadiefajoro Antananarivo, 103",
        "034 53 564 71",
        "assets/img/EntrepriseInformation/Logo.png"
    );

INSERT INTO
    SalesDocumentType (Prefix, Name)
VALUES
    ('PF', 'Proforma');

INSERT INTO
    SalesDocumentType (Prefix, Name)
VALUES
    ('BL', 'Bon de Livraison');

INSERT INTO
    SalesDocumentType (Prefix, Name)
VALUES
    ('FC', 'Facture');

INSERT INTO
    SalesDocumentType (Prefix, Name)
VALUES
    ('FR', 'Facture de retour');

INSERT INTO
    SalesDocumentType (Prefix, Name)
VALUES
    ('FA', 'Facture d''avoir');

INSERT INTO
    PurchaseDocumentType (Prefix, Name)
VALUES
    ('BC', 'Bon de commande');

INSERT INTO
    PurchaseDocumentType (Prefix, Name)
VALUES
    ('BR', 'Bon de réception');

INSERT INTO
    Unit(Unit)
VALUES
    ('Pièce');

INSERT INTO
    Unit(Unit)
VALUES
    ('Kg');

INSERT INTO
    Unit(Unit)
VALUES
    ('L');

INSERT INTO
    Currency(Currency, Symbol, CodeISO)
VALUES
    ('Ariary', 'Ar', 'MGA');

INSERT INTO
    Currency(Currency, Symbol, CodeISO)
VALUES
    ('Euro', 'Euro', 'EUR');

INSERT INTO
    Currency(Currency, Symbol, CodeISO)
VALUES
    ('Dollar', 'Dollar', 'DOL');

INSERT INTO
    Item(Reference, Designation)
VALUES
    ('PRD000001', 'Pneu');

INSERT INTO
    Item(Reference, Designation)
VALUES
    ('PRD000002', 'Frein');

INSERT INTO
    Item(Reference, Designation)
VALUES
    ('PRD000003', 'Chaise');

INSERT INTO
    Item(Reference, Designation)
VALUES
    ('PRD000004', 'Table');

INSERT INTO
    Customer(Reference, Name, Address, Phone, Email)
VALUES
    (
        'CST000001',
        'Kevin',
        'Antananarivo 101',
        '0345356471',
        'kevin@gmail.com'
    );

INSERT INTO
    Provider(
        Reference,
        Name,
        Address,
        Phone,
        LeaderName,
        Email,
        Description
    )
VALUES
    (
        "FRN0001",
        "Société Tama Import",
        "Anosivavaka Antananarivo, 101",
        "020 23 616 58",
        "Andrianasolo Lala Kevin",
        "TamaImport@gmail.com",
        "Provider de pièces de carosserie à Antananarivo"
    );

INSERT INTO
    Provider(
        Reference,
        Name,
        Address,
        Phone,
        LeaderName,
        Email,
        Description
    )
VALUES
    (
        "FRN0002",
        "Japan Actuel's",
        "Lot II D 2 Bis Manjakaray, 101",
        "034 02 110 88",
        "Andrianasolo Lala Sharon",
        "JapanActuel@gmail.com",
        "Provider de pièces de carosserie à Antananarivo"
    );

INSERT INTO
    Provider(
        Reference,
        Name,
        Address,
        Phone,
        LeaderName,
        Email,
        Description
    )
VALUES
    (
        "FRN0003",
        "AUTO PIECES CHAMSUL",
        "Lalana Ranaivo Jules, Antananarivo",
        "020 22 310 01",
        "Rasandison Tsiky",
        "Chamsul@gmail.com",
        "Magasin de pièces de rechange automobile à Antananarivo"
    );

INSERT INTO
    Service(ServiceName)
VALUES
    ('Service 1');

INSERT INTO
    Service(ServiceName)
VALUES
    ('Service 2');

create
or replace view salesDocumentCustType as
select
    salesDocument.*,
    customer.Name as CustomerName,
    salesdocumenttype.name as typeDocument
from
    salesDocument
    join customer on salesDocument.idCustomer = Customer.idCustomer
    join salesDocumentType on salesDocument.idSalesDocumentType = salesDocumentType.idSalesDocumentType;

INSERT INTO
    PurchaseStatus (Status)
VALUES
    ('En Cours'),
    ('Annulé'),
    ('Livré');

INSERT INTO
    SaleStatus (Status)
VALUES
    ('En Cours'),
    ('Annulé'),
    ('Facturé');

INSERT INTO
    TypeMouvement(TypeMouvement)
VALUES
    ('Entrée'),
    ('Sortie');

INSERT INTO
    MouvementStock(
        idTypeMouvement,
        idItem,
        Quantity,
        UnitPrice,
        DateMouvement
    )
VALUES
    (1, 1, 10, 20, '2021-02-13 00:00:00');

INSERT INTO
    MouvementStock(
        idTypeMouvement,
        idItem,
        Quantity,
        UnitPrice,
        DateMouvement
    )
VALUES
    (1, 1, 5, 30, '2021-02-13 00:10:00');

INSERT INTO
    MouvementStock(
        idTypeMouvement,
        idItem,
        Quantity,
        UnitPrice,
        DateMouvement
    )
VALUES
    (2, 1, 8, null, '2021-02-13 00:20:00');

INSERT INTO
    MouvementStock(
        idTypeMouvement,
        idItem,
        Quantity,
        UnitPrice,
        DateMouvement
    )
VALUES
    (2, 1, 4, null, '2021-02-13 00:30:00');

INSERT INTO
    MouvementStock(
        idTypeMouvement,
        idItem,
        Quantity,
        UnitPrice,
        DateMouvement
    )
VALUES
    (2, 1, 2, null, '2021-02-13 00:40:00');

INSERT INTO
    StockManagementMethod(Name)
VALUES
    ('FIFO'),
    ('LIFO'),
    ('CUMP');

-- View SalesDocument
create
or replace view salesDocumentCustType as
select
    salesDocument.*,
    customer.Name as CustomerName,
    salesdocumenttype.name as typeDocument,
    Sale.Reference as SaleReference
from
    salesDocument
    join customer on salesDocument.idCustomer = Customer.idCustomer
    join salesDocumentType on salesDocument.idSalesDocumentType = salesDocumentType.idSalesDocumentType
    LEFT JOIN Sale on SalesDocument.idSale = Sale.idSale;

-- View PurchaseDocument;
create
or replace view purchaseDocumentCustType as
select
    purchaseDocument.*,
    provider.Name as ProviderName,
    purchasedocumenttype.name as typeDocument,
    Purchase.Reference as PurchaseReference
from
    purchaseDocument
    join provider on purchaseDocument.idProvider = Provider.idProvider
    join purchaseDocumentType on purchaseDocument.idPurchaseDocumentType = purchaseDocumentType.idPurchaseDocumentType
    LEFT JOIN Purchase on purchaseDocument.idPurchase = Purchase.idPurchase;

create
or replace view purchaseServiceStatus as
select
    purchase.*,
    service.ServiceName,
    purchaseStatus.Status
from
    purchase
    join service on purchase.idService = Service.idService
    LEFT join purchaseStatus on purchase.idPurchaseStatus = purchaseStatus.idPurchaseStatus;

create
or replace view saleStatusStatus as
select
    sale.*,
    saleStatus.Status
from
    sale
    LEFT join saleStatus on sale.idSaleStatus = saleStatus.idSaleStatus;

INSERT INTO
    TVA (Value)
VALUES
    (20);

CREATE
OR REPLACE VIEW CurrentTVA As
SELECT
    *
FROM
    TVA
ORDER BY
    LastUPDATE DESC
limit
    1;

CREATE TABLE MariageStatus(
    IdMariageStatus INT NOT NULL AUTO_INCREMENT,
    Description VARCHAR(255) NOT NULL,
    PRIMARY KEY(IdMariageStatus)
);

INSERT INTO
    MariageStatus
VALUES
    (1, 'Célibataire');

INSERT INTO
    MariageStatus
VALUES
    (2, 'Marié/ée');

INSERT INTO
    MariageStatus
VALUES
    (3, 'Divorcé/ée');

INSERT INTO
    MariageStatus
VALUES
    (4, 'Veuf/ve');

CREATE TABLE Departement(
    IdDepartement INT NOT NULL AUTO_INCREMENT,
    Name VARCHAR(255) NOT NULL,
    PRIMARY KEY(IdDepartement)
);

INSERT INTO
    Departement
VALUES
    (1, 'Ressources Humaines');

INSERT INTO
    Departement
VALUES
    (2, 'Finance');

INSERT INTO
    Departement
VALUES
    (3, 'Commerce');

CREATE TABLE EmployeeStatus(
    IdEmployeeStatus INT NOT NULL,
    EmployeeStatus VARCHAR(255) NOT NULL,
    PRIMARY KEY(IdEmployeeStatus)
);

INSERT INTO
    EmployeeStatus
VALUES
    (1, 'Cadre supérieur');

INSERT INTO
    EmployeeStatus
VALUES
    (2, 'Autre');

CREATE TABLE Employee(
    IdEmployee INT NOT NULL AUTO_INCREMENT,
    Reference VARCHAR(10) NOT NULL,
    Name VARCHAR(255) NOT NULL,
    CIN VARCHAR(12) NOT NULL UNIQUE,
    NumCnaps VARCHAR(9) UNIQUE,
    IdMariageStatus INT NOT NULL,
    IdDepartement INT NOT NULL,
    IdEmployeeStatus INT NOT NULL,
    Fonction VARCHAR(255),
    HiringDate DATE NOT NULL,
    PRIMARY KEY(IdEmployee),
    FOREIGN KEY(IdMariageStatus) REFERENCES MariageStatus(IdMariageStatus),
    FOREIGN KEY(IdDepartement) REFERENCES Departement(IdDepartement),
    FOREIGN KEY(IdEmployeeStatus) REFERENCES EmployeeStatus(IdEmployeeStatus)
);

CREATE TABLE BasicSalary(
    IdBasicSalary INT NOT NULL AUTO_INCREMENT,
    IdEmployee INT NOT NULL,
    Salary NUMERIC(14, 2) NOT NULL,
    DateSalary DATE NOT NULL,
    PRIMARY KEY(IdBasicSalary, IdEmployee, DateSalary),
    FOREIGN KEY(IdEmployee) REFERENCES Employee(IdEmployee)
);

CREATE TABLE BonusType(
    IdBonusType INT NOT NULL AUTO_INCREMENT,
    Description VARCHAR(255) NOT NULL,
    PRIMARY KEY(IdBonusType)
);

INSERT INTO
    BonusType
VALUES
    (1, 'Prime');

INSERT INTO
    BonusType
VALUES
(2, 'Autre');

CREATE TABLE Bonus(
    IdBonus INT NOT NULL AUTO_INCREMENT,
    Reference VARCHAR(10) NOT NULL,
    IdEmployee INT NOT NULL,
    BonusType INT NOT NULL,
    CashValue NUMERIC (14, 2),
    Month INT NOT NULL,
    PRIMARY KEY(IdBonus),
    FOREIGN KEY(IdEmployee) REFERENCES Employee(IdEmployee)
);

CREATE TABLE ExtraHourValue(
    IdExtraHourValue INT NOT NULL AUTO_INCREMENT,
    Reference VARCHAR(10) NOT NULL,
    IdEmployee INT NOT NULL,
    ValuePerHour NUMERIC(16, 2) NOT NULL,
    DateValue DATE NOT NULL,
    PRIMARY KEY(IdExtraHourValue),
    FOREIGN KEY(IdEmployee) REFERENCES Employee(IdEmployee)
);

CREATE TABLE ExtraHour(
    IdExtraHour INT NOT NULL AUTO_INCREMENT,
    Reference VARCHAR(10) NOT NULL,
    IdEmployee INT NOT NULL,
    Hour NUMERIC (6, 2) NOT NULL,
    Month INT NOT NULL,
    -- 1 à 12 sinon erreur
    PRIMARY KEY(IdExtraHour),
    FOREIGN KEY(IdEmployee) REFERENCES Employee(IdEmployee)
);

CREATE TABLE SocialContribution(
    IdSocialContribution INT NOT NULL AUTO_INCREMENT,
    Reference VARCHAR(10) NOT NULL,
    ContributionType INT NOT NULL,
    -- 1 : CNAPS -- 2 : OSTIE
    Value NUMERIC(14, 2) NOT NULL,
    ValueType INT(2) NOT NULL,
    -- 1 : valeur | 2 : pourcentage
    Date DATE NOT NULL,
    PRIMARY KEY(IdSocialContribution)
);

CREATE TABLE MaximumCNAPS(
    IdMaximumCNAPS INT NOT NULL AUTO_INCREMENT,
    Reference VARCHAR(10) NOT NULL,
    Value NUMERIC(16, 2) NOT NULL,
    DateMaximumCNAPS DATE NOT NULL,
    PRIMARY KEY(IdMaximumCNAPS)
);

INSERT INTO
    MaximumCNAPS
VALUES
    (1, 'CNA0001', 1800000, '2020-01-01');

CREATE TABLE IRSA(
    IdIrsa INT NOT NULL AUTO_INCREMENT,
    Description VARCHAR(255) NOT NULL,
    ValueMin NUMERIC(14, 2) NOT NULL,
    ValueMax NUMERIC(14, 2) NOT NULL,
    Value NUMERIC(10, 2) NOT NULL,
    ValueType INT NOT NULL,
    -- 1 : valeur | 2 : pourcentage
    PRIMARY KEY(IdIrsa)
);

CREATE TABLE MinimumIRSA(
    idMinimumIRSA INT NOT NULL AUTO_INCREMENT,
    value NUMERIC(16, 2) NOT NULL,
    dateMinimumIRSA DATE NOT NULL,
    PRIMARY KEY(idMinimumIRSA)
);

INSERT INTO
    MinimumIRSA
VALUES
    (1, 2000, '2019-09-01');

INSERT INTO
    IRSA
VALUES
    (1, 0, 350000, 2000, 1);

INSERT INTO
    IRSA
VALUES
    (2, 350001, 400000, 5, 2);

INSERT INTO
    IRSA
VALUES
    (3, 400001, 500000, 10, 2);

INSERT INTO
    IRSA
VALUES
    (4, 500001, 600000, 15, 2);

INSERT INTO
    IRSA
VALUES
    (5, 600001, -1, 20, 2);

INSERT INTO
    SocialContribution
VALUES
    (1, 'SOC000001', 1, 1, 2, '2020-01-01');

INSERT INTO
    SocialContribution
VALUES
    (2, 'SOC000002', 2, 1, 2, '2020-01-01');

INSERT INTO
    Employee
VALUES
    (
        1,
        'EMP000001',
        'Employ 1',
        '101251201100',
        '122345678',
        1,
        2,
        1,
        'Fonction 1',
        '2021-01-01'
    );

INSERT INTO
    BasicSalary
VALUES
    (1, 1, 900000, '2021-01-01');

INSERT INTO
    Bonus
VALUES
    (1, 'BON000001', 1, 1, 'Prime', 50000, 1);

INSERT INTO
    Bonus
VALUES
    (2, 'BON000002', 2, 2, 'Logement', 100000, 1);

INSERT INTO
    Bonus
VALUES
    (3, 'BON000002', 2, 1, 'Prime', 100000, 1);

INSERT INTO
    ExtraHourValue
VALUES
    (1, 'EXH000001', 1, 20000, '2021-01-01');

INSERT INTO
    ExtraHour
VALUES
    (1, 'EXT000001', 1, 3, 1);

INSERT INTO
    BasicSalary
VALUES
    (2, 2, 300000, '2021-01-01');

INSERT INTO
    BasicSalary
VALUES
    (3, 3, 450000, '2021-01-01');
INSERT INTO ExtraHour VALUES (1,'EXT000001',1,3,1);


select per_item.idItem, per_item.quantity, (per_item.quantity/total.total_quantity)*100 as percent from (
    select coalesce(sum(purchasedocumentitem.quantity),0) as total_quantity from purchasedocument
        join purchasedocumentitem on purchasedocument.idpurchasedocument = purchasedocumentitem.idpurchasedocument
    where purchasedocument.idPurchasedocumentType=2 and  extract(month from purchasedocument.documentdate) = 3 and extract(year from purchasedocument.documentdate) = 2021
    ) as total
    cross join
    (select purchasedocumentitem.idItem,sum(purchasedocumentitem.quantity) as quantity from purchasedocument
        join purchasedocumentitem on purchasedocument.idpurchasedocument = purchasedocumentitem.idpurchasedocument
    where purchasedocument.idPurchasedocumentType=2 and extract(month from purchasedocument.documentdate) = 3 and extract(year from purchasedocument.documentdate) = 2021
    group by idItem) as per_item 
order by per_item.quantity desc;


select extract(year from current_timestamp);
