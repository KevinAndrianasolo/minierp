<?php
class Stock{
    public $idItem;
    public $idStockManagementMethod;
    public $Date;
    public $Reste = [];
    public $AllMouvements = [];
    public $Mouvements = [];
    public $ValeurStock = 0;
    public $QuantiteStock = 0;

    public $db;
    function __construct($idItem, $idStockManagementMethod, $Date ,$db){
        $this->idItem = $idItem;
        $this->idStockManagementMethod = $idStockManagementMethod;
        $this->Date = $Date;
        $this->db = $db;
        $this->AllMouvements = $this->GetAllMouvement();
        $this->InitMouvements();
        $this->ValeurStock = $this->GetValeurStock();
        $this->QuantiteStock = $this->GetQuantiteStock();

    }
    public function InitMouvements(){
        for($i=0;$i<count($this->AllMouvements);$i++){
            $Quantity = $this->AllMouvements[$i]['Quantity'];
            $UnitPrice = $this->AllMouvements[$i]['UnitPrice'];
            $DateMouvement = $this->AllMouvements[$i]['DateMouvement'];
            // Si Entrée :
            if($this->AllMouvements[$i]['idTypeMouvement'] == '1'){
                $this->Add($Quantity,$UnitPrice , $DateMouvement);
            }
            // Si Sortie :
            if($this->AllMouvements[$i]['idTypeMouvement'] == '2'){
                $this->Remove($Quantity , $DateMouvement);
            }
        }
    }
    function GetAllMouvement(){
        $sql = "SELECT * FROM MouvementStock WHERE idItem=%d and CAST(DateMouvement AS Date)<=CAST('%s' AS Date) order by DateMouvement asc";
        $sql = sprintf($sql, $this->idItem, $this->Date);
        return $this->db->query($sql)->result_array();
    }
    function Add($Quantity, $UnitPrice, $DateMouvement){
        if($this->idStockManagementMethod == 3){
            // SI CUMP :
            $QuantityTemp = $Quantity; // Qte Reste + $Quantite;
            $UnitPriceTemp = $UnitPrice;
            if (isset($this->Reste[0])) {
                $QuantityTemp = $this->Reste[0]['Quantity'] + $Quantity; // Quantité Total : Qté Precedente + Qté
                $UnitPriceTemp = ( ($this->Reste[0]['Quantity']*$this->Reste[0]['UnitPrice']) + ($Quantity*$UnitPrice) ) / $QuantityTemp; // Montant Total : Montant precedent + Montant Entrée / Qté Total
                
            }
            $Montant = $Quantity * $UnitPrice;
            $this->Reste[0] = ["Quantity"=>$QuantityTemp, "UnitPrice"=>$UnitPriceTemp, "DateMouvement"=>$DateMouvement];
            $this->Mouvements[] = ["Quantity"=>$Quantity, "UnitPrice"=>$UnitPrice,"Montant"=>$Montant, "DateMouvement"=>$DateMouvement];
            
        }
        else{
            $this->Reste[] = ["Quantity"=>$Quantity, "UnitPrice"=>$UnitPrice, "DateMouvement"=>$DateMouvement];
            $Montant = $Quantity * $UnitPrice;
            $this->Mouvements[] = ["Quantity"=>$Quantity, "UnitPrice"=>$UnitPrice,"Montant"=>$Montant, "DateMouvement"=>$DateMouvement];
        }
    }
    function Remove($Quantity, $DateMouvement){
        $QteAEnlever = $Quantity;
        if($this->idStockManagementMethod == 3){
            // Si CUMP:
            $Quantity = -1*$Quantity;
            $UnitPrice = $this->Reste[0]['UnitPrice'];
            $Montant = $Quantity * $UnitPrice;
            $this->Mouvements[] = ["Quantity"=>$Quantity, "UnitPrice"=>$UnitPrice,"Montant"=>$Montant, "DateMouvement"=>$DateMouvement];
            $this->Reste[0]['Quantity']+= $Quantity;
            return;
        }
        else if ($this->idStockManagementMethod == 1) $i = 0; // Si FIFO
        else if ($this->idStockManagementMethod == 2) $i = count($this->Reste) -1 ; // Si LIFO
        
        while(true){
            $UnitPrice = $this->Reste[$i]['UnitPrice'];
            if($this->Reste[$i]['Quantity']<$QteAEnlever){
                $QteAEnlever -= $this->Reste[$i]['Quantity'];
                $QuantityTemp = -1 * $this->Reste[$i]['Quantity'];
                $Montant = $QuantityTemp * $UnitPrice;
                $this->Mouvements[] = ["Quantity"=>$QuantityTemp, "UnitPrice"=>$UnitPrice,"Montant"=>$Montant, "DateMouvement"=>$DateMouvement];
                array_splice($this->Reste, $i, 1);
                if ($this->idStockManagementMethod == 1) $i = 0; // Si FIFO
                else if ($this->idStockManagementMethod == 2) $i = count($this->Reste) -1 ; // Si LIFO
            }
            else{
                $this->Reste[$i]['Quantity'] -= $QteAEnlever;
                $QuantityTemp = -1 * $QteAEnlever;
                $Montant = $QuantityTemp * $UnitPrice;
                $this->Mouvements[] = ["Quantity"=>$QuantityTemp, "UnitPrice"=>$UnitPrice,"Montant"=>$Montant, "DateMouvement"=>$DateMouvement];
                $QteAEnlever = 0;
                break;
            }
        }
    }
    function GetValeurStock(){
        $ValeurStock = 0;
        for($i=0;$i<count($this->Mouvements);$i++){
            $ValeurStock+=$this->Mouvements[$i]['Montant'];
        }
        return $ValeurStock;
    }
    function GetQuantiteStock(){
        $QuantiteStock = 0;
        for($i=0;$i<count($this->Mouvements);$i++){
            $QuantiteStock+=$this->Mouvements[$i]['Quantity'];
        }
        return $QuantiteStock;
    }
}
?>