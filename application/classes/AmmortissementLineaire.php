<?php
    require_once APPPATH.'classes/Periodicity.php';
    class AmmortissementLineaire{
        public $db;
        public $DureeUtilisation;
        public $DatePremiereUtilisation;
        public $DateAmmortissementDuBien;
        public $TauxLineaire;
        public $Periodicity; // Annuel : 1, Mensuel : 12
        public $ProrataTemporis;
        public $Interval;
        public $Dotation;
        public function __construct($db, $ValeurInitiale, $DureeUtilisation, $DatePremiereUtilisation, $Periodicity ){
            /**
             * Prenons l'exemple ,
             * Coefficient = 1.75
             * Valeur initiale = 100000
             * Duree Utilisation = 5ans
             * Date premiere utilisation = 15/02/2021
             * Periodicity = 12 car en MOIS
             * 
             * Prorata temporis(en jour) = 31 Dec N - Date premiere utilisation => 319 jours
             * Prorata temporis(en mois) = 12 - Mois(date premiere utilisation) + 1 => 12 - 2 + 1 = 11 mois
             */
            if(!isset($ValeurInitiale) || $ValeurInitiale=="") throw new Exception("Valeur initiale invalide.");
            if(!isset($DureeUtilisation) || $DureeUtilisation=="") throw new Exception("Duree utilisation invalide.");
            if(!isset($DatePremiereUtilisation) || $DatePremiereUtilisation=="") throw new Exception("Date de premiere utilisation invalide.");
            if(!isset($Periodicity) || $Periodicity=="") throw new Exception("Periodicité invalide.");

            $this->db = $db;
            $this->ValeurInitiale = $ValeurInitiale;
            $this->DatePremiereUtilisation = $DatePremiereUtilisation;
            $this->Periodicity = $Periodicity;
            $this->DureeUtilisation = $DureeUtilisation;
            $this->Init();
        }
        public function Init(){
            $this->DateAmmortissementDuBien = $this->GetDateAmmortissementDuBien();
            $this->DureeUtilisation = $this->GetDureeUtilisation($this->DureeUtilisation);
            $this->TauxLineaire = $this->GetTauxLineaire($this->DureeUtilisation);
            $this->ProrataTemporis = $this->GetProrataTemporis();
            $this->Dotation = $this->GetDotation();
        }
        public function GetDotation(){
            return $this->ValeurInitiale* ( $this->TauxLineaire / 100 );
        }
        public function GetDateAmmortissementDuBien(){
            $Interval = 'P'.$this->DureeUtilisation.'Y'; // Mbola en Année ilay duree eto
            $temp = new DateTime();
            $temp->setTimestamp($this->DatePremiereUtilisation->getTimestamp());
            return $temp->add(new DateInterval($Interval));
        }
        public function GetDureeUtilisation($dureeAnnuel){
            $duree = -1;
            $PERIODICITY=  new Periodicity();
            if($this->Periodicity == $PERIODICITY->ANNUEL){
                $duree = $dureeAnnuel;
                $this->Interval = "P1Y";
            }
            else if($this->Periodicity == $PERIODICITY->MENSUEL){
                $duree = $dureeAnnuel * 12;
                $this->Interval = "P1M";
            }
            else if($this->Periodicity ==$PERIODICITY->JOURNALIER){
                $interval = $this->DatePremiereUtilisation->diff($this->DateAmmortissementDuBien, true);
                $duree = $interval->days;
                $this->Interval = "P1D";
            }
            else{
                throw new Exception("La périodicité est invalide.");
            }
            return $duree; //  5 ans * 12 = 60 mois
        }
        public function GetTauxLineaire($Duree)  {
            return 100/$Duree; // 100/60 = 1.667
        }
        public function GetNextDate($Date){
            $temp = new DateTime();
            $temp->setTimestamp($Date->getTimestamp());
            return $temp->add(new DateInterval($this->Interval));
        }
        public function GetProrataTemporis(){
            /**
             * En jour : 
             */
           // return 365 - $this->DatePremiereUtilisation->format("d") + 1;
            $annee = $this->DatePremiereUtilisation->format("Y");
            $fin_annee_string = "%d-%d-%d";
            $fin_annee_string = sprintf($fin_annee_string, $annee, 12, 31);
            $fin_annee = new DateTime($fin_annee_string);
            $diff = $fin_annee->diff($this->DatePremiereUtilisation);
            return $diff->days + 1;
        }
        public function FormaterDate($date){
            $format = "d-m-Y";
            $PERIODICITY=  new Periodicity();
            if($this->Periodicity == $PERIODICITY->ANNUEL){
                $format = "Y";
            }
            else if($this->Periodicity == $PERIODICITY->MENSUEL){
                $format = "m-Y";
            }
            else if($this->Periodicity == $PERIODICITY->JOURNALIER){
                $format = "d-m-Y";
            }
            return $date->format($format);
        }
        public function Ammortissements(){
            $Ammortissements = [];
            /**
             * Boucler la durée d'utilisation de durée d'utilisation à 1
             */
            $Ammortissement = [];
            $Ammortissement['Date'] = $this->DatePremiereUtilisation;
            $Ammortissement['DateFormat'] = $this->FormaterDate($Ammortissement['Date']);
            $Ammortissement['Valeur'] = $this->ValeurInitiale;
            $Ammortissement['TauxLineaire'] = $this->TauxLineaire;
            $Ammortissement['AmmortissementCumuleDebut'] = 0;
            $Ammortissement['Dotation'] = $this->Dotation * ($this->ProrataTemporis / 365);
            $Ammortissement['AmmortissementCumuleFin'] = $Ammortissement['AmmortissementCumuleDebut'] + $Ammortissement['Dotation'];
            $Ammortissement['ValeurNette'] = $Ammortissement['Valeur'] - $Ammortissement['AmmortissementCumuleFin'];
            $Ammortissements[] = $Ammortissement;

            $i = 1;
            for($DureeRestant = $this->DureeUtilisation - 1; $DureeRestant > 0; $DureeRestant --){
                $Ammortissement = [];
                $Ammortissement['Date'] = $this->GetNextDate($Ammortissements[$i-1]['Date']);
                $Ammortissement['DateFormat'] = $this->FormaterDate($Ammortissement['Date']);
                $Ammortissement['Valeur'] = $this->ValeurInitiale;
                $Ammortissement['TauxLineaire'] = $this->TauxLineaire;
                 $Ammortissement['AmmortissementCumuleDebut'] =  $Ammortissements[$i-1]['AmmortissementCumuleFin'];
                $Ammortissement['Dotation'] = $this->Dotation;
                $Ammortissement['AmmortissementCumuleFin'] = $Ammortissement['AmmortissementCumuleDebut'] + $Ammortissement['Dotation'];
                $Ammortissement['ValeurNette'] = $Ammortissement['Valeur'] - $Ammortissement['AmmortissementCumuleFin'];
                $Ammortissements[] = $Ammortissement;
                $i++;
            }
            if($this->ProrataTemporis != 365){
                $Ammortissement = [];
                $Ammortissement['Date'] = $this->DateAmmortissementDuBien;
                $Ammortissement['DateFormat'] = $this->FormaterDate($Ammortissement['Date']);
                $Ammortissement['Valeur'] = $this->ValeurInitiale;
                $Ammortissement['TauxLineaire'] = $this->TauxLineaire;
                $Ammortissement['AmmortissementCumuleDebut'] = $Ammortissements[count($Ammortissements)-1]['AmmortissementCumuleFin'];
                $Ammortissement['Dotation'] = $this->Dotation - $Ammortissements[0]['Dotation'];
                $Ammortissement['AmmortissementCumuleFin'] = $Ammortissement['AmmortissementCumuleDebut'] + $Ammortissement['Dotation'];
                $Ammortissement['ValeurNette'] = abs(round($Ammortissement['Valeur'] - $Ammortissement['AmmortissementCumuleFin'], 5));
                $Ammortissements[] = $Ammortissement;
            }

            return $Ammortissements;
        }
    }
?>