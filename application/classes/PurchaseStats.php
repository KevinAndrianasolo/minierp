<?php

    class PurchaseStats{
        public $db;
        public function __construct($db){
            $this->db = $db;
        }
        public function getPurchaseStatsOf($month, $year){
            $id_default_document_type = 2; // Purchase document type => 2 (Bon de réception)
            $sql = "select per_item.idItem, per_item.Reference, per_item.Designation, per_item.Quantity, (per_item.quantity/total.total_quantity)*100 as Percent from (
                        select coalesce(sum(purchasedocumentitem.quantity),0) as total_quantity from purchasedocument
                            join purchasedocumentitem on purchasedocument.idpurchasedocument = purchasedocumentitem.idpurchasedocument
                        where purchasedocument.idPurchasedocumentType=%d and  extract(month from purchasedocument.documentdate) = %d and extract(year from purchasedocument.documentdate) = %d
                        ) as total
                        cross join
                        (select purchasedocumentitem.idItem,item.Reference, item.Designation, sum(purchasedocumentitem.quantity) as quantity from purchasedocument
                            join purchasedocumentitem on purchasedocument.idpurchasedocument = purchasedocumentitem.idpurchasedocument
                            join item on purchasedocumentitem.idItem = item.idItem
                        where purchasedocument.idPurchasedocumentType=%d and extract(month from purchasedocument.documentdate) = %d and extract(year from purchasedocument.documentdate) = %d
                        group by idItem) as per_item 
                    order by per_item.quantity desc";
            $sql = sprintf($sql, $id_default_document_type, $month, $year, $id_default_document_type, $month, $year);
            $stats = $this->db->query($sql)->result_array();

            return $stats;
        }
    }
?>