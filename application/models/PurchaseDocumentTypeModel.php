<?php
class PurchaseDocumentTypeModel extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
    public function GetById($id)
    {
        return $this->db->get_where('PurchaseDocumentType', ['idPurchaseDocumentType' => $id])->row_array();
    }
}
