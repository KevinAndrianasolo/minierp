<?php
    require_once APPPATH.'models/BaseModel.php';

    class SaleStatusModel extends BaseModel{
        public function __construct(){
            parent::__construct();
        }
        public function GetList(){
            return $this->db->get("SaleStatus")->result_array();
        }
    }
?>