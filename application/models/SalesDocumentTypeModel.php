<?php
class SalesDocumentTypeModel extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
    public function GetById($id)
    {
        return $this->db->get_where('SalesDocumentType', ['idSalesDocumentType' => $id])->row_array();
    }
}
