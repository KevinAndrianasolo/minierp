<?php
class DepartementModel extends BaseModel
{
    public function __construct()
    {
        parent::__construct();
    }
    public function GetAll()
    {
        return $this->db->get('Departement')->result_array();
    }
}
