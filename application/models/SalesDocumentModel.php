<?php
require_once APPPATH . 'models/BaseModel.php';

class SalesDocumentModel extends BaseModel
{
    public function __construct()
    {
        parent::__construct();
    }
    public function Insert($SalesDocument)
    {
        $this->db->trans_start();
        $query = "INSERT INTO SalesDocument(idSalesDocumentType,idCustomer,DocumentDate,GlobalDiscount,idCurrency,idSale) VALUES (%d,%d,CAST('%s' AS DATE),%f,%d,%s)";
        $idSale = empty($SalesDocument["idSale"]) ? "NULL" : $SalesDocument["idSale"];
        if (!isset($SalesDocument["GlobalDiscount"])) throw new Exception("La remise globale est invalide.");
        if ($SalesDocument["GlobalDiscount"] < 0) throw new Exception("La remise globale doit être positive.");
        if ($SalesDocument["GlobalDiscount"] > 100) throw new Exception("La remise globale doit être inferieure à 100.");

        $query = sprintf($query, $SalesDocument["idSalesDocumentType"], $SalesDocument["idCustomer"], $SalesDocument["DocumentDate"], $SalesDocument["GlobalDiscount"], $SalesDocument["idCurrency"], $idSale);
        $this->db->query($query);
        $idSalesDocument = $this->db->insert_id();
        $SalesDocumentType = $this->GetSalesDocumentType($SalesDocument["idSalesDocumentType"]);
        $query = "UPDATE SalesDocument SET Reference=CreateReference('%s',%d) WHERE idSalesDocument=%d";
        $query = sprintf($query, $SalesDocumentType['Prefix'], $idSalesDocument, $idSalesDocument);
        $this->db->query($query);

        for ($i = 0; $i < count($SalesDocument['Items']); $i++) {
            $this->InsertSalesDocumentItem($SalesDocument['Items'][$i], $idSalesDocument);
        }
        $this->db->trans_complete();
    }
    public function GetById($id)
    {
        return $this->db->get_where("SalesDocument", ["idSalesDocument" => $id])->row_array();
    }
    public function InsertSalesDocumentItem($Item, $idSalesDocument)
    {
        if (!isset($Item["Quantity"]) || empty($Item["Quantity"])) throw new Exception("La Quantité est invalide.");
        if (!isset($Item["UnitPrice"]) || empty($Item["UnitPrice"])) throw new Exception("Le prix unitaire est invalide.");
        if (!isset($Item["Reduction"])) throw new Exception("La reduction est invalide.");

        if ($Item["Quantity"] < 0) throw new Exception("La Quantité doit être positive.");
        if ($Item["UnitPrice"] < 0) throw new Exception("Le prix unitaire doit être positive.");
        if ($Item["Reduction"] < 0) throw new Exception("La reduction doit être positive.");

        $query = "INSERT INTO SalesDocumentItem(idSalesDocument,idItem,Quantity,idUnit,UnitPrice,Reduction) VALUES (%d,%d,%f,%d,%f,%f)";
        $query = sprintf($query, $idSalesDocument, $Item["idItem"], $Item["Quantity"], $Item["idUnit"], $Item["UnitPrice"], $Item["Reduction"]);
        $this->db->query($query);
    }
    public function CreateReference($id, $prefix)
    {
        $ref = $id;
        $taille = strlen($prefix) + strlen($id);
        $size = 10;
        for ($i = $taille; $i < $size; $i++) {
            $ref = "0" . $ref;
        }
        $ref = $prefix . $ref;
        return $ref;
    }
    public function GetSalesDocumentType($idSalesDocumentType)
    {
        return $this->db->get_where("SalesDocumentType", ["idSalesDocumentType" => $idSalesDocumentType])->row_array();
    }
    public function GetSalesDocumentTypeList()
    {
        return $this->db->get("SalesDocumentType")->result_array();
    }
    public function get_salesDocumentType()
    {
        $sql = "select * from salesdocumenttype";
        $query = $this->db->query($sql);
        $result = array();
        foreach ($query->result_array() as $row) {
            $result[] = $row;
        }
        return $result;
    }

    public function get_customer()
    {
        $sql = "select * from customer";
        $query = $this->db->query($sql);
        $result = array();
        foreach ($query->result_array() as $row) {
            $result[] = $row;
        }
        return $result;
    }

    public function get_salesDocument()
    {
        $sql = "select * from salesDocumentCustType";
        $query = $this->db->query($sql);
        $result = array();
        foreach ($query->result_array() as $row) {
            $result[] = $row;
        }
        return $result;
    }

    public function get_salesDocumentById($idSale, $idCustomer, $idSalesDocumentType, $documentDate)
    {
        $choix = array();
        $i = 0;
        if ($idSale != -1) {
            $choix[$i++] = "idSale = '$idSale'";
        }
        if ($idCustomer != -1) {
            $choix[$i++] = "idCustomer = '$idCustomer'";
        }
        if ($idSalesDocumentType != -1) {
            $choix[$i++] = "idSalesDocumentType = '$idSalesDocumentType'";
        }
        if (!empty($documentDate)) {
            $choix[$i++] = "documentDate = '$documentDate'";
        }

        if ($i > 0) {
            $critere = $choix[0] . " ";

            for ($j = 1; $j < $i; $j++) {
                $critere .= " AND " . $choix[$j] . " ";
            }
            $sql = "select * from salesDocumentCustType where $critere";
        } else {
            $sql = "select * from salesDocumentCustType";
        }
        $sql = sprintf($sql);
        $query = $this->db->query($sql);
        $result = array();
        foreach ($query->result_array() as $row) {
            $result[] = $row;
        }
        return $result;
    }
}
