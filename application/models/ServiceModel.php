<?php
    require_once APPPATH.'models/BaseModel.php';

    class ServiceModel extends BaseModel{
        public function __construct(){
            parent::__construct();
        }
        public function GetList(){
            return $this->db->get("Service")->result_array();
        }
        function insert($Name)
        {
            $data = array(
            'ServiceName' => $Name
            );
            $this->db->insert('Service', $data);
        }

        public function GetById($id)
        {
            $array = array();
            $sql = sprintf("SELECT * FROM Service WHERE idService = %d", $id);
            $query = $this->db->query($sql);
            $array = $query->row_array();
            return $array;
        }
        
        function update($Name, $id)
        {
            $data = array(
                'ServiceName' => $Name
                );
                $this->db->where('idService', $id);
             $this->db->update('Service', $data);
        }
    public function Delete($id)
    {
        $this->db->where('idService', $id);
        $this->db->delete('Service');
   
    }
    }
?>