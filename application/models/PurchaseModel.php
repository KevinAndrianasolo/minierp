<?php
    require_once APPPATH.'models/BaseModel.php';

    class PurchaseModel extends BaseModel{
        public function __construct(){
            parent::__construct();
        }
        public function GetList(){
            return $this->db->get("Purchase")->result_array();
        }
        public function Insert($Purchase){
            $this->db->trans_start();
            $query = "INSERT INTO Purchase(idService,PurchaseDate) VALUES (%d,CAST('%s' AS DATE))";
            $query = sprintf($query,$Purchase["idService"],$Purchase["PurchaseDate"]);
            $this->db->query($query);
            $idPurchase = $this->db->insert_id();
            $query = "UPDATE Purchase SET Reference=CreateReference('ACH',%d) WHERE idPurchase=%d";
            $query = sprintf($query,$idPurchase,$idPurchase);
            $this->db->query($query);

            for($i=0;$i<count($Purchase['Items']);$i++){
                $this->InsertPurchaseItem($Purchase['Items'][$i],$idPurchase);
            }
            $this->db->trans_complete();
        }
        public function InsertPurchaseItem($Item,$idPurchase){
            if(!isset($Item["Quantity"]) || empty($Item["Quantity"])) throw new Exception("La quantité est invalide.");
            if($Item["Quantity"]<0) throw new Exception("La quantité doit être positive.");

            $query = "INSERT INTO PurchaseItem(idPurchase,idItem,Quantity) VALUES (%d,%d,%f)";
            $query = sprintf($query,$idPurchase,$Item["idItem"],$Item["Quantity"]);
            $this->db->query($query);
        }

        public function get_purchaseById($idService,$idPurchaseStatus,$purchaseDate){
            $choix = array();
            $i = 0;
            if($idService != -1) { $choix[$i++] = "idService = '$idService'"; }
            if($idPurchaseStatus != -1) { $choix[$i++] = "idPurchaseStatus = '$idPurchaseStatus'"; }
            if(!empty($purchaseDate)) { $choix[$i++] = "purchaseDate = '$purchaseDate'"; }

            if($i > 0) {
                $critere = $choix[0]." ";
           
                for($j = 1; $j < $i; $j++) {
                    $critere .= " AND ".$choix[$j]." ";
                }
                $sql = "select * from purchaseServiceStatus where $critere";
            }
            else{
                $sql = "select * from purchaseServiceStatus";
            }
            $sql = sprintf($sql);
            $query = $this->db->query($sql);
            $result = array();
            foreach($query->result_array() as $row){
                $result[] = $row;
            }
            return $result;
        }
    }
?>