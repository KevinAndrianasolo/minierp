<?php
require_once APPPATH . 'models/BaseModel.php';

class PurchaseDocumentModel extends BaseModel
{
    public function __construct()
    {
        parent::__construct();
    }
    public function GetById($id)
    {
        return $this->db->get_where('PurchaseDocument', ['idPurchaseDocument' => $id])->row_array();
    }
    public function Insert($PurchaseDocument)
    {
        $this->db->trans_start();
        $query = "INSERT INTO PurchaseDocument(idPurchaseDocumentType,idProvider,DocumentDate,GlobalDiscount,idCurrency,idPurchase) VALUES (%d,%d,CAST('%s' AS DATE),%f,%d,%s)";
        $idPurchase = empty($PurchaseDocument["idPurchase"]) ? "NULL" : $PurchaseDocument["idPurchase"];
        if (!isset($PurchaseDocument["GlobalDiscount"])) throw new Exception("La remise globale est invalide.");
        if ($PurchaseDocument["GlobalDiscount"] < 0) throw new Exception("La remise globale doit être positive.");
        if ($PurchaseDocument["GlobalDiscount"] > 100) throw new Exception("La remise globale doit être inferieure à 100.");

        $query = sprintf($query, $PurchaseDocument["idPurchaseDocumentType"], $PurchaseDocument["idProvider"], $PurchaseDocument["DocumentDate"], $PurchaseDocument["GlobalDiscount"], $PurchaseDocument["idCurrency"], $idPurchase);
        $this->db->query($query);
        $idPurchaseDocument = $this->db->insert_id();
        $PurchaseDocumentType = $this->GetPurchaseDocumentType($PurchaseDocument["idPurchaseDocumentType"]);
        $query = "UPDATE PurchaseDocument SET Reference=CreateReference('%s',%d) WHERE idPurchaseDocument=%d";
        $query = sprintf($query, $PurchaseDocumentType['Prefix'], $idPurchaseDocument, $idPurchaseDocument);
        $this->db->query($query);

        for ($i = 0; $i < count($PurchaseDocument['Items']); $i++) {
            $this->InsertPurchaseDocumentItem($PurchaseDocument['Items'][$i], $idPurchaseDocument);
        }
        $this->db->trans_complete();
    }
    public function InsertPurchaseDocumentItem($Item, $idPurchaseDocument)
    {
        if (!isset($Item["Quantity"]) || empty($Item["Quantity"])) throw new Exception("La Quantité est invalide.");
        if (!isset($Item["UnitPrice"]) || empty($Item["UnitPrice"])) throw new Exception("Le prix unitaire est invalide.");
        if (!isset($Item["Reduction"])) throw new Exception("La reduction est invalide.");

        if ($Item["Quantity"] < 0) throw new Exception("La Quantité doit être positive.");
        if ($Item["UnitPrice"] < 0) throw new Exception("Le prix unitaire doit être positive.");
        if ($Item["Reduction"] < 0) throw new Exception("La reduction doit être positive.");

        $query = "INSERT INTO PurchaseDocumentItem(idPurchaseDocument,idItem,Quantity,idUnit,UnitPrice,Reduction) VALUES (%d,%d,%f,%d,%f,%f)";
        $query = sprintf($query, $idPurchaseDocument, $Item["idItem"], $Item["Quantity"], $Item["idUnit"], $Item["UnitPrice"], $Item["Reduction"]);
        $this->db->query($query);
    }

    public function GetPurchaseDocumentType($idPurchaseDocumentType)
    {
        return $this->db->get_where("PurchaseDocumentType", ["idPurchaseDocumentType" => $idPurchaseDocumentType])->row_array();
    }
    public function GetPurchaseDocumentTypeList()
    {
        return $this->db->get("PurchaseDocumentType")->result_array();
    }

    public function get_purchaseDocumentById($idPurchase, $idProvider, $idpurchaseDocumentType, $documentDate)
    {
        $choix = array();
        $i = 0;
        if ($idPurchase != -1) {
            $choix[$i++] = "idPurchase = '$idPurchase'";
        }
        if ($idProvider != -1) {
            $choix[$i++] = "idProvider = '$idProvider'";
        }
        if ($idpurchaseDocumentType != -1) {
            $choix[$i++] = "idpurchaseDocumentType = '$idpurchaseDocumentType'";
        }
        if (!empty($documentDate)) {
            $choix[$i++] = "documentDate = '$documentDate'";
        }

        if ($i > 0) {
            $critere = $choix[0] . " ";

            for ($j = 1; $j < $i; $j++) {
                $critere .= " AND " . $choix[$j] . " ";
            }
            $sql = "select * from purchaseDocumentCustType where $critere";
        } else {
            $sql = "select * from purchaseDocumentCustType";
        }
        $sql = sprintf($sql);
        $query = $this->db->query($sql);
        $result = array();
        foreach ($query->result_array() as $row) {
            $result[] = $row;
        }
        return $result;
    }
}
