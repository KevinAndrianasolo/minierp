<?php
    class TVAModel extends CI_Model{
        public function __construct(){
            parent::__construct();
        }
        public function Insert($TVA){
            if($TVA['Value']<0) throw new Exception("La valeur de la TVA doit être positive.");
            if($TVA['Value']>100) throw new Exception("La valeur de la TVA doit être inferieure à 100.");
            $sql = "INSERT INTO TVA (Value) VALUES ('%f')";
            $sql = sprintf($sql, $TVA['Value']);
            $this->db->query($sql);
        }
        public function CurrentTVA(){
            $TVA =  $this->db->query("SELECT * FROM CurrentTVA")->row_array();
            if($TVA==null) throw new Exception("La TVA n'a pas encore été définie.");
            return $TVA['Value'];
        }
    }
?>