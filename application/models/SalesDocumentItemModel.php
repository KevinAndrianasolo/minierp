<?php
class SalesDocumentItemModel extends BaseModel
{
    public function __construct()
    {
        parent::__construct();
    }
    public function GetItemList()
    {
        $sql = "SELECT sdi.idSalesDocument,sdi.idSalesDocumentItem,i.Designation, i.Reference, sdi.Quantity, sdi.Reduction, sdi.UnitPrice, sdi.idUnit, u.Unit FROM SalesDocumentItem sdi JOIN SalesDocument sd ON sd.idSalesDocument = sdi.idSalesDocument JOIN Item i ON i.idItem = sdi.idItem JOIN Unit u ON u.idUnit = sdi.idUnit";
        return $this->db->query($sql)->result_array();
    }
    public function GetItemsById($id)
    {
        $sql = "SELECT sdi.idSalesDocument,sdi.idSalesDocumentItem,i.Designation, i.Reference, sdi.Quantity, sdi.Reduction, sdi.UnitPrice, sdi.idUnit, u.Unit FROM SalesDocumentItem sdi JOIN SalesDocument sd ON sd.idSalesDocument = sdi.idSalesDocument JOIN Item i ON i.idItem = sdi.idItem JOIN Unit u ON u.idUnit = sdi.idUnit WHERE sdi.idSalesDocument = %d";
        $query = sprintf($sql, $id);
        return $this->db->query($query)->result_array();
    }
}
