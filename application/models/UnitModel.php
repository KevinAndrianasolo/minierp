<?php
    require_once APPPATH.'models/BaseModel.php';

    class UnitModel extends BaseModel{
        public function __construct(){
            parent::__construct();
        }
        public function GetList(){
            return $this->db->get("Unit")->result_array();
        }
    }
?>