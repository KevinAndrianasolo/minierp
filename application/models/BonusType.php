<?php
class BonusType extends BaseModel
{
    public static $PRIME = 1;
    public static $AUTRE = 2;

    public function GetBonusType()
    {
        return $this->db->get('BonusType')->result_array();
    }
}
