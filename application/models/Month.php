<?php
class Month extends BaseModel
{
    public static $JANVIER = 1;
    public static $FEVRIER = 2;
    public static $MARS = 3;
    public static $AVRIL = 4;
    public static $MAIS = 5;
    public static $JUIN = 6;
    public static $JUILLET = 7;
    public static $AOUT = 8;
    public static $SEPTEMBRE = 9;
    public static $OCTOBRE = 10;
    public static $NOVEMBRE = 11;
    public static $DECEMBRE = 12;
    public function GetMonths()
    {
        $array = array();
        $array[1]['Name'] = 'Janvier';
        $array[2]['Name'] = 'Fevrier';
        $array[3]['Name'] = 'Mars';
        $array[4]['Name'] = 'Avril';
        $array[5]['Name'] = 'Mai';
        $array[6]['Name'] = 'Juin';
        $array[7]['Name'] = 'Juillet';
        $array[8]['Name'] = 'Aout';
        $array[9]['Name'] = 'Septembre';
        $array[10]['Name'] = 'October';
        $array[11]['Name'] = 'Novembre';
        $array[12]['Name'] = 'Decembre';
        return $array;
    }
}
