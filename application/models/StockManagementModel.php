<?php
    require_once APPPATH.'models/BaseModel.php';
    require_once APPPATH.'classes/Stock.php';
    class StockManagementModel extends BaseModel{
        public function __construct(){
            parent::__construct();
        }
        public function GetStockManagementList(){
            return $this->db->get('StockManagementMethod')->result_array();
        }
        public function InsertEntree($idItem,$Quantity,$UnitPrice,$Date){
            
            if(!isset($Quantity) || empty($Quantity)) throw new Exception("Quantité est invalide.");
            if(!isset($UnitPrice) || empty($UnitPrice)) throw new Exception("Le prix unitaire est invalide.");
            if(!isset($Date) || empty($Date)) throw new Exception("La date est invalide.");


            if($Quantity<0) throw new Exception("Quantité doit être une valeur positive.");
            if($UnitPrice<0) throw new Exception("Le prix unitaire doit être une valeur positive.");
            // $format = 'Y-m-d h:i';
            // $now = new DateTime();
            // $date_stock = DateTime::createFromFormat($format, $Date, new DateTimeZone('Indian/Antananarivo'));
            // var_dump($now);
            // var_dump($Date);

            // var_dump($date_stock);
            // var_dump($date_stock>$now);
            // if($date_stock<$now) throw new Exception("La date d'entrée doit être inferieure ou égale à la date du jour.");
            
            $query = "INSERT INTO MouvementStock(idTypeMouvement, idItem, Quantity, UnitPrice , DateMouvement) VALUES (1, %d, %f, %f, '%s')";
            $query = sprintf($query,$idItem,$Quantity,$UnitPrice,$Date);
            $this->db->query($query);

        }
        public function InsertSortie($idItem,$Quantity,$Date,$idStockManagementMethod){
            if(!isset($Quantity) || empty($Quantity)) throw new Exception("Quantité est invalide.");
            if(!isset($Date) || empty($Date)) throw new Exception("La date est invalide.");
            if($Quantity<0) throw new Exception("Quantité doit être une valeur positive.");
            // $now = new DateTime();
            // $date_stock = DateTime::createFromFormat('Y-m-d h:i', $Date);
            // var_dump($now);
            // var_dump($date_stock);
            // var_dump($date_stock>$now);


            // if($date_stock>$now) throw new Exception("La date de sortie doit être inferieure ou égale à la date du jour.");
            
            $Stock = new Stock($idItem, $idStockManagementMethod, $Date, $this->db);
            if($Stock->QuantiteStock<$Quantity) throw new Exception("La Quantité à sortir doit être inférieur à la quantité en Stock : ".$Stock->QuantiteStock);
            $query = "INSERT INTO MouvementStock(idTypeMouvement, idItem, Quantity , DateMouvement) VALUES (2, %d, %f, '%s')";
            $query = sprintf($query,$idItem,$Quantity,$Date);
            $this->db->query($query);
        }
    }
?>