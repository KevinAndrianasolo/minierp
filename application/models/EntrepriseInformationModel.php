<?php
    class EntrepriseInformationModel extends BaseModel{
        public function __construct(){
            parent::__construct();
        }
        public function GetList(){
            return $this->db->get("EntrepriseInformation")->row_array();
        }
        public function GetById($id){
            return $this->db->get_where("EntrepriseInformation",["idEntrepriseInformation"=>$id])->row_array();
        }
    }
?>