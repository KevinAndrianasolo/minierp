<?php
require_once APPPATH . 'models/BaseModel.php';

class ItemModel extends BaseModel
{
    public function __construct()
    {
        parent::__construct();
    }
    public function GetList()
    {
        return $this->db->get("Item")->result_array();
    }
    public function GetById($id)
    {
        return $this->db->get_where("Item", ["idItem" => $id])->row_array();
    }
    public function Insert($Designation)
    {
        $tmp = $this->db->query("SELECT AUTO_INCREMENT AS next_id FROM information_schema.TABLES WHERE TABLE_SCHEMA = 'minierp' AND TABLE_NAME='Item'")->row_array();
        $idItem = $tmp['next_id'];
        $sql = sprintf("INSERT INTO Item (idItem,Reference,Designation) VALUES(%d,CreateReference('ART',%d),'%s')", $idItem, $idItem, $Designation);
        $this->db->query($sql);
    }

    public function Update($idItem, $Designation)
    {
        $sql = sprintf("UPDATE  Item  SET Designation = '%s' WHERE idItem=%d", $Designation, $idItem);
        $this->db->query($sql);
    }
    public function Delete($idItem)
    {
        return $this->db->query(sprintf("DELETE FROM Item WHERE idItem = %d", $idItem));
    }
}
