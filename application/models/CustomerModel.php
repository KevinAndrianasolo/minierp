<?php
require_once APPPATH . 'models/BaseModel.php';

class CustomerModel extends BaseModel
{
    public function __construct()
    {
        parent::__construct();
    }
    public function GetList()
    {
        return $this->db->get("Customer")->result_array();
    }

    public function GetById($id)
    {
        $array = array();
        $sql = sprintf("SELECT * FROM Customer WHERE idCustomer = %d", $id);
        $query = $this->db->query($sql);
        $array = $query->row_array();
        return $array;
    }
    public function Insert($Name, $Address, $Phone, $Email)
    {
        $tmp = $this->db->query("SELECT AUTO_INCREMENT AS next_id FROM information_schema.TABLES WHERE TABLE_SCHEMA = 'minierp' AND TABLE_NAME='Customer'")->row_array();
        $id = $tmp['next_id'];
        $sql = sprintf("INSERT INTO Customer (idCustomer,Reference,Name,Address,Phone,Email) VALUES(%d,CreateReference('CST',%d),'%s','%s','%s','%s')", $id, $id, $Name, $Address, $Phone, $Email);
        $this->db->query($sql);
    }

    public function Update($idCustomer, $Name, $Address, $Phone, $Email)
    {
        $sql = sprintf("UPDATE Customer SET Name = '%s',Address = '%s',Phone = '%s',Email = '%s' WHERE idCustomer = %d", $Name, $Address, $Phone, $Email, $idCustomer);
        $this->db->query($sql);
    }
    public function Delete($idCustomer)
    {
        return $this->db->query(sprintf("DELETE FROM Customer WHERE idCustomer = %d", $idCustomer));
    }
}
