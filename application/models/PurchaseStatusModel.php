<?php
    require_once APPPATH.'models/BaseModel.php';

    class PurchaseStatusModel extends BaseModel{
        public function __construct(){
            parent::__construct();
        }
        public function GetList(){
            return $this->db->get("PurchaseStatus")->result_array();
        }
    }
?>