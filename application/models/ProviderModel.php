<?php
require_once APPPATH . 'models/BaseModel.php';

class ProviderModel extends BaseModel
{
    public function __construct()
    {
        parent::__construct();
    }
    public function GetList()
    {
        return $this->db->get("Provider")->result_array();
    }
    public function GetById($id)
    {
        return $this->db->get_where('Provider', ['idProvider' => $id])->row_array();
    }
    public function Insert($Name, $Address, $Phone, $LeaderName, $Email, $Description)
    {
        $tmp = $this->db->query("SELECT AUTO_INCREMENT AS next_id FROM information_schema.TABLES WHERE TABLE_SCHEMA = 'minierp' AND TABLE_NAME='Provider'")->row_array();
        $id = $tmp['next_id'];
        $sql = sprintf("INSERT INTO Provider(idProvider,Reference,Name,Address,Phone,LeaderName,Email,Description) VALUES(%d,CreateReference('FRN',%d),'%s','%s','%s','%s','%s','%s')", $id, $id, $Name, $Address, $Phone, $LeaderName, $Email, $Description);
        $this->db->query($sql);
    }
    public function Update($id, $Name, $Address, $Phone, $LeaderName, $Email, $Description)
    {
        $sql = sprintf("UPDATE  Provider  SET Name = '%s',Address='%s', Phone='%s', LeaderName = '%s',Email='%s',Description = '%s' WHERE idProvider=%d", $Name, $Address, $Phone, $LeaderName, $Email, $Description, $id);
        $this->db->query($sql);
    }

    public function Delete($id)
    {
        return $this->db->query(sprintf("DELETE FROM Provider WHERE idProvider = %d", $id));
    }
}
