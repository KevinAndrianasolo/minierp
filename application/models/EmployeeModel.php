<?php
require_once APPPATH . 'models/BaseModel.php';
require_once APPPATH . 'models/Month.php';
require_once APPPATH . 'models/ContributionType.php';
require_once APPPATH . 'models/BonusType.php';
require_once APPPATH . 'models/ValueType.php';

class EmployeeModel extends BaseModel
{
    public function __construct()
    {
        parent::__construct();
    }
    public function GetList()
    {
        $sql = "SELECT e.*, d.Name AS departementName, ms.Description AS mariageStatus,EmployeeStatus FROM Employee e JOIN Departement d ON d.IdDepartement = e.IdDepartement JOIN MariageStatus ms ON ms.IdMariageStatus = e.IdMariageStatus JOIN EmployeeStatus es ON es.IdEmployeeStatus = e.IdEmployeeStatus";
        return $this->db->query($sql)->result_array();
    }

    public function InsertEmployee($Name, $CIN, $NumCnaps, $IdMariageStatus, $IdDepartement, $Status, $Fonction, $HiringDate)
    {
        $tmp = $this->db->query("SELECT AUTO_INCREMENT AS next_id FROM information_schema.TABLES WHERE TABLE_SCHEMA = 'minierp' AND TABLE_NAME='Employee'")->row_array();
        $id = $tmp['next_id'];
        $sql = sprintf("INSERT INTO Employee  VALUES(%d,CreateReference('EMP',%d),'%s','%s','%s',%d,%d,'%s','%s','%s')", $id, $id, $Name, $CIN, $NumCnaps, $IdMariageStatus, $IdDepartement, $Status, $Fonction, $HiringDate);
        $this->db->query($sql);
    }
    public function GetBasicSalary($IdEmployee)
    {
        $sql = sprintf("SELECT * FROM BasicSalary WHERE IdEmployee = %d ORDER BY DateSalary DESC LIMIT 1", $IdEmployee);
        return $this->db->query($sql)->row_array();
    }
    public function GetIRSATotal($IdEmployee, $Month)
    {
        $irsaMin = $this->GetMinimumIRSA();
        $irsa = $this->GetIRSAByRange($IdEmployee, $Month);

        $irsaTotal = 0;
        foreach ($irsa as $i) {
            $irsaTotal += $i['irsaObtained'];
        }

        return $irsaTotal;
    }

    public function GetIRSAByRange($IdEmployee, $Month)
    {
        $irsa = $this->GetIRSA();
        $irsaMin = $this->GetMinimumIRSA();
        $baseImposable = $this->GetBaseImposable($IdEmployee, $Month);

        for ($i = 0; $i < count($irsa); $i++) {
            $irsa[$i]['irsaObtained'] = 0;
            if (($irsa[$i]['ValueMin']) == -1 || ($irsa[$i]['ValueMin']) == 0  && ($irsa[$i]['ValueMax']) >= $baseImposable) {
                if ($irsa[$i]['ValueType'] == ValueType::$VALEUR) {
                    $irsa[$i]['irsaObtained'] = $irsa[$i]['Value'];
                } elseif ($irsa[$i]['ValueType'] == ValueType::$POURCENTAGE) {
                    $irsa[$i]['irsaObtained'] = ceil(($i['Value'] / 100)  * $baseImposable);
                    if ($irsa[$i]['irsaObtained'] < $irsaMin['value']) {
                        $irsa[$i]['irsaObtained'] = $irsaMin['value'];
                    }
                }
            } else {
                if (($irsa[$i]['ValueMin']) <= $baseImposable && $irsa[$i]['ValueMin'] != -1 && $irsa[$i]['ValueMin'] != 0 && $baseImposable >= ($irsa[$i]['ValueMax']) && $irsa[$i]['ValueMax'] != -1) {
                    if ($irsa[$i]['ValueType'] == ValueType::$VALEUR) {
                        $irsa[$i]['irsaObtained'] = $irsa[$i]['Value'];
                    } elseif ($irsa[$i]['ValueType'] == ValueType::$POURCENTAGE) {
                        $irsa[$i]['irsaObtained'] = ceil(($irsa[$i]['Value'] / 100) * (($irsa[$i]['ValueMax']) - ($irsa[$i]['ValueMin'])));
                    }
                    if ($irsa[$i]['irsaObtained'] < $irsaMin['value']) {
                        $irsa[$i]['irsaObtained'] = $irsaMin['value'];
                    }
                } elseif (($irsa[$i]['ValueMin'] <= $baseImposable && $irsa[$i]['ValueMax'] == -1) || ($irsa[$i]['ValueMin'] <= $baseImposable && $irsa[$i]['ValueMax'] > $baseImposable)) {
                    if ($irsa[$i]['ValueType'] == ValueType::$VALEUR) {
                        $irsa[$i]['irsaObtained'] = ($i['Value']);
                    } elseif ($irsa[$i]['ValueType'] == ValueType::$POURCENTAGE) {
                        $irsa[$i]['irsaObtained'] = ceil((($irsa[$i]['Value']) / 100) * ($baseImposable - ($irsa[$i]['ValueMin'])));
                    }
                    if ($irsa[$i]['irsaObtained'] < $irsaMin['value']) {
                        $irsa[$i]['irsaObtained'] = $irsaMin['value'];
                    }
                }
            }
        }
        return $irsa;
    }
    public function DeleteEmployee($IdEmployee)
    {
        $sql = sprintf("DELETE FROM Employee WHERE IdEmployee = %d", $IdEmployee);
        $this->db->query($sql);
    }
    public function UpdateEmployee($IdEmployee, $Name, $CIN, $NumCnaps, $IdMariageStatus, $IdDepartement, $Status, $Fonction, $HiringDate)
    {
        $sql = sprintf("UPDATE Employee SET Name = '%s' , CIN = '%s', NumCnaps = '%s', IdMariageStatus = %d , IdDepartement = %d , IdEmployeeStatus = %d , Fonction = '%s' , HiringDate = '%s' WHERE IdEmployee = %d", $Name, $CIN, $NumCnaps, $IdMariageStatus, $IdDepartement, $Status, $Fonction, $HiringDate, $IdEmployee);
        $this->db->query($sql);
    }
    public function GetById($IdEmployee)
    {
        $sql = sprintf("SELECT e.*, d.Name AS departementName, ms.Description AS mariageStatus,EmployeeStatus FROM Employee e JOIN Departement d ON d.IdDepartement = e.IdDepartement JOIN MariageStatus ms ON ms.IdMariageStatus = e.IdMariageStatus JOIN EmployeeStatus es ON es.IdEmployeeStatus = e.IdEmployeeStatus", $IdEmployee);
        return $this->db->query($sql)->row_array();
    }
    public function GetBonus($IdEmployee, $Month)
    {
        $sql = sprintf("SELECT b.*,bt.Description AS type FROM Bonus b JOIN BonusType bt ON bt.IdBonusType = b.BonusType WHERE IdEmployee = %d AND Month = %d", $IdEmployee, $Month);
        return $this->db->query($sql)->result_array();
    }
    public function GetOtherBonus($IdEmployee, $Month)
    {
        $sql = sprintf("SELECT * FROM Bonus WHERE IdEmployee = %d AND Month = %d AND BonusType != %d", $IdEmployee, $Month, BonusType::$PRIME);
        return $this->db->query($sql)->result_array();
    }

    public function GetPrime($IdEmployee, $Month)
    {

        $sql = sprintf("SELECT * FROM Bonus WHERE IdEmployee = %d AND Month = %d AND BonusType = %d LIMIT 1", $IdEmployee, $Month, BonusType::$PRIME);
        return $this->db->query($sql)->row_array();
    }

    public function GetExtraHour($IdEmployee, $Month)
    {
        $employee = $this->GetById($IdEmployee);

        if ($employee['IdEmployeeStatus'] == EmployeeStatus::$CADRE_SUPERIEUR) {
            return null;
        }
        $sql = sprintf("SELECT * FROM ExtraHour WHERE IdEmployee = %d AND Month = %d LIMIT 1", $IdEmployee, $Month);
        return $this->db->query($sql)->row_array();
    }
    public function GetNetSalary($IdEmployee, $Month)
    {
        $baseImposable = $this->GetBaseImposable($IdEmployee, $Month);
        $irsaTotal = $this->GetIRSATotal($IdEmployee, $Month);
        return $baseImposable - $irsaTotal;
    }

    public function InsertBonus($IdEmployee, $BonusType, $CashValue, $Month)
    {
        $tmp = $this->db->query("SELECT AUTO_INCREMENT AS next_id FROM information_schema.TABLES WHERE TABLE_SCHEMA = 'minierp' AND TABLE_NAME='Bonus'")->row_array();
        $id = $tmp['next_id'];
        $sql = sprintf("INSERT INTO Bonus VALUES (%d,CreateReference('BON',%d),%d,%d,%s,%s)", $id, $id, $IdEmployee, $BonusType, $CashValue, $Month);
        $this->db->query($sql);
    }



    public function InsertExtraHour($IdEmployee, $Hour, $Month)
    {
        $employee = $this->GetById($IdEmployee);
        if ($employee['Status'] == EmployeeStatus::$CADRE_SUPERIEUR) {
            throw new Exception("Les cadres supérieurs ne bénéficient pas d'heures suplémentaires.");
        }
        $tmp = $this->db->query("SELECT AUTO_INCREMENT AS next_id FROM information_schema.TABLES WHERE TABLE_SCHEMA = 'minierp' AND TABLE_NAME='ExtraHour'")->row_array();
        $id = $tmp['next_id'];
        $sql = sprintf("INSERT INTO ExtraHour VALUES (%d,CreateReference('EXT',%d),%d,%s,%d)", $id, $id, $IdEmployee, $Hour, $Month);
        $this->db->query($sql);
    }

    public function GetMinimumIRSA()
    {
        $sql = "SELECT * FROM MinimumIRSA ORDER BY DateMinimumIRSA DESC LIMIT 1";
        return $this->db->query($sql)->row_array();
    }
    public function GetIRSA()
    {
        $sql = "SELECT * FROM IRSA ORDER BY ValueMin ASC";
        return $this->db->query($sql)->result_array();
    }
    public function GetBaseImposable($IdEmployee, $Month)
    {
        $basic_salary = $this->GetBasicSalary($IdEmployee);
        $ostieAmount = $this->GetOstieAmount($IdEmployee);
        $cnapsAmount = $this->GetCnapsAmount($IdEmployee);
        $baseImposable = $basic_salary['Salary'] - ($ostieAmount + $cnapsAmount);

        $bonus = $this->GetOtherBonus($IdEmployee, $Month);
        if (isset($bonus) && count($bonus) > 0) {
            foreach ($bonus as $tmp) {
                $baseImposable += $tmp['CashValue'];
            }
        }

        $prime = $this->GetPrime($IdEmployee, $Month);
        if (isset($prime)) {
            $baseImposable += $prime['CashValue'];
        }

        $extraHourValue = $this->GetExtraHourValue($IdEmployee);
        $extraHour = $this->GetExtraHour($IdEmployee, $Month);
        if (isset($extraHour)) {
            $baseImposable += $extraHour['Hour'] * $extraHourValue['ValuePerHour'];
        }
        return $baseImposable;
    }

    public function InsertExtraHourValue($IdEmployee, $ValuePerHour, $DateValue)
    {
        $tmp = $this->db->query("SELECT AUTO_INCREMENT AS next_id FROM information_schema.TABLES WHERE TABLE_SCHEMA = 'minierp' AND TABLE_NAME='ExtraHourValue'")->row_array();
        $id = $tmp['next_id'];
        $sql = sprintf("INSERT INTO ExtraHourValue VALUES (%d,CreateReference('EHV',%d),%d,%s,'%s')", $id, $id, $IdEmployee, $ValuePerHour, $DateValue);
        $this->db->query($sql);
    }
    public function GetExtraHourValue($IdEmployee)
    {
        $sql = sprintf("SELECT * FROM ExtraHourValue WHERE IdEmployee = %d ORDER BY DateValue DESC LIMIT 1", $IdEmployee);
        return $this->db->query($sql)->row_array();
    }
    public function GetCnapsAmount($IdEmployee)
    {
        $basic_salary = $this->GetBasicSalary($IdEmployee);
        $cnaps = $this->GetCNAPSContribution();
        $maximumCNAPS = $this->GetMaximumCNAPS();
        if ($basic_salary['Salary'] > $maximumCNAPS['Value']) {
            $basic_salary['Salary'] = $maximumCNAPS['Value'];
        }
        return $basic_salary['Salary'] * ($cnaps['Value'] / 100);;
    }
    public function GetOstieAmount($IdEmployee)
    {
        $maximumCNAPS = $this->GetMaximumCNAPS();
        $basic_salary = $this->GetBasicSalary($IdEmployee);
        $ostie = $this->GetOstieContribution();

        if ($basic_salary['Salary'] > $maximumCNAPS['Value']) {
            $basic_salary['Salary'] = $maximumCNAPS['Value'];
        }
        return $basic_salary['Salary'] * ($ostie['Value'] / 100);;
    }
    public function GetCNAPSContribution()
    {
        $sql = sprintf("SELECT * FROM SocialContribution WHERE ContributionType = %d ORDER BY Date DESC LIMIT 1", ContributionType::$CNAPS);
        return $this->db->query($sql)->row_array();
    }
    public function GetOSTIEContribution()
    {
        $sql = sprintf("SELECT * FROM SocialContribution WHERE ContributionType = %d ORDER BY Date DESC LIMIT 1", ContributionType::$OSTIE);
        return $this->db->query($sql)->row_array();
    }
    public function GetMaximumCNAPS()
    {
        $sql = sprintf("SELECT * FROM MaximumCNAPS ORDER BY DateMaximumCNAPS DESC LIMIT 1");
        return $this->db->query($sql)->row_array();
    }
}
