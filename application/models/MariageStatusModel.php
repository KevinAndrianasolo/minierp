<?php
class MariageStatusModel extends BaseModel
{
    public function __construct()
    {
        parent::__construct();
    }
    public function GetAll()
    {
        return $this->db->get('MariageStatus')->result_array();
    }
}
