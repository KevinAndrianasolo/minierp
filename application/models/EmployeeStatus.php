<?php
class EmployeeStatus extends BaseModel
{
    public static $CADRE_SUPERIEUR = 1;

    public function GetAll()
    {
        return $this->db->get('EmployeeStatus')->result_array();
    }
}
