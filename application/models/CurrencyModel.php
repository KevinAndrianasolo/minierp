<?php
    require_once APPPATH.'models/BaseModel.php';

    class CurrencyModel extends BaseModel{
        public function __construct(){
            parent::__construct();
        }
        public function GetList(){
            return $this->db->get("Currency")->result_array();
        }
    }
?>