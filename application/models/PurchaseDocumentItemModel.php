<?php
class PurchaseDocumentItemModel extends BaseModel
{
    public function __construct()
    {
        parent::__construct();
    }
    public function GetItemList()
    {
        $sql = "SELECT sdi.idPurchaseDocument,sdi.idPurchaseDocumentItem,i.Designation, i.Reference, sdi.Quantity, sdi.Reduction, sdi.UnitPrice, sdi.idUnit, u.Unit FROM PurchaseDocumentItem sdi JOIN PurchaseDocument sd ON sd.idPurchaseDocument = sdi.idPurchaseDocument JOIN Item i ON i.idItem = sdi.idItem JOIN Unit u ON u.idUnit = sdi.idUnit";
        return $this->db->query($sql)->result_array();
    }
    public function GetItemsById($id)
    {
        $sql = "SELECT sdi.idPurchaseDocument,sdi.idPurchaseDocumentItem,i.Designation, i.Reference, sdi.Quantity, sdi.Reduction, sdi.UnitPrice, sdi.idUnit, u.Unit FROM PurchaseDocumentItem sdi JOIN PurchaseDocument sd ON sd.idPurchaseDocument = sdi.idPurchaseDocument JOIN Item i ON i.idItem = sdi.idItem JOIN Unit u ON u.idUnit = sdi.idUnit WHERE sdi.idPurchaseDocument = %d";
        $query = sprintf($sql, $id);
        return $this->db->query($query)->result_array();
    }
}
