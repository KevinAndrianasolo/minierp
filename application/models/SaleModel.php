<?php
    require_once APPPATH.'models/BaseModel.php';

    class SaleModel extends BaseModel{
        public function __construct(){
            parent::__construct();
        }
        public function GetList(){
            return $this->db->get("Sale")->result_array();
        }
        public function Insert($Sale){
            $this->db->trans_start();
            $query = "INSERT INTO Sale(SaleDate) VALUES (CAST('%s' AS DATE))";
            $query = sprintf($query,$Sale["SaleDate"]);
            $this->db->query($query);
            $idSale = $this->db->insert_id();
            $query = "UPDATE Sale SET Reference=CreateReference('VNT',%d) WHERE idSale=%d";
            $query = sprintf($query,$idSale,$idSale);
            $this->db->query($query);

            for($i=0;$i<count($Sale['Items']);$i++){
                $this->InsertSaleItem($Sale['Items'][$i],$idSale);
            }
            $this->db->trans_complete();
        }
        public function InsertSaleItem($Item,$idSale){
            if(!isset($Item["Quantity"]) || empty($Item["Quantity"])) throw new Exception("La quantité est invalide.");
            if($Item["Quantity"]<0) throw new Exception("La quantité doit être positive.");

            $query = "INSERT INTO SaleItem(idSale,idItem,Quantity) VALUES (%d,%d,%f)";
            $query = sprintf($query,$idSale,$Item["idItem"],$Item["Quantity"]);
            $this->db->query($query);
        }

        public function get_saleById($idSaleStatus,$saleDate){
            $choix = array();
            $i = 0;
            if($idSaleStatus != -1) { $choix[$i++] = "idSaleStatus = '$idSaleStatus'"; }
            if(!empty($saleDate)) { $choix[$i++] = "saleDate = '$saleDate'"; }

            if($i > 0) {
                $critere = $choix[0]." ";
           
                for($j = 1; $j < $i; $j++) {
                    $critere .= " AND ".$choix[$j]." ";
                }
                $sql = "select * from saleStatusStatus where $critere";
            }
            else{
                $sql = "select * from saleStatusStatus";
            }
            $sql = sprintf($sql);
            $query = $this->db->query($sql);
            $result = array();
            foreach($query->result_array() as $row){
                $result[] = $row;
            }
            return $result;
        }
    }
?>