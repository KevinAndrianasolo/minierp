<?php
function format_number($number)
{
    return number_format($number, 2, ',', ' ');
}

function format_text($text)
{
    return iconv('UTF-8', 'windows-1252', $text);
}
