<!DOCTYPE html>
<html>

<head>
  <title>Insertion TVA</title>
  <link rel="stylesheet" type="text/css" href="<?php echo site_url('assets/bootstrap/css/bootstrap.min.css'); ?>">
  <link rel="stylesheet" type="text/css" href="<?php echo site_url('assets/css/crud_style.css'); ?>">
  <link rel="stylesheet" type="text/css" href="<?php echo site_url('assets/css/font.css'); ?>">
</head>

<body>
  <?php require_once APPPATH . 'views/header.php';
  ?>
  <div class="main-content">

    <h1 class="display-4 title">Insertion TVA</h1>
    <form method="post" action="<?php echo site_url("TVA/InsertTVA") ?>" class="form-wrap">
      <div class="form-group">
        <label>Valeur</label>
        <input class="form-control" type="text" name="Value" />
      </div>
      <?php if (isset($success)) { ?>
        <div class="alert alert-success" role="alert">
          <?php echo $success; ?>
        </div>
      <?php } ?>
      <?php if (isset($error)) { ?>
        <div class="alert alert-danger" role="alert">
          <?php echo $error; ?>
        </div>
      <?php } ?>
      <button class="btn btn-dark" type="submit">Changer la TVA</button>

    </form>

    <div>
      <script src="<?php echo site_url('assets/bootstrap/js/jquery.min.js'); ?>"></script>
      <script src="<?php echo site_url('assets/bootstrap/js/bootstrap.min.js'); ?>"></script>

</body>

</html>