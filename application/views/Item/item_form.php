<!DOCTYPE html>
<html lang="en">

<head>
    <title>Formulaire Article</title>
    <link rel="stylesheet" type="text/css" href="<?php echo site_url('assets/bootstrap/css/bootstrap.min.css'); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo site_url('assets/css/crud_style.css'); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo site_url('assets/css/font.css'); ?>">

</head>

<body>
<?php require_once APPPATH . 'views/header.php';
?>
    <div class="main-content">
        <h1 class="display-4 title">Formulaire article <a href="<?=site_url('Item')?>" class="btn btn-primary">Liste</a></h1>
        <form action="<?php if (isset($item)) {
                            echo site_url("Item/Update_Action");
                        } else {
                            echo site_url("Item/Add_Action");
                        } ?>
" method="POST" class="form-wrap">
            <?php if (isset($item)) { ?>
                <input type="hidden" name="idItem" value="<?= $item['idItem'] ?>">
            <?php } ?>
            <div class="form-group">
                <label for="Designation">Designation</label>
                <input class="form-control" type="text" name="Designation" id="Designation" <?php if (isset($item)) {
                                                                                                echo "value =\"" . $item['Designation'] . "\"";
                                                                                            } ?>>
            </div>

            <button class="btn btn-dark" type="submit">Valider</button>
        </form>
    </div>
    <script src="<?php echo site_url('assets/bootstrap/js/jquery.min.js'); ?>"></script>
    <script src="<?php echo site_url('assets/bootstrap/js/bootstrap.min.js'); ?>"></script>

</body>

</html>