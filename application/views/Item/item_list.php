<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="<?php echo site_url('assets/bootstrap/css/bootstrap.min.css'); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo site_url('assets/css/font.css'); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo site_url('assets/css/crud_style.css'); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo site_url('assets/fontawesome/css/font-awesome.min.css'); ?>">

    <title>Liste d'articles</title>
</head>

<body>
<?php require_once APPPATH . 'views/header.php';
?>
    <div class="main-content">
        <h1 class="display-4 title">Liste articles <a href="<?=site_url('Item/Add')?>" class="btn btn-success">Ajouter</a></h1>

        <table class="table">
            <thead class="thead-dark">

                <tr>
                    <th>IdItem</th>
                    <th>Reference</th>
                    <th>Designation</th>
                    <th colspan=2>Actions</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($item as $i) { ?>
                    <tr>
                        <td><?= $i['idItem'] ?></td>
                        <td><?= $i['Reference'] ?></td>
                        <td><?= $i['Designation'] ?></td>
                        <td class="icon-wrap"><a href="<?php echo base_url() . "Item/Update/" . $i["idItem"] ?>"><i class="fa fa-edit icon icon-edit"></i></a></td>
                        <td class="icon-wrap"><a href="<?php echo site_url("Item/Delete/" . $i["idItem"]) ?>"><i class="fa fa-remove icon icon-remove"></i></a></td>
                        <td><a class="btn btn-dark" href="<?php echo site_url('StockManagement'); ?>/index/<?= $i['idItem'] ?>">Voir Stock</a></td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
    <script src="<?php echo site_url('assets/bootstrap/js/jquery.min.js'); ?>"></script>
    <script src="<?php echo site_url('assets/bootstrap/js/bootstrap.min.js'); ?>"></script>

</body>

</html>