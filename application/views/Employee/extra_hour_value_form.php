<!DOCTYPE html>
<html lang="en">

<head>
    <title>Formulaire prix heure supplémentaire</title>
    <link rel="stylesheet" type="text/css" href="<?php echo site_url('assets/bootstrap/css/bootstrap.min.css'); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo site_url('assets/css/crud_style.css'); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo site_url('assets/css/font.css'); ?>">

</head>

<body>
    <?php require_once APPPATH . 'views/header.php';
    ?>
    <div class="main-content">
        <h1 class="display-4 title">Formulaire prix heure supplementaire</h1>
        <form action="<?= site_url('Employee/ExtraHourValue_Add') ?>" method="POST" class="form-wrap">
            <div class="form-group">
                <label for="IdEmployee">Nom employé</label>
                <select name="IdEmployee" id="IdEmployee" class="form-control">
                    <?php foreach ($Employees as $tmp) { ?>
                        <option <?php if (isset($employee) && $employee['IdEmployee'] == $tmp['IdEmployee']) {
                                    echo "selected =\" selected\"";
                                }  ?> value="<?= $tmp['IdEmployee'] ?>"><?= $tmp['Name'] ?></option>
                    <?php } ?>
                </select>
            </div>
            <div class="form-group">
                <label for="valuePerHour">Prix heure supplémentaire par heure :</label>
                <input type="number" name="valuePerHour" id="valuePerHour" class="form-control">
            </div>
            <div class="form-group">
                <label for="dateValue">Date de mise en oeuvre :</label>
                <input type="date" name="dateValue" id="dateValue" class="form-control">
            </div>
            <div class="form-group">
                <input type="submit" class="btn btn-dark form-control" value="Valider">
            </div>
        </form>
    </div>
    <script src="<?php echo site_url('assets/bootstrap/js/jquery.min.js'); ?>"></script>
    <script src="<?php echo site_url('assets/bootstrap/js/bootstrap.min.js'); ?>"></script>

</body>

</html>