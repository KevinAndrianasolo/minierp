<!DOCTYPE html>
<html lang="en">

<head>
    <title>Fiche de paie</title>
    <link rel="stylesheet" type="text/css" href="<?php echo site_url('assets/bootstrap/css/bootstrap.min.css'); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo site_url('assets/css/crud_style.css'); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo site_url('assets/css/font.css'); ?>">

</head>

<body>
    <?php require_once APPPATH . 'views/header.php';
    require_once APPPATH . 'views/viewHelper.php';
    ?>
    <div class="main-content">
        <h1 class="display-4 title">Fiche de paie : <?= $employee['Reference'] ?></h1>
        <h6 class="display-4 title"><?= $employee['Name'] ?>  - Statut : <?= $employee['EmployeeStatus'] ?></h6>
        <h6 class="display-4 title" style="color:black; text-align:left;"> Mois : <?= $Month ?></h6>
        <h6>Heures supplementaires : <?= $extra_hour ?> | Prix horaire : <?= $extra_hour_value ?></h6>
        <?php if (count($bonus) == 0) {
            echo '<h6>Indemnites : Aucune</h6>';
        } else { ?>
            <div>
                <table class="table table-bordered">
                    <thead class="thead-dark">
                        <tr>
                            <th>Indemnités</th>
                            <th>Valeur numéraire</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($bonus as $b) { ?>
                            <tr>
                                <td><?= $b['type'] ?></td>
                                <td><?= format_number($b['CashValue']) ?></td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
        <?php } ?>
        <table class="table table-bordered">
            <thead class="thead-dark">
                <tr>
                    <th>Rubriques</th>
                    <th>Taux (%) / Valeur</th>
                    <th>Base</th>
                    <th>Gains</th>
                    <th>Retenues</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>Salaire de base</td>
                    <td></td>
                    <td></td>
                    <td><?= format_number($basic_salary['Salary']) ?></td>
                </tr>
                <tr>
                    <td>CNAPS (%)</td>
                    <td><?= format_number($cnaps['Value']) ?></td>
                    <td><?= format_number($basic_salary['Salary']) ?></td>
                    <td></td>
                    <td><?= format_number($cnapsAmount) ?></td>
                </tr>
                <tr>
                    <td>OSTIE (%)</td>
                    <td><?= format_number($ostie['Value']) ?></td>
                    <td><?= format_number($basic_salary['Salary']) ?></td>
                    <td></td>
                    <td><?= format_number($ostieAmount) ?></td>
                </tr>
                <tr>
                    <td>IRSA</td>
                    <td></td>
                    <td><?= format_number($base_imposable) ?></td>
                    <td></td>
                    <td></td>
                </tr>
                <?php foreach ($IRSA as $i) { ?>
                    <tr>
                        <td style="text-align: left;"><?= $i['Description'] ?> <?php if ($i['ValueType'] == 2) {
                                                                                    echo '(%)';
                                                                                } ?> : <?php if ($i['ValueMin'] <= 0) {
                                                                                            echo 'Inférieure à ' . format_number($i['ValueMax']);
                                                                                        } elseif ($i['ValueMax'] < 0) {
                                                                                            echo 'Supérieure à ' . format_number($i['ValueMin']);
                                                                                        } else {
                                                                                            echo 'De ' . format_number($i['ValueMin']) . ' à ' . format_number($i['ValueMax']);
                                                                                        } ?></td>
                        <td><?= format_number($i['Value']) ?></td>
                        <td></td>
                        <td></td>
                        <td><?= format_number($i['irsaObtained']) ?></td>
                    </tr>
                <?php } ?>
                <tr>
                    <td>Total IRSA</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td><?= format_number($irsaTotal) ?></td>
                </tr>
                <tr>
                    <td>Salaire Net</td>
                    <td></td>
                    <td></td>
                    <td><?= format_number($salaireNet) ?></td>
                    <td></td>
                </tr>
            </tbody>
        </table>
    </div>
    <script src="<?php echo site_url('assets/bootstrap/js/jquery.min.js'); ?>"></script>
    <script src="<?php echo site_url('assets/bootstrap/js/bootstrap.min.js'); ?>"></script>

</body>

</html>