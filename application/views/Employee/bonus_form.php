<!DOCTYPE html>
<html lang="en">

<head>
    <title>Formulaire avantage</title>
    <link rel="stylesheet" type="text/css" href="<?php echo site_url('assets/bootstrap/css/bootstrap.min.css'); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo site_url('assets/css/crud_style.css'); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo site_url('assets/css/font.css'); ?>">

</head>

<body>
    <?php require_once APPPATH . 'views/header.php';
    ?>
    <div class="main-content">
        <h1 class="display-4 title">Voir avantages</h1>
        <form action="<?= site_url('Employee/Bonus') ?>" class="form-inline" method="GET">
            <div class="form-group">
                <label for="IdEmployee">Nom employé : </label>
                <select name="IdEmployee" id="IdEmployee" class="form-control">
                    <?php foreach ($Employees as $tmp) { ?>
                        <option <?php if (isset($employee) && $employee['IdEmployee'] == $tmp['IdEmployee']) {
                                    echo "selected =\" selected\"";
                                }  ?> value="<?= $tmp['IdEmployee'] ?>"><?= $tmp['Name'] ?></option>
                    <?php } ?>
                </select>
            </div>
            <div class="form-group">
                <label for="Month">   Mois :</label>
                <select name="Month" id="Month" class="form-control">
                    <?php for ($i = 1; $i <= count($Month); $i++) { ?>
                        <option value="<?= $i ?>"><?= $Month[$i]['Name'] ?></option>
                    <?php } ?>
                </select>
            </div>
            <div class="form-group">
                <input type="submit" class="btn btn-dark form-control" value="Voir">
            </div>
        </form>
        <h1 class="display-4 title">Formulaire avantages</h1>
        <form action="<?= site_url('Employee/Bonus_Add') ?>" method="POST" class="form-wrap">
            <div class="form-group">
                <label for="IdEmployee">Nom employé</label>
                <select name="IdEmployee" id="IdEmployee" class="form-control">
                    <?php foreach ($Employees as $tmp) { ?>
                        <option <?php if (isset($employee) && $employee['IdEmployee'] == $tmp['IdEmployee']) {
                                    echo "selected =\" selected\"";
                                }  ?> value="<?= $tmp['IdEmployee'] ?>"><?= $tmp['Name'] ?></option>
                    <?php } ?>
                </select>
            </div>
            <div class="form-group">
                <label for="BonusType">Type :</label>
                <select name="BonusType" id="BonusType" class="form-control">
                    <?php foreach ($BonusType as $tmp) { ?>
                        <option value="<?= $tmp['IdBonusType'] ?>"><?= $tmp['Description'] ?></option>
                    <?php } ?>
                </select>
            </div>
            <div class="form-group">
                <label for="Month">Mois</label>
                <select name="Month" id="Month" class="form-control">
                    <?php for ($i = 1; $i <= count($Month); $i++) { ?>
                        <option value="<?= $i ?>"><?= $Month[$i]['Name'] ?></option>
                    <?php } ?>
                </select>
            </div>
            <div class="form-group">
                <label for="CashValue">Valeur numéraire</label>
                <input type="number" name="CashValue" id="CashValue" class="form-control">
            </div>
            <div class="form-group">
                <input type="submit" class="btn btn-dark form-control" value="Ajouter">
            </div>
        </form>
    </div>
    <script src="<?php echo site_url('assets/bootstrap/js/jquery.min.js'); ?>"></script>
    <script src="<?php echo site_url('assets/bootstrap/js/bootstrap.min.js'); ?>"></script>

</body>

</html>