<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="<?php echo site_url('assets/bootstrap/css/bootstrap.min.css'); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo site_url('assets/css/font.css'); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo site_url('assets/css/crud_style.css'); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo site_url('assets/fontawesome/css/font-awesome.min.css'); ?>">

    <title>Liste des employés</title>
</head>

<body>
    <?php require_once APPPATH . 'views/header.php';
    ?>
    <div class="main-content">
        <h1 class="display-4 title">Liste des employes <a class="btn btn-success" href="<?= site_url('Employee/Add') ?>">Ajouter</a></h1>

        <table class="table">
            <thead class="thead-dark">
                <!-- $Name, $CIN, $NumCnaps, $IdMariageStatus, $IdDepartement, $Status, $Fonction, $HiringDate -->
                <tr>
                    <th>ID</th>
                    <th>Reference</th>
                    <th>Nom</th>
                    <th>Numéro CIN</th>
                    <th>Numero Cnaps</th>
                    <th>Statut Marital</th>
                    <th>Departement</th>
                    <th>Statut</th>
                    <th>Fonction</th>
                    <th>Date d'embauche</th>
                    <th colspan=2>Actions</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($Employee as $i) { ?>
                    <tr>
                        <td><?= $i['IdEmployee'] ?></td>
                        <td><?= $i['Reference'] ?></td>
                        <td><?= $i['Name'] ?></td>
                        <td><?= $i['CIN'] ?></td>
                        <td><?= $i['NumCnaps'] ?></td>
                        <td><?= $i['mariageStatus'] ?></td>
                        <td><?= $i['departementName'] ?></td>
                        <td><?= $i['EmployeeStatus'] ?></td>
                        <td><?= $i['Fonction'] ?></td>
                        <td><?= $i['HiringDate'] ?></td>

                        <td class="icon-wrap"><a href="<?php echo base_url() . "Employee/Update/" . $i["IdEmployee"] ?>"><i class="fa fa-edit icon icon-edit"></i></a></td>
                        <td class="icon-wrap"><a href="<?php echo site_url("Employee/Delete/" . $i["IdEmployee"]) ?>"><i class="fa fa-remove icon icon-remove"></i></a></td>
                        <td> <a href="<?= site_url('Employee/PayStubForm') . '?IdEmployee=' . $i['IdEmployee'] ?>" class="btn btn-dark">Fiche</a></td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>

    </div>
    <script src="<?php echo site_url('assets/bootstrap/js/jquery.min.js'); ?>"></script>
    <script src="<?php echo site_url('assets/bootstrap/js/bootstrap.min.js'); ?>"></script>

</body>

</html>