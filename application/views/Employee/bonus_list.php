<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="<?php echo site_url('assets/bootstrap/css/bootstrap.min.css'); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo site_url('assets/css/font.css'); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo site_url('assets/css/crud_style.css'); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo site_url('assets/fontawesome/css/font-awesome.min.css'); ?>">

    <title>Liste des avantages</title>
</head>

<body>
    <?php require_once APPPATH . 'views/header.php';
    ?>
    <div class="main-content">
        <h1 class="display-4 title">Liste des avantages : <?= $employee['Name'] ?> | <?= $Month['Name'] ?><a class="btn btn-success" href="<?= site_url('Employee/Bonus') ?>">Ajouter</a></h1>

        <table class="table">
            <thead class="thead-dark">
                <tr>
                    <th>ID Bonus</th>
                    <th>Reference</th>
                    <th>Type</th>
                    <th>Valeur numéraire</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($bonus as $i) { ?>
                    <tr>
                        <td><?= $i['IdBonus'] ?></td>
                        <td><?= $i['Reference'] ?></td>
                        <td><?= $i['type'] ?></td>
                        <td><?= format_number($i['CashValue']) ?></td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>

    </div>
    <script src="<?php echo site_url('assets/bootstrap/js/jquery.min.js'); ?>"></script>
    <script src="<?php echo site_url('assets/bootstrap/js/bootstrap.min.js'); ?>"></script>

</body>

</html>