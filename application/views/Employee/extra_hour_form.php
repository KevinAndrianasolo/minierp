<!DOCTYPE html>
<html lang="en">

<head>
    <title>Formulaire heure supplémentaire</title>
    <link rel="stylesheet" type="text/css" href="<?php echo site_url('assets/bootstrap/css/bootstrap.min.css'); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo site_url('assets/css/crud_style.css'); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo site_url('assets/css/font.css'); ?>">

</head>

<body>
    <?php require_once APPPATH . 'views/header.php';
    ?>
    <div class="main-content">
        <h1 class="display-4 title">Formulaire heure supplementaire</h1>
        <form action="<?= site_url('Employee/ExtraHour_Add') ?>" method="POST" class="form-wrap">
            <div class="form-group">
                <label for="IdEmployee">Nom employé</label>
                <select name="IdEmployee" id="IdEmployee" class="form-control">
                    <?php foreach ($Employees as $tmp) { ?>
                        <option <?php if (isset($employee) && $employee['IdEmployee'] == $tmp['IdEmployee']) {
                                    echo "selected =\" selected\"";
                                }  ?> value="<?= $tmp['IdEmployee'] ?>"><?= $tmp['Name'] ?></option>
                    <?php } ?>
                </select>
            </div>
            <div class="form-group">
                <label for="Month">Mois</label>
                <select name="Month" id="Month" class="form-control">
                    <?php for ($i = 1; $i <= count($Month); $i++) { ?>
                        <option value="<?= $i ?>"><?= $Month[$i]['Name'] ?></option>
                    <?php } ?>
                </select>
            </div>
            <div class="form-group">
                <label for="ExtraHour">Heures supplémentaires :</label>
                <input type="number" name="ExtraHour" id="ExtraHour" class="form-control">
            </div>
            <div class="form-group">
                <input type="submit" class="btn btn-dark form-control" value="Valider">
            </div>
        </form>
    </div>
    <script src="<?php echo site_url('assets/bootstrap/js/jquery.min.js'); ?>"></script>
    <script src="<?php echo site_url('assets/bootstrap/js/bootstrap.min.js'); ?>"></script>

</body>

</html>