<!DOCTYPE html>
<html lang="en">

<head>
    <title>Formulaire Article</title>
    <link rel="stylesheet" type="text/css" href="<?php echo site_url('assets/bootstrap/css/bootstrap.min.css'); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo site_url('assets/css/crud_style.css'); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo site_url('assets/css/font.css'); ?>">

</head>

<body>
    <?php require_once APPPATH . 'views/header.php';
    ?>
    <div class="main-content">
        <h1 class="display-4 title">Formulaire Employe <a class="btn btn-primary" href="<?php echo site_url("Employee/List") ?>">Liste</a>
        </h1>
        <form action="<?php if (isset($employee)) {
                            echo site_url("Employee/Update_Action");
                        } else {
                            echo site_url("Employee/Add_Action");
                        } ?>
" method="POST" class="form-wrap">
            <?php if (isset($employee)) { ?>
                <input type="hidden" name="IdEmployee" value="<?= $employee['IdEmployee'] ?>">
            <?php } ?>
            <div class="form-group">

                <label for="Name">Nom</label>
                <input class="form-control" type="text" name="Name" id="Name" <?php if (isset($employee)) {
                                                                                    echo "value =\"" . $employee['Name'] . "\"";
                                                                                } ?>>
            </div>
            <div class="form-group">
                <label for="CIN">CIN</label>
                <input class="form-control" type="text" name="CIN" id="CIN" <?php if (isset($employee)) {
                                                                                echo "value =\"" . $employee['CIN'] . "\"";
                                                                            } ?>>
            </div>
            <div class="form-group">
                <label for="NumCnaps">Numero Cnaps</label>
                <input class="form-control" type="text" name="NumCnaps" id="NumCnaps" <?php if (isset($employee)) {
                                                                                            echo "value =\"" . $employee['NumCnaps'] . "\"";
                                                                                        } ?>>
            </div>
            <div class="form-group">
                <label for="IdMariageStatus">Situation matrimoniale</label>
                <select name="IdMariageStatus" id="IdMariageStatus" class="form-control">
                    <?php foreach ($MariageStatus as $ms) { ?>
                        <option <?php if (isset($employee) && $employee['IdMariageStatus'] == $ms['IdMariageStatus']) {
                                    echo "selected =\" selected\"";
                                }  ?> value="<?= $ms['IdMariageStatus'] ?>">
                            <?= $ms['Description'] ?>
                        </option>
                    <?php } ?>
                </select>
            </div>
            <div class="form-group">
                <label for="IdDepartement">Département</label>
                <select name="IdDepartement" id="IdDepartement" class="form-control">
                    <?php foreach ($Departement as $d) { ?>
                        <option <?php if (isset($employee) && $employee['IdDepartement'] == $d['IdDepartement']) {
                                    echo "selected =\" selected\"";
                                }  ?> value="<?= $d['IdDepartement'] ?>">
                            <?= $d['Name'] ?>
                        </option>
                    <?php } ?>
                </select>
            </div>
            <div class="form-group">
                <label for="EmployeeStatus">Statut </label>
                <select name="EmployeeStatus" id="EmployeeStatus" class="form-control">
                    <?php foreach ($employeeStatus as $es) { ?>
                        <option <?php if (isset($employee) && $employee['IdEmployeeStatus'] == $es['IdEmployeeStatus']) {
                                    echo "selected =\" selected\"";
                                }  ?> value="<?= $es['IdEmployeeStatus'] ?>">
                            <?= $es['EmployeeStatus'] ?>
                        </option>
                    <?php } ?>
                </select>
            </div>
            <div class="form-group">
                <label for="Fonction">Fonction</label>
                <input class="form-control" type="text" name="Fonction" id="Fonction" <?php if (isset($employee)) {
                                                                                            echo "value =\"" . $employee['Fonction'] . "\"";
                                                                                        } ?>>
            </div>
            <div class="form-group">
                <label for="HiringDate">Date d'embauche</label>
                <input class="form-control" type="date" name="HiringDate" id="HiringDate" <?php if (isset($employee)) {
                                                                                                echo "value =\"" . $employee['HiringDate'] . "\"";
                                                                                            } ?>>
            </div>
            <button class="btn btn-dark" type="submit">Valider</button>
        </form>
    </div>
    <script src="<?php echo site_url('assets/bootstrap/js/jquery.min.js'); ?>"></script>
    <script src="<?php echo site_url('assets/bootstrap/js/bootstrap.min.js'); ?>"></script>

</body>

</html>