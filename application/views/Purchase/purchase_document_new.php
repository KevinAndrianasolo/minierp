<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>Document d'achat</title>
    <link rel="stylesheet" type="text/css" href="<?php echo site_url('assets/bootstrap/css/bootstrap.min.css'); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo site_url('assets/css/Form.css'); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo site_url('assets/fontawesome-4.15/css/font-awesome.min.css'); ?>">

</head>

<body>
    <div class="container document-wrap">
        <h1 class="display-4 document-title">Nouveau document d'achat : </h1>
        <form method="post" action="<?php echo site_url('PurchaseDocument/Insert'); ?>">
            <div class="document-form-information">
                <input type="hidden" name="NbItems" id="NbItems" value="0" />
                <div class="form-group">
                    <label for="idPurchaseDocumentType">Type de document</label>
                    <select class="form-control" id="idPurchaseDocumentType" name="idPurchaseDocumentType">
                        <?php for ($i = 0; $i < count($PurchaseDocumentType); $i++) { ?>
                            <option value="<?php echo $PurchaseDocumentType[$i]["idPurchaseDocumentType"]; ?>"><?php echo $PurchaseDocumentType[$i]["Name"]; ?></option>
                        <?php } ?>

                    </select>
                </div>
                <div class="form-group">
                    <label for="idPurchase">Achat</label>
                    <select class="form-control" id="idPurchase" name="idPurchase">
                        <option value=""></option>
                        <?php for ($i = 0; $i < count($Purchases); $i++) { ?>
                            <option value="<?php echo $Purchases[$i]["idPurchase"]; ?>"><?php echo $Purchases[$i]["Reference"]; ?></option>
                        <?php } ?>

                    </select>
                </div>
                <div class="form-group">
                    <label for="idProvider">Fournisseur</label>
                    <select class="form-control" id="idProvider" name="idProvider">
                        <?php for ($i = 0; $i < count($Providers); $i++) { ?>
                            <option value="<?php echo $Providers[$i]["idProvider"]; ?>"><?php echo $Providers[$i]["Name"]; ?></option>
                        <?php } ?>
                    </select>
                </div>
                <div class="form-group">
                    <label for="DocumentDate">Date du document</label>
                    <input type="date" class="form-control" name="DocumentDate" required />
                </div>
            </div>
            <div class="document-form-information">
                <div class="form-group">
                    <label for="GlobalDiscount">Remise Global (en %)</label>
                    <input type="text" class="form-control" name="GlobalDiscount" id="GlobalDiscount" value="0" onchange="RefreshDocument()" required />
                </div>
                <div class="form-group">
                    <label for="idCurrency">Devise</label>
                    <select class="form-control" id="idCurrency" name="idCurrency" onchange="ChangerDevise()">
                        <?php for ($i = 0; $i < count($CurrencyList); $i++) { ?>
                            <option value="<?php echo $CurrencyList[$i]["idCurrency"]; ?>"><?php echo $CurrencyList[$i]["Currency"]; ?></option>
                        <?php } ?>
                    </select>
                </div>
            </div>
            <div class="document-flex">
                <button type="button" onclick="AddNewItem()" class="btn btn-dark document-button-add-item">Ajouter nouvel item <i class="fa fa-plus"></i></button>
                <div class="document-resume">
                    <p>Montant HT : <span id="MontantHT">0</span> <span class="devise-value"></span></p>
                    <p>Net remisé: <span id="NetRemise">0</span> <span class="devise-value"></span></p>
                    <p>TVA(<span id="tva-percent"></span> %) : <span id="tva-value"></span> <span class="devise-value"></span></p>
                    <p>Montant TTC : <span id="MontantTTC">0</span> <span class="devise-value"></span></p>
                </div>

                <button type="submit" class="btn btn-warning">Créer le document</button>
            </div>
            </br>
            <div class="table-responsive document-table">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th scope="col">Article</th>
                            <th scope="col">Quantité</th>
                            <th scope="col">Unité</th>
                            <th scope="col">Prix Unitaire</th>
                            <th scope="col">Réduction</th>
                            <th scope="col">Montant</th>
                            <th scope="col"></th>

                        </tr>
                    </thead>
                    <tbody id="ItemsWrap">
                    </tbody>
                </table>
                <?php if (isset($error)) { ?>
                    <div class="alert alert-danger" role="alert">
                        <?php echo $error; ?>
                    </div>
                <?php } ?>
            </div>
        </form>
    </div>
    <script type="text/javascript">
        let Items = <?php echo json_encode($Items); ?>;
        let UnitList = <?php echo json_encode($UnitList); ?>;
        let CurrencyList = <?php echo json_encode($CurrencyList); ?>;

        let ItemWrap = document.getElementById('ItemsWrap');
        let NbItems = 0;

        let tva_percent = document.getElementById("tva-percent");
        tva_percent.innerHTML = <?php echo json_encode($TVA); ?>;

        function getCurrency(idCurrency) {
            for (i = 0; i < CurrencyList.length; i++) {
                if (CurrencyList[i]["idCurrency"] == idCurrency) return CurrencyList[i];
            }
            return null;
        }

        function ChangerDevise() {
            let idCurrency = document.getElementById("idCurrency").value;
            let Currency = getCurrency(idCurrency);
            console.log(Currency);
            let tab = document.getElementsByClassName("devise-value");
            for (i = 0; i < tab.length; i++) {
                tab[i].innerHTML = Currency["Currency"];
            }
        }

        function SetMontantOf(indiceItem) {
            let idItem = document.getElementById("idItem_" + indiceItem).value;
            let Quantity = document.getElementById("Quantity_" + indiceItem).value;
            let Unit = document.getElementById("idUnit_" + indiceItem).value;
            let UnitPrice = document.getElementById("UnitPrice_" + indiceItem).value;
            let Reduction = document.getElementById("Reduction_" + indiceItem).value;


            if (idItem && Quantity && Unit && UnitPrice && Reduction) {
                let Montant = Quantity * UnitPrice * (1 - (Reduction / 100));
                document.getElementById("Montant_" + indiceItem).value = Montant.toFixed(3);
                RefreshDocument();
            }
        }

        function RefreshDocument() {
            let GlobalDiscount = document.getElementById("GlobalDiscount").value;
            let MontantHT_Element = document.getElementById("MontantHT");
            let NetRemise_Element = document.getElementById("NetRemise");
            let MontantTTC_Element = document.getElementById("MontantTTC");
            let tva_value = document.getElementById("tva-value");

            if (GlobalDiscount) {
                let MontantHT = 0;
                for (let i = 0; i < NbItems; i++) {
                    let input = document.getElementById("Montant_" + i);
                    if (!input) continue;
                    MontantHT += parseFloat(input.value);
                }
                let NetRemise = MontantHT * (1 - (GlobalDiscount) / 100);
                let TVA = <?php echo json_encode($TVA); ?>;
                let montantTVA = (NetRemise * TVA / 100);
                let MontantTTC = NetRemise + montantTVA;

                tva_value.innerHTML = montantTVA;
                MontantHT_Element.innerHTML = MontantHT.toFixed(3);
                NetRemise_Element.innerHTML = NetRemise.toFixed(3);
                MontantTTC_Element.innerHTML = MontantTTC.toFixed(3);
            }
        }

        function DeleteItem(idItem) {
            document.getElementById("Item_" + idItem).remove();
            RefreshDocument();
        }

        function AddNewItem() {
            let tr = document.createElement('tr');
            tr.setAttribute('id', 'Item_' + NbItems);
            tr.setAttribute('data-index', NbItems);


            // IdItem field : 
            let tdIdItem = document.createElement('td');
            let selectItem = document.createElement('select');
            selectItem.setAttribute('name', 'idItem_' + NbItems);
            selectItem.setAttribute('id', 'idItem_' + NbItems);
            selectItem.setAttribute('class', 'form-control');
            for (let i = 0; i < Items.length; i++) {
                let optionItem = document.createElement('option');
                optionItem.setAttribute('value', Items[i]["idItem"]);
                let value = document.createTextNode(Items[i]["Designation"]);
                optionItem.appendChild(value);
                selectItem.appendChild(optionItem);
            }
            tdIdItem.appendChild(selectItem);

            // Quantity
            let tdQuantity = document.createElement('td');
            let inputQuantity = document.createElement('input');
            inputQuantity.setAttribute('class', 'form-control');
            inputQuantity.setAttribute('type', 'number');
            inputQuantity.setAttribute('name', 'Quantity_' + NbItems);
            inputQuantity.setAttribute('id', 'Quantity_' + NbItems);
            inputQuantity.setAttribute('required', 'true');
            inputQuantity.setAttribute('onchange', `SetMontantOf(${NbItems})`);
            tdQuantity.appendChild(inputQuantity);

            // IdUnit field : 
            let tdIdUnit = document.createElement('td');
            let selectUnit = document.createElement('select');
            selectUnit.setAttribute('name', 'idUnit_' + NbItems);
            selectUnit.setAttribute('id', 'idUnit_' + NbItems);
            selectUnit.setAttribute('class', 'form-control');
            for (let i = 0; i < UnitList.length; i++) {
                let optionUnit = document.createElement('option');
                optionUnit.setAttribute('value', UnitList[i]["idUnit"]);
                let value = document.createTextNode(UnitList[i]["Unit"]);
                optionUnit.appendChild(value);
                selectUnit.appendChild(optionUnit);
            }
            tdIdUnit.appendChild(selectUnit);

            // UnitPrice
            let tdUnitPrice = document.createElement('td');
            let inputUnitPrice = document.createElement('input');
            inputUnitPrice.setAttribute('class', 'form-control');
            inputUnitPrice.setAttribute('type', 'text');
            inputUnitPrice.setAttribute('name', 'UnitPrice_' + NbItems);
            inputUnitPrice.setAttribute('id', 'UnitPrice_' + NbItems);
            inputUnitPrice.setAttribute('required', 'true');
            inputUnitPrice.setAttribute('onchange', `SetMontantOf(${NbItems})`);
            tdUnitPrice.appendChild(inputUnitPrice);

            // Reduction
            let tdReduction = document.createElement('td');
            let inputReduction = document.createElement('input');
            inputReduction.setAttribute('class', 'form-control');
            inputReduction.setAttribute('type', 'text');
            inputReduction.setAttribute('name', 'Reduction_' + NbItems);
            inputReduction.setAttribute('id', 'Reduction_' + NbItems);
            inputReduction.setAttribute('required', 'true');
            inputReduction.setAttribute('onchange', `SetMontantOf(${NbItems})`);
            tdReduction.appendChild(inputReduction);

            // Montant
            let tdMontant = document.createElement('td');
            let inputMontant = document.createElement('input');
            inputMontant.setAttribute('class', 'form-control');
            inputMontant.setAttribute('type', 'text');
            inputMontant.setAttribute('name', 'Montant_' + NbItems);
            inputMontant.setAttribute('id', 'Montant_' + NbItems);
            inputMontant.setAttribute('disabled', 'true');
            tdMontant.appendChild(inputMontant);

            // DeleteButton
            let tdDeleteButton = document.createElement('td');
            let deleteButton = document.createElement('button');
            deleteButton.setAttribute("class", "btn btn-danger");
            deleteButton.setAttribute("onclick", `DeleteItem(${NbItems})`);
            deleteButton.appendChild(document.createTextNode("Supprimer"));
            tdDeleteButton.appendChild(deleteButton);

            tr.appendChild(tdIdItem);
            tr.appendChild(tdQuantity);
            tr.appendChild(tdIdUnit);
            tr.appendChild(tdUnitPrice);
            tr.appendChild(tdReduction);
            tr.appendChild(tdMontant);
            tr.appendChild(tdDeleteButton);


            ItemWrap.appendChild(tr);

            NbItems++;
            document.getElementById("NbItems").value = NbItems;
        }
        AddNewItem();

        ChangerDevise();
    </script>
    <script src="<?php echo site_url('assets/bootstrap/js/jquery.min.js'); ?>"></script>
    <script src="<?php echo site_url('assets/bootstrap/js/bootstrap.min.js'); ?>"></script>

</body>

</html>