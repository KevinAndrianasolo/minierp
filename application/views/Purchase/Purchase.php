<?php
    defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Purchase</title>
        <link rel="stylesheet" type="text/css" href="<?php echo site_url('assets/bootstrap/css/bootstrap.min.css'); ?>">

        <link rel="stylesheet" type="text/css" href="<?php echo site_url('assets/css/font.css'); ?>">
        <link rel="stylesheet" type="text/css" href="<?php echo site_url('assets/css/stock.css'); ?>">

        <script src="<?php echo site_url('assets/bootstrap/js/jquery.min.js'); ?>"></script>
        <script src="<?php echo site_url('assets/bootstrap/js/bootstrap.min.js'); ?>"></script>
    </head>
    <body>
        <div class="main-content">
            <h1 class="display-4 title">Liste des achats <a href="Purchase/Form" class="btn btn-success">Ajouter</a>  </h1>
          
            <div class="form-wrap">
                <form>
                    <div class="form-group">
                        <label>Services</label>
                        <select class="form-control" id="idService" name="idService">
                            <option value="-1">Tous les services</option>    
                            <?php for($i=0;$i<count($Services);$i++) { ?>    
                            <option value="<?php echo $Services[$i]["idService"]; ?>"><?php echo $Services[$i]["ServiceName"]; ?></option>
                            <?php } ?>
                            
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Etat</label>
                        <select class="form-control" id="idStatus" name="idStatus">
                            <option value="-1">Tous</option>    
                            <?php for($i=0;$i<count($PurchaseStatus);$i++) { ?>    
                            <option value="<?php echo $PurchaseStatus[$i]["idPurchaseStatus"]; ?>"><?php echo $PurchaseStatus[$i]["Status"]; ?></option>
                            <?php } ?>
                            
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Date de l'achat</label>
                        <input name="PurchaseDate" type="date" class="form-control"/>
                    </div>
                    <button class="btn btn-warning">Filtrer</button>
                </form>
            </div>
            <br/>
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th scope="col">Reférence</th>
                        <th scope="col">Service</th>
                        <th scope="col">Etat</th>
                        <th scope="col">Date de l'achat</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                <?php for($i=0;$i<count($Purchases);$i++) { ?>
                    <tr>
                        <th scope="row"><?php echo $Purchases[$i]["Reference"] ?></th>
                        <td><?php echo $Purchases[$i]["ServiceName"] ?></td>
                        <td><?php echo $Purchases[$i]["Status"] ?></td>
                        <td><?php echo $Purchases[$i]["PurchaseDate"] ?></td>
                        <td><a href="<?php echo site_url('PurchaseDocument?idPurchase=3'); ?>">Voir les détails</a></td>
                    </tr>
                <?php } ?>
                   
                </tbody>
                </table>
        </div>
    </body>
</html>