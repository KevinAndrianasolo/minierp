<?php
    defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Purchase document</title>
        <link rel="stylesheet" type="text/css" href="<?php echo site_url('assets/bootstrap/css/bootstrap.min.css'); ?>">

        <link rel="stylesheet" type="text/css" href="<?php echo site_url('assets/css/font.css'); ?>">
        <link rel="stylesheet" type="text/css" href="<?php echo site_url('assets/css/stock.css'); ?>">

        <script src="<?php echo site_url('assets/bootstrap/js/jquery.min.js'); ?>"></script>
        <script src="<?php echo site_url('assets/bootstrap/js/bootstrap.min.js'); ?>"></script>
    </head>
    <body>
        <div class="main-content">
            <h1 class="display-4 title">Documents d'achat</h1>
            <div>
                <a class="btn btn-primary" href="<?php echo site_url('PurchaseDocument/Form'); ?>">
                Nouveau document d'achat
                </a>    
            </div>
            <div class="form-wrap">
                <form>
                    <div class="form-group">
                        <label for="idPurchaseDocumentType">Type de document</label>
                        <select class="form-control" id="idTypeDocument" name="idTypeDocument">
                            <option value="-1">Tous les documents</option>    
                            <?php for($i=0;$i<count($PurchaseDocumentType);$i++) { ?>    
                            <option value="<?php echo $PurchaseDocumentType[$i]["idPurchaseDocumentType"]; ?>"><?php echo $PurchaseDocumentType[$i]["Name"]; ?></option>
                            <?php } ?>
                            
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="idProvider">Fournisseurs</label>
                        <select class="form-control" id="idProvider" name="idProvider">
                            <option value="-1">Tous les fournisseurs</option>    
                            <?php for($i=0;$i<count($Providers);$i++) { ?>    
                            <option value="<?php echo $Providers[$i]["idProvider"]; ?>"><?php echo $Providers[$i]["Name"]; ?></option>
                            <?php } ?>
                            
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="idPurchase">Achat</label>
                        <select class="form-control" id="idPurchase" name="idPurchase">
                            <option value="-1">Tous les achats</option>    
                            <?php for($i=0;$i<count($Purchasess);$i++) { ?>    
                            <option value="<?php echo $Purchasess[$i]["idPurchase"]; ?>"><?php echo $Purchasess[$i]["Reference"]; ?></option>
                            <?php } ?>
                            
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="DocumentDate">Date du document</label>
                        <input type="date" class="form-control" name="DocumentDate"/>
                    </div>
                    <button type="submit" class="btn btn-warning">Filtrer</button>
                </form>
            </div>
                <table class="table table-bordered">
                <thead>
                    <tr> 
                        <th scope="col">Type du document</th>
                        <th scope="col">Achat</th>
                        <th scope="col">Reference</th>
                        <th scope="col">Fournisseur</th>
                        <th scope="col">Date du document</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                <?php for($i=0;$i<count($PurchaseDocument);$i++) { ?>
                    <tr>
                        <th scope="row"><?php echo $PurchaseDocument[$i]["typeDocument"] ?></th>
                        <td><?php echo $PurchaseDocument[$i]["PurchaseReference"] ?></td>
                        <td><?php echo $PurchaseDocument[$i]["Reference"] ?></td>
                        <td><?php echo $PurchaseDocument[$i]["ProviderName"] ?></td>
                        <td><?php echo $PurchaseDocument[$i]["DocumentDate"] ?></td>
                        <td><a href="<?php echo site_url('PurchaseDocument/index/'.$PurchaseDocument[$i]["idPurchaseDocument"]); ?>">Voir détails</a></td>
                    </tr>
                <?php } ?>
                   
                </tbody>
            </table>
        </div>
    </body>
</html>