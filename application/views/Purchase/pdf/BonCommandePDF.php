<?php
require('fpdf.php');
require('viewHelper.php');

class BonCommandePDF extends FPDF
{
    // En-t�te
    function Header()
    {
       // Logo
       $this->Image('logo.png', 170, 13, 35, 20);

       //EntrepriseInformation
       $this->SetFont('Arial', 'BI', 11);
       $this->Cell(10, 5, "Kevin Corporation"); // Nom
       $this->Ln(6);
       $this->SetFont('Arial', '', 7);

       $this->Cell(20, 2, "Adresse Antananarivo, 103"); // Adresse
       $this->Ln(4);

       $this->SetFont('Arial', 'U', 7);
       $this->Cell(18, 2,format_text("Téléphone :"));

       $this->SetFont('Arial', '', 7);
       $this->Cell(20, 2, "+261 34 53 564 71", 0, 0, 'R'); // Contact

       $this->Ln(4);

       $this->SetFont('Arial', '', 7);
       $this->Cell(10, 2, "CIF : 022230 /DGI-G", 0, 0, 'L'); // Contact

       $this->Ln(4);

       $this->SetFont('Arial', '', 7);
       $this->Cell(10, 2, "Stat : 24101 11 2000 0 10023 du 01/01/2000", 0, 0, 'L'); // Contact

       $this->Ln(4);

       $this->SetFont('Arial', '', 7);
       $this->Cell(10, 2, "NIF : 10 0000 22 111 du 01/01/2000", 0, 0, 'L'); // Contact
       $this->Ln(4);

       $this->SetFont('Arial', '', 7);
       $this->Cell(10, 2, "RCS : 2000 B 00141 du 24/03/2000", 0, 0, 'L'); // Contact

       $this->Ln(15);

       $this->SetFont('Arial', 'B', 14);

        //SalesDocument Information
        $this->SetTextColor(61, 118, 166);
        $this->Cell(190, 10, "Bon de commande (BCOOO1-21/21-K_Corp.)", 0, 0, 'C'); //Reference
        $this->SetTextColor(0, 0, 0);

        $this->Ln(8);
        $this->Cell(70); // Décalage à droite
        $this->SetFont('Arial', 'U', 11);
        $this->Cell(10, 10, format_text("Date édition"), 0, 0, 'C');

        $this->SetFont('Arial', '', 11);
        $this->Cell(45, 10, " 03-05-2021 12:24:18", 0, 0, 'R'); // Date edition
        $this->Ln(15);

        //Provider Information
        $this->Cell(80); // Décalage à droite

        $this->SetFont('Arial', 'BI', 14);
        $this->Cell(30, 10, 'Fournisseur : Japan Actuel\'s', 0, 0, 'C'); // Nom fournisseur
        $this->Ln(6);

        $this->Cell(80); // Décalage à droite
        $this->SetFont('Arial', '', 10);
        $this->Cell(35, 10, "Lot II D 2 Bis Manjakaray, 101", 0, 0, 'C'); // Adresse Fournisseur
        $this->Ln(6);

        $this->Cell(72); // Décalage à droite
        $this->SetFont('Arial', 'U', 10);
        $this->Cell(25, 10, format_text("Téléphone :"));

        $this->SetFont('Arial', '', 10);
        $this->Cell(26, 10, "+261 34 53 564 71", 0, 0, 'R'); // Contact
        $this->Ln(20);
    }
    function ContentTable()
    {
        $header = array(format_text('Désignation'), format_text('Unité'), format_text('Quantite'), 'Prix unitaire', format_text('Remise'), 'Montant');
        $data = $this->LoadData('pays.txt');
        // Couleurs, épaisseur du trait et police grasse
        $this->SetFillColor(182, 186, 183);
        $this->SetTextColor(0);
        $this->SetDrawColor(61, 118, 166);
        $this->SetLineWidth(.1);
        $this->SetFont('', 'B');
        // En-tête
        $w = array(50, 20, 20, 30, 30, 40);
        for ($i = 0; $i < count($header); $i++)
            $this->Cell($w[$i], 7, $header[$i], 1, 0, 'C', true);
        $this->Ln();
        // Restauration des couleurs et de la police
        $this->SetFillColor(224, 235, 255);
        $this->SetTextColor(0);
        $this->SetFont('');
        // Données
        $fill = false;
        foreach ($data as $row) {
            $this->Cell($w[0], 6, $row[0], 'LRB', 0, 'L', $fill); // Designation
            $this->Cell($w[1], 6, $row[1], 'LRB', 0, 'C', $fill); // Unite
            $this->Cell($w[2], 6, $row[2], 'LRB', 0, 'C', $fill); // Quantite
            $this->Cell($w[3], 6, format_number($row[3]), 'LRB', 0, 'C', $fill); // Prix unitaire
            $this->Cell($w[4], 6, $row[4], 'LRB', 0, 'C', $fill); // Reduction
            $this->Cell($w[5], 6, format_number($row[5]), 'LRB', 0, 'C', $fill); //Montant
            $this->Ln();
        }
        $this->Cell($w[0] + $w[1] + $w[2] + $w[3]);
        $this->Cell($w[4], 6, 'Total HT', 'LRB', 0, 'C', $fill); // Total HT
        $this->Cell($w[5], 6, '50 000', 'LRB', 0, 'C', $fill);

        $this->Ln();

        $this->Cell($w[0] + $w[1] + $w[2] + $w[3]);
        $this->Cell($w[4], 6, 'Remise globale %', 'LRB', 0, 'C', $fill);
        $this->Cell($w[5], 6, '25', 'LRB', 0, 'C', $fill);

        $this->Ln();

        $this->Cell($w[0] + $w[1] + $w[2] + $w[3]);
        $this->Cell($w[4], 6, format_text('HT remisé %'), 'LRB', 0, 'C', $fill);
        $this->Cell($w[5], 6, '37 500', 'LRB', 0, 'C', $fill);

        $this->Ln();

        $this->Cell($w[0] + $w[1] + $w[2] + $w[3]);
        $this->Cell($w[4], 6, 'TVA %', 'LRB', 0, 'C', $fill);
        $this->Cell($w[5], 6, '20', 'LRB', 0, 'C', $fill);

        $this->Ln();

        $this->Cell($w[0] + $w[1] + $w[2] + $w[3]);
        $this->Cell($w[4], 6, 'Montant TTC', 'LRB', 0, 'C', $fill);
        $this->Cell($w[5], 6, '45 000', 'LRB', 0, 'C', $fill);
    }

    function Signature(){
        $this->Ln(30);
        $this->Cell(60);
        $this->SetFont('Arial','UI',13);
        $this->Cell(60,10,'Fournisseur');
        $this->Cell(60,10,format_text('La Société'));
        
        $this->SetFont('Arial','',11);


    }
    
    function LoadData($file)
    {
        // Lecture des lignes du fichier
        $lines = file($file);
        $data = array();
        foreach ($lines as $line)
            $data[] = explode(';', trim($line));
        return $data;
    }
}
