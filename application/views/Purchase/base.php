<?php
require_once 'pdf/BasePDF.php';
$montantHT = 0;
foreach ($result['DocumentItems'] as $r) {
    $montantHT += ($r['UnitPrice'] * $r['Quantity']) - ($r['UnitPrice'] * $r['Quantity'] * ($r['Reduction'] / 100));
}
$remiseGlobale = $result['PurchaseDocument']['GlobalDiscount'];
$montantHTRemise = $montantHT - ($montantHT * ($remiseGlobale / 100));
$tva = $result['TVA'];

$montantTTC = $montantHTRemise + ($montantHTRemise * ($tva / 100));

$result['montantHT'] = $montantHT;
$result['remiseGlobable'] = $remiseGlobale;
$result['montantHTRemise'] = $montantHTRemise;
$result['tva'] = $tva;
$result['montantTTC'] = $montantTTC;
$pdf = new BasePDF();
$pdf->AddPage();
$pdf->Content($result);
$pdf->Output();
