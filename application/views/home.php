<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script src="assets/bootstrap/js/bootstrap.min.js"></script>
    <script src="assets/bootstrap/js/jquery.min.js"></script>
    <link href="assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <link rel="stylesheet" type="text/css" href="<?php echo site_url('assets/css/font.css'); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo site_url('assets/css/style.css'); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo site_url('assets/fontawesome/css/font-awesome.min.css'); ?>">
    <title>Mini ERP</title>
</head>

<body>
    <section id="what-we-do">
        <div class="container-fluid">
            <h1 class="section-title mb-2" style="font-family: 'BigJohn';">Mini ERP</h1>
            <p class="text-center text-muted h5">Application de gestion</p>
            <div class="row mt-5">
                <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 col-xl-4" onclick="location.href='Provider';" style="cursor: pointer;">
                    <div class="card">
                        <div class="card-block block-fournisseur">
                            <h3 class="card-title">Fournisseurs</h3>
                            <p class="card-text">Liste des fournisseurs<br> Formulaire d'insertion/modification<br> Suppression</p>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 col-xl-4" onclick="location.href='Customer';" style="cursor: pointer;">
                    <div class="card">
                        <div class="card-block block-2">
                            <h3 class="card-title">Clients</h3>
                            <p class="card-text">Liste des clients<br> Formulaire d'insertion/modification<br> Suppression</p>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 col-xl-4" onclick="location.href='Item';" style="cursor: pointer;">
                    <div class="card">
                        <div class="card-block block-3">
                            <h3 class="card-title">Articles</h3>
                            <p class="card-text">Liste des articles<br> Formulaire d'insertion/modification<br> Suppression</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 col-xl-4" onclick="location.href='Employee';" style="cursor: pointer;">
                    <div class="card">
                        <div class="card-block block-1">
                            <h3 class="card-title">Employés</h3>
                            <p class="card-text">Liste des employés<br> Formulaire d'insertion/modification<br> Suppression</p>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 col-xl-4" onclick="location.href='TVA';" style="cursor: pointer;">
                    <div class="card">
                        <div class="card-block block-1">
                            <h3 class="card-title">TVA</h3>
                            <p class="card-text">Historique TVA<br> Formulaire d'insertion</p>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 col-xl-4" onclick="location.href='Service';" style="cursor: pointer;">
                    <div class="card">
                        <div class="card-block block-1">
                            <h3 class="card-title">Service</h3>
                            <p class="card-text">Liste des services<br> Formulaire d'insertion/modification<br> Suppression</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <script src="<?php echo site_url('assets/bootstrap/js/jquery.min.js'); ?>"></script>
    <script src="<?php echo site_url('assets/bootstrap/js/bootstrap.min.js'); ?>"></script>
</body>

</html>
