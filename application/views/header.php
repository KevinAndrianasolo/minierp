<header>
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <a class="navbar-brand" href="<?= base_url() ?>">Mini ERP</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item">
                    <a class="nav-link" href="<?= base_url() ?>">CRUD<span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="<?= site_url('StockManagement') ?>">Stock</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="<?= site_url('ImmoManagement') ?>">Immobilisation</a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Ressources Humaines
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="<?= site_url('Employee/PayStubForm') ?>">Fiche de paie</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="<?= site_url('Employee/ExtraHour') ?>">Heures supplémentaires</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="<?= site_url('Employee/ExtraHourValue') ?>">Prix heures supplémentaires</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="<?= site_url('Employee/Bonus') ?>">Avantages</a>
                       
                    </div>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Procédure d'achat
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="<?= site_url('Purchase') ?>">Liste des achats</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="<?= site_url('PurchaseDocument') ?>">Document d'achat</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="<?= site_url('Purchase/Form') ?>">Nouvelle demande d'achat</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="<?= site_url('Stats/Purchase') ?>">Statistiques d'achat</a>
                    
                    </div>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Procédure de vente
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="<?= site_url('Sale') ?>">Liste des ventes</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="<?= site_url('SalesDocument') ?>">Document de vente</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="<?= site_url('Sale/Form') ?>">Nouvelle vente</a>
                    </div>
                </li>
            </ul>
        </div>
    </nav>
</header>
