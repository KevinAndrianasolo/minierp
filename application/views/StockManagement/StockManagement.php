<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>Stock Management</title>
    <link rel="stylesheet" type="text/css" href="<?php echo site_url('assets/bootstrap/css/bootstrap.min.css'); ?>">

    <link rel="stylesheet" type="text/css" href="<?php echo site_url('assets/css/font.css'); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo site_url('assets/css/stock.css'); ?>">

    <script src="<?php echo site_url('assets/bootstrap/js/jquery.min.js'); ?>"></script>
    <script src="<?php echo site_url('assets/bootstrap/js/bootstrap.min.js'); ?>"></script>



</head>

<body>
    <div class="main-content">
        <h1 class="display-4 title">Gestion de stock : </h1>
        <h2 class="display-4 subtitle">(pour l'article <b><?php echo $Item['Designation']; ?> ~ <?php echo $Item['Reference']; ?></b>)</h1>
            <div>
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#ResteModal">
                    Voir reste en stock
                </button>
            </div>
            </br>
            <div class="form-wrap">
                <ul class="nav nav-tabs" id="myTab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="entree-tab" data-toggle="tab" href="#entree" role="tab" aria-controls="entree" aria-selected="true">Entrée</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="sortie-tab" data-toggle="tab" href="#sortie" role="tab" aria-controls="sortie" aria-selected="false">Sortie</a>
                    </li>
                </ul>
                <div class="tab-content" id="myTabContent">
                    <div class="tab-pane fade show active" id="entree" role="tabpanel" aria-labelledby="entree-tab">
                        <form action="<?php echo site_url('StockManagement/InsertEntree'); ?>" method="post">
                            <input class="form-control" type="hidden" name="idItem" value="<?php echo $idItem; ?>" />
                            <input class="form-control" type="hidden" name="idStockManagementMethod" value="<?php echo $idStockManagementMethod; ?>" />
                            <input class="form-control" type="hidden" name="DateStock" value="<?php echo $Date; ?>" />
                            <div class="form-group">
                                <label>Quantité</label>
                                <input class="form-control" type="text" name="Quantity" />
                            </div>
                            <div class="form-group">
                                <label>Prix unitaire</label>
                                <input class="form-control" type="text" name="UnitPrice" />
                            </div>
                            <div class="form-group">
                                <label>Date de l'entrée</label>
                                <input class="form-control" type="date" name="Date" />
                                <input class="form-control" type="time" name="Heure" />

                            </div>
                            <button class="btn btn-warning">Insérer</button>
                        </form>
                    </div>
                    <div class="tab-pane fade" id="sortie" role="tabpanel" aria-labelledby="sortie-tab">
                        <form action="<?php echo site_url('StockManagement/InsertSortie'); ?>" method="post">
                            <input class="form-control" type="hidden" name="idItem" value="<?php echo $idItem; ?>" />
                            <input class="form-control" type="hidden" name="idStockManagementMethod" value="<?php echo $idStockManagementMethod; ?>" />
                            <input class="form-control" type="hidden" name="DateStock" value="<?php echo $Date; ?>" />

                            <div class="form-group">
                                <label>Quantité</label>
                                <input class="form-control" type="text" name="Quantity" />
                            </div>
                            <div class="form-group">
                                <label>Date sortie</label>
                                <input class="form-control" type="date" name="Date" />
                                <input class="form-control" type="time" name="Heure" />

                            </div>
                            <button class="btn btn-warning">Insérer</button>
                        </form>
                    </div>
                </div>
                <?php if (isset($error)) { ?>
                    <div class="alert alert-danger mt-2" role="alert">
                        <?php echo $error; ?>
                    </div>
                <?php } ?>

            </div>
            <br />
            <div class="form-wrap-invert">
                <h2 class="title"> Choix : </h2>
                <form action="<?php echo site_url('StockManagement'); ?>" method="get">
                    <input class="form-control" type="hidden" name="idItem" value="<?php echo $idItem; ?>" />

                    <div class="form-group">
                        <select class="form-control" name="idStockManagementMethod">
                            <?php for ($i = 0; $i < count($StockManagementMethod); $i++) { ?>
                                <option value="<?php echo $StockManagementMethod[$i]["idStockManagementMethod"]; ?>" <?php if ($idStockManagementMethod == $StockManagementMethod[$i]["idStockManagementMethod"]) {
                                                                                                                            echo "selected";
                                                                                                                        } ?>><?php echo $StockManagementMethod[$i]["Name"]; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <input class="form-control" type="date" name="Date" value="<?php echo $Date; ?>" />
                    </div>
                    <button class="btn btn-warning">Voir</button>
                </form>
            </div>
            <h2>Mouvements</h2>
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th scope="col">Date du Mouvement</th>
                        <th scope="col">Quantité</th>
                        <th scope="col">Prix Unitaire</th>
                        <th scope="col">Montant</th>
                    </tr>
                </thead>
                <tbody>
                    <?php for ($i = 0; $i < count($Stock->Mouvements); $i++) { ?>
                        <tr>
                            <td><?php echo $Stock->Mouvements[$i]["DateMouvement"] ?></td>
                            <td><?php echo $Stock->Mouvements[$i]["Quantity"] ?></td>
                            <td><?php echo $Stock->Mouvements[$i]["UnitPrice"] ?></td>
                            <td><?php echo $Stock->Mouvements[$i]["Montant"] ?></td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
            <div class="information-wrap">
                <div class="info">Quantité en Stock : <span class="info-value"><?php echo $Stock->QuantiteStock; ?></span></div>
                <div class="info">Valeur du Stock : <span class="info-value"><?php echo $Stock->ValeurStock; ?></span></div>
            </div>
    </div>
    <div class="modal fade" id="ResteModal" tabindex="-1" role="dialog" aria-labelledby="ResteModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="ResteModalLabel">Reste en stock : <b><?php echo $Item['Designation']; ?> ~ <?php echo $Item['Reference']; ?></b></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th scope="col">Quantité</th>
                                <th scope="col">Prix Unitaire</th>
                                <th scope="col">Date</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php for ($i = 0; $i < count($Stock->Reste); $i++) { ?>
                                <tr>
                                    <td><?php echo $Stock->Reste[$i]["Quantity"] ?></td>
                                    <td><?php echo $Stock->Reste[$i]["UnitPrice"] ?></td>
                                    <td><?php echo $Stock->Reste[$i]["DateMouvement"] ?></td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
                </div>
            </div>
        </div>
    </div>
</body>

</html>