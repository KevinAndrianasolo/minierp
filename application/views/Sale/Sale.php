<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>Liste des ventes</title>
    <link rel="stylesheet" type="text/css" href="<?php echo site_url('assets/bootstrap/css/bootstrap.min.css'); ?>">

    <link rel="stylesheet" type="text/css" href="<?php echo site_url('assets/css/font.css'); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo site_url('assets/css/stock.css'); ?>">

    <script src="<?php echo site_url('assets/bootstrap/js/jquery.min.js'); ?>"></script>
    <script src="<?php echo site_url('assets/bootstrap/js/bootstrap.min.js'); ?>"></script>
</head>

<body>
    <div class="main-content">
        <h1 class="display-4 title">Liste des ventes <a href="<?= site_url('Sale/Form') ?>" class="btn btn-success">Ajouter</a> </h1>
        <div class="form-wrap">
            <form>
                <div class="form-group">
                    <label>Etat</label>
                    <select class="form-control" id="idStatus" name="idStatus">
                        <option value="-1">Tous</option>
                        <?php for ($i = 0; $i < count($SaleStatus); $i++) { ?>
                            <option value="<?php echo $SaleStatus[$i]["idSaleStatus"]; ?>"><?php echo $SaleStatus[$i]["Status"]; ?></option>
                        <?php } ?>

                    </select>
                </div>
                <div class="form-group">
                    <label>Date de la vente</label>
                    <input name="SaleDate" type="date" class="form-control" />
                </div>
                <button class="btn btn-warning" style="font-family: 'SlimJoe'; font-size:small;">Filtrer</button>
            </form>
        </div>
        <br />
        <h2>Liste des ventes : </h2>
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th scope="col">Reférence</th>
                    <th scope="col">Etat</th>
                    <th scope="col">Date de l'achat</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <?php for ($i = 0; $i < count($Sales); $i++) { ?>
                    <tr>
                        <th scope="row"><?php echo $Sales[$i]["Reference"] ?></th>
                        <td><?php echo $Sales[$i]["Status"] ?></td>
                        <td><?php echo $Sales[$i]["SaleDate"] ?></td>
                        <td><a href="<?php echo site_url('SalesDocument?idSale=2'); ?>">Voir les détails</a></td>
                    </tr>
                <?php } ?>

            </tbody>
        </table>

    </div>
</body>

</html>