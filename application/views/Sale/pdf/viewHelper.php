<?php
require 'ChiffresEnLettres.php';
function format_number($number)
{
    return number_format($number, 0, ',', ' ');
}

function format_text($text)
{
    return iconv('UTF-8', 'windows-1252', $text);
}

function intToStr($number)
{
    $letter = new ChiffreEnLettre();
    return $letter->Conversion($number);
}