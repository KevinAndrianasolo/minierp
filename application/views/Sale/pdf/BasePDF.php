<?php
require('fpdf.php');
require('viewHelper.php');

class BasePDF extends FPDF
{

    function Content($data)
    {
        // Logo
        $title = preg_replace('/\s+/', "-", strtolower($data["SalesDocumentType"]["Name"])) . "-" . $data["SalesDocument"]["Reference"];
        $this->Image($data["EntrepriseInformation"]["LogoPath"], 170, 13, 35, 20);
        $this->SetTitle($title, true);
        //EntrepriseInformation
        $this->SetFont('Arial', 'BI', 11);
        $this->Cell(10, 5, $data["EntrepriseInformation"]["EntrepriseName"]); // Nom
        $this->Ln(6);
        $this->SetFont('Arial', '', 7);

        $this->Cell(20, 2, $data["EntrepriseInformation"]["Address"]); // Adresse
        $this->Ln(4);

        $this->SetFont('Arial', 'U', 7);
        $this->Cell(18, 2, format_text("Téléphone :"));

        $this->SetFont('Arial', '', 7);
        $this->Cell(15, 2, $data["EntrepriseInformation"]["Phone"], 0, 0, 'R'); // Contact

        $this->Ln(4);

        $this->SetFont('Arial', '', 7);
        $this->Cell(10, 2, "CIF : " . $data["EntrepriseInformation"]["CIF"], 0, 0, 'L'); // CIF

        $this->Ln(4);

        $this->SetFont('Arial', '', 7);
        $this->Cell(10, 2, "STAT : " . $data["EntrepriseInformation"]["STAT"], 0, 0, 'L'); // STAT

        $this->Ln(4);

        $this->SetFont('Arial', '', 7);
        $this->Cell(10, 2, "NIF : " . $data["EntrepriseInformation"]["NIF"], 0, 0, 'L'); // NIF
        $this->Ln(4);

        $this->SetFont('Arial', '', 7);
        $this->Cell(10, 2, "RCS : " . $data["EntrepriseInformation"]["RCS"], 0, 0, 'L'); // RCS

        $this->Ln(15);

        $this->SetFont('Arial', 'B', 14);

        //SalesDocument Information
        $this->SetTextColor(61, 118, 166);
        $this->Cell(20);
        $this->Cell(70, 10, $data["SalesDocumentType"]["Name"] . " " . $data["SalesDocument"]["Reference"] . "/" . $data['EntrepriseInformation']['EntrepriseAcronym'] . "/" . $data['SalesDocument']['DocumentDate'], 0, 0, 'C'); //Reference
        $this->SetTextColor(0, 0, 0);
        $this->Ln(4);

        //Customer Information
        $this->Cell(140); // Décalage à droite

        $this->SetFont('Arial', 'BI', 14);
        $this->Cell(10, 10, 'Client : ' . $data["Customer"]["Name"], 0, 0, 'C'); // Nom Client

        $this->Ln(4);

        // $this->Cell(70); // Décalage à droite
        $this->SetFont('Arial', 'U', 10);
        $this->Cell(20, 10, format_text("Date édition :"), 0, 0, 'C');

        $this->SetFont('Arial', '', 10);
        $this->Cell(20, 10, " " . $data["SalesDocument"]["DocumentDate"], 0, 0, 'R'); // Date edition
        $this->Ln(4);
        // $this->Ln(15);

        // Customer Information

        $this->Cell(125); // Décalage à droite
        $this->SetFont('Arial', '', 10);
        $this->Cell(40, 10, $data["Customer"]["Address"], 0, 0, 'C'); // Adresse Client
        $this->Ln(6);

        $this->Cell(130); // Décalage à droite
        $this->SetFont('Arial', 'U', 10);
        $this->Cell(25, 10, format_text("Téléphone :"));

        $this->SetFont('Arial', '', 10);
        $this->Cell(17, 10, $data["Customer"]["Phone"], 0, 0, 'R'); // Contact Client
        $this->Ln(20);

        $this->ContentTable($data['DocumentItems'], $data);

        $this->Signature();
    }
    function ContentTable($documentItems, $result)
    {
        $header = array(format_text('Désignation'), format_text('Unité'), format_text('Quantite'), 'Prix unitaire', format_text('Remise'), 'Montant');
        // $data = array();
        // for ($i = 0; $i < 3; $i++) {
        //     for ($j = 0; $j < 6; $j++) {
        //         $data[$i][$j] = $i + $j;
        //     }
        // }
        // Couleurs, épaisseur du trait et police grasse
        $this->SetFillColor(182, 186, 183);
        $this->SetTextColor(0);
        $this->SetDrawColor(61, 118, 166);
        $this->SetLineWidth(.1);
        $this->SetFont('Arial', 'B');
        // En-tête
        $w = array(50, 20, 20, 30, 30, 40);
        for ($i = 0; $i < count($header); $i++)
            $this->Cell($w[$i], 7, $header[$i], 1, 0, 'C', true);
        $this->Ln();
        // Restauration des couleurs et de la police
        $this->SetFillColor(224, 235, 255);
        $this->SetTextColor(0);
        $this->SetFont('');
        // Données
        $fill = false;
        foreach ($documentItems as $row) {
            $this->Cell($w[0], 6, $row['Designation'], 'LRB', 0, 'L', $fill); // Designation
            $this->Cell($w[1], 6, format_text($row['Unit']), 'LRB', 0, 'C', $fill); // Unite
            $this->Cell($w[2], 6, format_number($row['Quantity']), 'LRB', 0, 'C', $fill); // Quantite
            $this->Cell($w[3], 6, format_number($row['UnitPrice']), 'LRB', 0, 'C', $fill); // Prix unitaire
            $this->Cell($w[4], 6, format_number($row['Reduction']), 'LRB', 0, 'C', $fill); // Reduction
            $this->Cell($w[5], 6, format_number(($row['UnitPrice'] * $row['Quantity']) - $row['UnitPrice'] * $row['Quantity'] * ($row['Reduction'] / 100)), 'LRB', 0, 'C', $fill); //Montant
            $this->Ln();
        }

        // Informaiton Montant

        $this->Cell($w[0] + $w[1] + $w[2] + $w[3]);
        $this->Cell($w[4], 6, 'Total HT', 'LRB', 0, 'C', $fill); // Total HT
        $this->Cell($w[5], 6, format_number($result['montantHT']), 'LRB', 0, 'C', $fill);

        $this->Ln();

        $this->Cell($w[0] + $w[1] + $w[2] + $w[3], 10, format_text("Arrêtée la présente facture à la somme de :"));
        $this->Cell($w[4], 6, 'Remise glob. ' . format_number($result['remiseGlobaleValue']) . '%', 'LRB', 0, 'C', $fill); // Remise globale
        $this->Cell($w[5], 6, $result['remiseGlobable'], 'LRB', 0, 'C', $fill);

        $this->Ln();

        $this->Cell($w[0] + $w[1] + $w[2] + $w[3], 10, intToStr($result['montantTTC']) . "Ariary"); // Montant TTC en lettre
        $this->Cell($w[4], 6, format_text('HT remisé'), 'LRB', 0, 'C', $fill); // HT remisé
        $this->Cell($w[5], 6, format_number($result['montantHTRemise']), 'LRB', 0, 'C', $fill);

        $this->Ln();

        $this->Cell($w[0] + $w[1] + $w[2] + $w[3]);
        $this->Cell($w[4], 6, 'TVA ' . format_number($result['tvaValue']) . '%', 'LRB', 0, 'C', $fill); // TVA
        $this->Cell($w[5], 6, format_number($result['tva']), 'LRB', 0, 'C', $fill);

        $this->Ln();

        $this->Cell($w[0] + $w[1] + $w[2] + $w[3]);
        $this->Cell($w[4], 6, 'Montant TTC', 'LRB', 0, 'C', $fill); // Montant TTC
        $this->Cell($w[5], 6, format_number($result['montantTTC']), 'LRB', 0, 'C', $fill);
    }

    function Signature()
    {
        $this->Ln(30);
        $this->Cell(60);
        $this->SetFont('Arial', 'UI', 13);
        $this->Cell(60, 10, 'Client');
        $this->Cell(60, 10, format_text('La Société'));

        $this->SetFont('Arial', '', 11);
    }
}
