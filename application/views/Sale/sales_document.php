<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>Sales document</title>
    <link rel="stylesheet" type="text/css" href="<?php echo site_url('assets/bootstrap/css/bootstrap.min.css'); ?>">

    <link rel="stylesheet" type="text/css" href="<?php echo site_url('assets/css/font.css'); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo site_url('assets/css/stock.css'); ?>">

    <script src="<?php echo site_url('assets/bootstrap/js/jquery.min.js'); ?>"></script>
    <script src="<?php echo site_url('assets/bootstrap/js/bootstrap.min.js'); ?>"></script>
</head>

<body>
    <div class="main-content">
        <h1 class="display-4 title">Documents de vente <a href="<?= site_url('SalesDocument/Form') ?>" class="btn btn-success">Ajouter</a> </h1>
        <div class="form-wrap">
            <form action="<?php echo site_url('SalesDocument/Index'); ?>" method="get">
                <div class="form-group">
                    <label for="idTypeDocument">Type de document</label>
                    <select class="form-control" id="idTypeDocument" name="idTypeDocument">
                        <option value="-1">Tous les documents</option>
                        <?php for ($i = 0; $i < count($SalesDocumentType); $i++) { ?>
                            <option value="<?php echo $SalesDocumentType[$i]["idSalesDocumentType"]; ?>"><?php echo $SalesDocumentType[$i]["Name"]; ?></option>
                        <?php } ?>

                    </select>
                </div>
                <div class="form-group">
                    <label for="idCustomer">Client</label>
                    <select class="form-control" id="idCustomer" name="idCustomer">
                        <option value="-1">Tous les clients</option>
                        <?php for ($i = 0; $i < count($Customers); $i++) { ?>
                            <option value="<?php echo $Customers[$i]["idCustomer"]; ?>"><?php echo $Customers[$i]["Name"]; ?></option>
                        <?php } ?>
                    </select>
                </div>
                <div class="form-group">
                    <label for="idSale">Vente</label>
                    <select class="form-control" id="idSale" name="idSale">
                        <option value="-1">Toutes les ventes</option>
                        <?php for ($i = 0; $i < count($Sales); $i++) { ?>
                            <option value="<?php echo $Sales[$i]["idSale"]; ?>"><?php echo $Sales[$i]["Reference"]; ?></option>
                        <?php } ?>

                    </select>
                </div>
                <div class="form-group">
                    <label for="DocumentDate">Date du document</label>
                    <input type="date" class="form-control" name="DocumentDate" />
                </div>
                <button type="submit" class="btn btn-warning">Filtrer</button>
            </form>
        </div>
        <h2>Mouvements</h2>
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th scope="col">Type du document</th>
                    <th scope="col">Vente</th>

                    <th scope="col">Reference</th>
                    <th scope="col">Client</th>
                    <th scope="col">Date du document</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <?php for ($i = 0; $i < count($SalesDocument); $i++) { ?>
                    <tr>
                        <th scope="row"><?php echo $SalesDocument[$i]["typeDocument"] ?></th>
                        <td><?php echo $SalesDocument[$i]["SaleReference"] ?></td>
                        <td><?php echo $SalesDocument[$i]["Reference"] ?></td>
                        <td><?php echo $SalesDocument[$i]["CustomerName"] ?></td>
                        <td><?php echo $SalesDocument[$i]["DocumentDate"] ?></td>
                        <td><a href="<?php echo site_url('SalesDocument/index/' . $SalesDocument[$i]["idSalesDocument"]); ?>">Voir détails</a></td>
                    </tr>
                <?php } ?>
                <!-- <tr>
                        <th scope="row">Bon de Livraison</th>
                        <td>BL000001</td>
                        <td>Sharon</td>
                        <td>16 Septembre 2020</td>
                        <td><a href="#">Voir détails</a></td>

                    </tr>
                    <tr>
                        <th scope="row">Pro format</th>
                        <td>PRF000001</td>
                        <td>Kevin</td>
                        <td>12 Novembre 2020</td>
                        <td><a href="#">Voir détails</a></td>

                    </tr> -->
            </tbody>
        </table>

    </div>
</body>

</html>