<?php
    defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>New Sale</title>
        <link rel="stylesheet" type="text/css" href="<?php echo site_url('assets/bootstrap/css/bootstrap.min.css'); ?>">
        <link rel="stylesheet" type="text/css" href="<?php echo site_url('assets/css/Form.css'); ?>">
        <link rel="stylesheet" type="text/css" href="<?php echo site_url('assets/fontawesome-4.15/css/font-awesome.min.css'); ?>">

        <script src="<?php echo site_url('assets/bootstrap/js/jquery.min.js'); ?>"></script>
        <script src="<?php echo site_url('assets/bootstrap/js/bootstrap.min.js'); ?>"></script>
    </head>
    <body>
        <div class="container document-wrap">
            <h1 class="display-4 document-title">Nouvelle vente</h1>
            <form action="<?php echo site_url('Sale/Insert'); ?>" method="post">
                <div class="document-form-information">    
                    <input type="hidden" name="NbItems" id="NbItems" value="0"/>
                    
                    <div class="form-group">
                        <label>Date de la vente</label>
                        <input name="SaleDate" type="date" class="form-control" required/>
                    </div>
                </div>
            <div class="document-flex">
                <button type="button" onclick="AddNewItem()" class="btn btn-dark document-button-add-item">Ajouter nouvel item <i class="fa fa-plus"></i></button>
                <button type="submit" class="btn btn-warning">Inserer</button>
            </div>   
            <br/>
            <div class="table-responsive document-table">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th scope="col">Article</th>
                            <th scope="col">Quantité</th>
                            <th scope="col"></th>

                        </tr>
                    </thead>
                    <tbody id="ItemsWrap">
                    </tbody>
                </table>
                <?php if(isset($error)){ ?> 
                <div class="alert alert-danger" role="alert">
                    <?php echo $error; ?>
                </div>
                <?php } ?>
            </div>
            </form>
        </div>
        <script type="text/javascript">
            let Items = <?php echo json_encode($Items); ?>;
            let ItemWrap = document.getElementById('ItemsWrap');
            let NbItems = 0;
            function DeleteItem(idItem){
                document.getElementById("Item_"+idItem).remove();

            }
            function AddNewItem(){
                let tr = document.createElement('tr');
                tr.setAttribute('id','Item_'+NbItems);
                tr.setAttribute('data-index',NbItems);


                // IdItem field : 
                let tdIdItem = document.createElement('td');
                let selectItem = document.createElement('select');
                selectItem.setAttribute('name','idItem_'+NbItems);
                selectItem.setAttribute('id','idItem_'+NbItems);
                selectItem.setAttribute('class','form-control');
                for(let i=0;i<Items.length;i++){
                    let optionItem = document.createElement('option');
                    optionItem.setAttribute('value',Items[i]["idItem"]);
                    let value = document.createTextNode(Items[i]["Designation"]);
                    optionItem.appendChild(value);
                    selectItem.appendChild(optionItem);
                }
                tdIdItem.appendChild(selectItem);

                // Quantity
                let tdQuantity = document.createElement('td');
                let inputQuantity = document.createElement('input');
                inputQuantity.setAttribute('class','form-control');
                inputQuantity.setAttribute('type','number');
                inputQuantity.setAttribute('name','Quantity_'+NbItems);
                inputQuantity.setAttribute('id','Quantity_'+NbItems);
                inputQuantity.setAttribute('required','true');
                tdQuantity.appendChild(inputQuantity);

                // DeleteButton
                let tdDeleteButton = document.createElement('td');
                let  deleteButton = document.createElement('button');
                deleteButton.setAttribute("class","btn btn-danger");
                deleteButton.setAttribute("onclick",`DeleteItem(${NbItems})`);
                deleteButton.appendChild(document.createTextNode("Supprimer"));
                tdDeleteButton.appendChild(deleteButton);

                tr.appendChild(tdIdItem);
                tr.appendChild(tdQuantity);
                tr.appendChild(tdDeleteButton);

                ItemWrap.appendChild(tr);

                NbItems++;
                document.getElementById("NbItems").value = NbItems;
            }
            AddNewItem();
        </script>
    </body>
</html>