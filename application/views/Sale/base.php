<?php
require_once 'pdf/BasePDF.php';
$montantHT = 0;
foreach ($result['DocumentItems'] as $r) {
    $montantHT += ($r['UnitPrice'] * $r['Quantity']) - ($r['UnitPrice'] * $r['Quantity'] * ($r['Reduction'] / 100));
}
$remiseGlobale = $result['SalesDocument']['GlobalDiscount'];
$montantHTRemise = $montantHT - ($montantHT * ($remiseGlobale / 100));
$tva = $result['TVA'];

$montantTTC = $montantHTRemise + ($montantHTRemise * ($tva / 100));

$result['montantHT'] = $montantHT;
$result['remiseGlobable'] = ($montantHT * ($remiseGlobale / 100));
$result['remiseGlobaleValue']=$remiseGlobale;
$result['tvaValue']=$tva;
$result['montantHTRemise'] = $montantHTRemise;
$result['tva'] = ($montantHTRemise * ($tva / 100));
$result['montantTTC'] = $montantTTC;
$pdf = new BasePDF();
$pdf->AddPage();
$pdf->Content($result);
$pdf->Output();
