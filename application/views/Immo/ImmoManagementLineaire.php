<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="<?php echo site_url('assets/bootstrap/css/bootstrap.min.css'); ?>">

    <link rel="stylesheet" type="text/css" href="<?php echo site_url('assets/css/font.css'); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo site_url('assets/css/stock.css'); ?>">

    <script src="<?php echo site_url('assets/bootstrap/js/jquery.min.js'); ?>"></script>
    <script src="<?php echo site_url('assets/bootstrap/js/bootstrap.min.js'); ?>"></script>


    <title>Immo Management</title>
</head>

<body>
    <div class="main-content">
        <h1 class="display-4 title">Gestion Immobilisation : </h1>
        <div>
            <a href="<?php echo site_url('Home'); ?>" class="home-link">Retour</a>
        </div>
        </br>
        <br />
        <div class="form-wrap-invert">
            <h2 class="title" style="color:#777777;"> Ammortissement Lineaire : </h2>
            <form action="<?php echo site_url('ImmoManagement/Lineaire'); ?>" method="get" style="align-items:flex-end;">
                <input type="hidden" name="init" value="true" />
                <div class="form-group">
                    <label>Valeur Initiale</label>
                    <input class="form-control" type="text" name="ValeurInitiale" value="<?php if (isset($input['ValeurInitiale'])) echo $input['ValeurInitiale']; ?>" required />
                </div>
                <div class="form-group">
                    <label>Durée utilisation</label>
                    <input class="form-control" type="text" name="DureeUtilisation" value="<?php if (isset($input['DureeUtilisation'])) echo $input['DureeUtilisation']; ?>" required />
                </div>
                <div class="form-group">
                    <label>Périodicité</label>
                    <select class="form-control" name="Periodicity">
                        <option value="<?php echo $Periodicity->ANNUEL; ?>" <?php if (isset($input['Periodicity']) && $input['Periodicity'] == $Periodicity->ANNUEL) echo 'selected'; ?>>Annuel</option>
                        <option value="<?php echo $Periodicity->MENSUEL; ?>" <?php if (isset($input['Periodicity']) && $input['Periodicity'] == $Periodicity->MENSUEL) echo 'selected'; ?>>Mensuel</option>
                        <option value="<?php echo $Periodicity->JOURNALIER; ?>" <?php if (isset($input['Periodicity']) && $input['Periodicity'] == $Periodicity->JOURNALIER) echo 'selected'; ?>>Journalier</option>

                    </select>
                </div>
                <div class="form-group">
                    <label>Date de première utilisation</label>
                    <input class="form-control" type="date" name="DatePremiereUtilisation" value="<?php if (isset($input['DatePremiereUtilisation'])) echo $input['DatePremiereUtilisation']; ?>" required />
                </div>
                <button class="btn btn-warning">Voir</button>
            </form>
        </div>
        <h2 class="display-4">Ammortissement du bien : </h2>
        <?php if (isset($Ammortissements)) { ?>
            <div class="table-responsive">
                <table class="table table-bordered ammortissement-table">
                    <thead>
                        <tr>
                            <th scope="col">Date</th>
                            <th scope="col">Valeur</th>
                            <th scope="col">Taux linéaire</th>
                            <th scope="col">Ammortissement cumulé début</th>
                            <th scope="col">Dotation</th>
                            <th scope="col">Ammortissement cumulé fin</th>
                            <th scope="col">Valeur nette</th>

                        </tr>
                    </thead>
                    <tbody>
                        <?php for ($i = 0; $i < count($Ammortissements); $i++) { ?>
                            <tr>
                                <td><?php echo $Ammortissements[$i]["DateFormat"] ?></td>
                                <td><?php echo $Ammortissements[$i]["Valeur"] ?></td>
                                <td><?php echo $Ammortissements[$i]["TauxLineaire"] ?></td>
                                <td><?php echo $Ammortissements[$i]["AmmortissementCumuleDebut"] ?></td>
                                <td><?php echo $Ammortissements[$i]["Dotation"] ?></td>
                                <td><?php echo $Ammortissements[$i]["AmmortissementCumuleFin"] ?></td>
                                <td><?php echo $Ammortissements[$i]["ValeurNette"] ?></td>

                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
        <?php } else { ?>
            <div class="alert alert-dark" role="alert">
                Veuillez remplir les champs ci dessus.
            </div>
        <?php } ?>
        <?php if (isset($err_message)) { ?>
            <div class="alert alert-danger" role="alert">
                <?php echo $err_message; ?>
            </div>
        <?php } ?>
    </div>
</body>

</html>