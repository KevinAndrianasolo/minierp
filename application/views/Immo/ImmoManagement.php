<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="<?php echo site_url('assets/bootstrap/css/bootstrap.min.css'); ?>">

    <link rel="stylesheet" type="text/css" href="<?php echo site_url('assets/css/font.css'); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo site_url('assets/css/stock.css'); ?>">

    <script src="<?php echo site_url('assets/bootstrap/js/jquery.min.js'); ?>"></script>
    <script src="<?php echo site_url('assets/bootstrap/js/bootstrap.min.js'); ?>"></script>


    <title>Immo Management</title>
</head>

<body>
    <div class="main-content">
        <h1 class="display-4 title">Gestion Immobilisation</h1>
        </br>
        <div class="card-wrap">
            <a href="ImmoManagement/Degressif">
                <div class="card">
                    <h1 class="card-title">Ammortissement degressif</h1>
                </div>
            </a>
            <a href="ImmoManagement/Lineaire">
                <div class="card">
                    <h1 class="card-title">Ammortissement lineaire</h1>
                </div>
            </a>
        </div>

    </div>
</body>

</html>