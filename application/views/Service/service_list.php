<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="<?php echo site_url('assets/bootstrap/css/bootstrap.min.css'); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo site_url('assets/css/font.css'); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo site_url('assets/css/crud_style.css'); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo site_url('assets/fontawesome/css/font-awesome.min.css'); ?>">
    <title>Service</title>
</head>

<body>
    <?php require_once APPPATH . 'views/header.php';
    ?>
    <div class="main-content">
        <h1 class="display-4 title">Liste des services <a class="btn btn-primary" href="<?php echo site_url("Service/Add") ?>">Ajouter</a>
        </h1>
        <table class="table">
            <thead class="thead-dark">
                <tr>
                    <th class="column-id">ID</th>
                    <th>Nom</th>
                    <th colspan=2>Actions</th>

                </tr>
            </thead>
            <tbody>
                <?php foreach ($Service as $i) { ?>
                    <tr>
                        <td><?= $i['idService'] ?></td>
                        <td><?= $i['ServiceName'] ?></td>
                        <td class="icon-wrap"><a href="<?php echo base_url() . "Service/remove/" . $i["idService"] ?>"><i class="fa fa-remove icon icon-remove"></i></a></td>
                        <td class="icon-wrap"><a href="<?php echo base_url() . "Service/Service_Form/" . $i["idService"] ?>"><i class="fa fa-edit icon icon-edit"></i></a></td>
                        </td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
        <script src="<?php echo site_url('assets/bootstrap/js/jquery.min.js'); ?>"></script>
        <script src="<?php echo site_url('assets/bootstrap/js/bootstrap.min.js'); ?>"></script>
</body>

</html>