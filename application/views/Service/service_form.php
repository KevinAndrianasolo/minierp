<!DOCTYPE html>
<html lang="en">

<head>
    <title>formulaire </title>
    <link rel="stylesheet" type="text/css" href="<?php echo site_url('assets/bootstrap/css/bootstrap.min.css'); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo site_url('assets/css/crud_style.css'); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo site_url('assets/css/font.css'); ?>">

</head>


<body>
    <?php require_once APPPATH . 'views/header.php';
    ?>
    <div class="main-content">
        <h1 class="display-4 title">Formulaire service <a href="<?= site_url('Service') ?>" class="btn btn-primary">Liste</a></h1>
        <form action="<?php if (isset($Service)) {
                            echo site_url("service/Update");
                        } else {
                            echo site_url("service/add_form");
                        } ?>
" method="POST" class="form-wrap">
            <?php if (isset($Service)) { ?>
                <input type="hidden" name="idService" value="<?= $Service['idService'] ?>">
            <?php } ?>
            <div class="form-group">
                <label for="Name">Name</label>
                <input class="form-control" type="text" name="Name" id="Name" <?php if (isset($Service)) {
                                                                                    echo "value =\"" . $Service['ServiceName'] . "\"";
                                                                                } ?>>
                <button class="btn btn-dark" type="submit">Valider</button>
        </form>
    </div>
    <script src="<?php echo site_url('assets/bootstrap/js/jquery.min.js'); ?>"></script>
    <script src="<?php echo site_url('assets/bootstrap/js/bootstrap.min.js'); ?>"></script>

</body>

</html>