<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="<?php echo site_url('assets/bootstrap/css/bootstrap.min.css'); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo site_url('assets/css/font.css'); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo site_url('assets/css/crud_style.css'); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo site_url('assets/fontawesome/css/font-awesome.min.css'); ?>">
    <title>Liste des fournisseurs</title>
</head>

<body>
    <?php require_once APPPATH . 'views/header.php';
    ?>
    <div class="main-content">
        <h1 class="display-4 title">Liste des fournisseurs <a href="<?= site_url('Provider/Add') ?> " class="btn btn-success"> Ajouter</a></h1>
        <table class="table">
            <thead class="thead-dark">
                <tr>
                    <th>ID</th>
                    <th>Reference</th>
                    <th>Nom</th>
                    <th>Adresse</th>
                    <th>Telephone</th>
                    <th>Nom du leader</th>
                    <th>Email</th>
                    <th>Description</th>
                    <th colspan=2>Actions</th>
                </tr>
            </thead>
            <tbody>

                <?php foreach ($providers as $provider) :  ?>
                    <tr>
                        <td><?php echo $provider['idProvider']; ?></td>
                        <td><?php echo $provider['Reference']; ?></td>
                        <td><?php echo $provider['Name']; ?></td>
                        <td><?php echo $provider['Address']; ?></td>
                        <td><?php echo $provider['Phone']; ?></td>
                        <td><?php echo $provider['LeaderName']; ?></td>
                        <td><?php echo $provider['Email']; ?></td>
                        <td><?php echo $provider['Description']; ?></td>
                        <td class="icon-wrap"><a href="<?php echo base_url() . "Provider/Update/" . $provider["idProvider"] ?>"><i class="fa fa-edit icon icon-edit"></i></a></td>
                        <td class="icon-wrap"><a href="<?php echo site_url("Provider/Delete/" . $provider["idProvider"]) ?>"><i class="fa fa-remove icon icon-remove"></i></a></td>
                        </td>
                    </tr>
            </tbody>
        <?php endforeach; ?>
        </table>
    </div>
    <script src="assets/bootstrap/js/jquery.min.js"></script>
    <script src="assets/bootstrap/js/bootstrap.min.js"></script>
</body>