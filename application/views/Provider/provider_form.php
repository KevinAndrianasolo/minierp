<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>Formulaire Fournisseur</title>
    <link rel="stylesheet" type="text/css" href="<?php echo site_url('assets/bootstrap/css/bootstrap.min.css'); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo site_url('assets/css/font.css'); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo site_url('assets/css/crud_style.css'); ?>">
</head>

<body>
<?php require_once APPPATH . 'views/header.php';
?>
    <div class="main-content">
        <h1 class="display-4 title">Formulaire fournisseur  <a href="<?= site_url('Provider') ?> " class="btn btn-primary">  Liste</a></h1>
        <form action="<?php if (isset($provider)) {
                            echo site_url("Provider/Update_Action");
                        } else {
                            echo site_url("Provider/Add_Action");
                        } ?>
" method="POST" class="form-wrap">
            <?php if (isset($provider)) { ?>
                <input type="hidden" name="idProvider" value="<?= $provider['idProvider'] ?>">
            <?php } ?>
            <div class="form-group">
                <label for="Name">Nom</label>
                <input class="form-control" type="text" name="Name" id="Name" <?php if (isset($provider)) {
                                                                                                echo "value =\"" . $provider['Name'] . "\"";
                                                                                            } ?>>
            </div>
            <div class="form-group">
                <label for="Address">Adresse</label>
                <input class="form-control" type="text" name="Address" id="Address" <?php if (isset($provider)) {
                                                                                                echo "value =\"" . $provider['Address'] . "\"";
                                                                                            } ?>>
            </div>
            <div class="form-group">
                <label for="Phone">Telephone</label>
                <input class="form-control" type="text" name="Phone" id="Phone" <?php if (isset($provider)) {
                                                                                                echo "value =\"" . $provider['Phone'] . "\"";
                                                                                            } ?>>
            </div>
            <div class="form-group">
                <label for="LeaderName">Nom du leader</label>
                <input class="form-control" type="text" name="LeaderName" id="LeaderName" <?php if (isset($provider)) {
                                                                                                echo "value =\"" . $provider['LeaderName'] . "\"";
                                                                                            } ?>>
            </div>
            <div class="form-group">
                <label for="Email">Email</label>
                <input class="form-control" type="email" name="Email" id="Email" <?php if (isset($provider)) {
                                                                                                echo "value =\"" . $provider['Email'] . "\"";
                                                                                            } ?>>
            </div>
            <div class="form-group">
                <label for="Description">Description</label>
                <input class="form-control" type="text" name="Description" id="Description" <?php if (isset($provider)) {
                                                                                                echo "value =\"" . $provider['Description'] . "\"";
                                                                                            } ?>>
            </div>
            <button class="btn btn-dark" style="font-family: 'BigJohn';" type="submit">Valider</button>
        </form>
    </div>
</body>

</html>