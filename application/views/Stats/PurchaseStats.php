<!DOCTYPE html>
<html>

<head>
  <title>Insertion TVA</title>
  <link rel="stylesheet" type="text/css" href="<?php echo site_url('assets/bootstrap/css/bootstrap.min.css'); ?>">
  <link rel="stylesheet" type="text/css" href="<?php echo site_url('assets/css/crud_style.css'); ?>">
  <link rel="stylesheet" type="text/css" href="<?php echo site_url('assets/css/font.css'); ?>">
</head>

<body>
  <?php require_once APPPATH . 'views/header.php';
  ?>
  <div class="main-content">

    <h1 class="display-4 title">Statitisques d'achat</h1>
    <form method="get" action="<?php echo site_url("Stats/Purchase") ?>" class="form-wrap">
      <div class="form-group">
        <label>Mois</label>
        <select class="form-control" name="month">
            <?php for($i=1; $i<=count($months);$i++){ ?>
                <option value="<?php echo $i; ?>" <?php if($i == $month){ echo "selected"; } ?>><?php echo $months[$i]['Name']; ?></option>
            <?php } ?>
        </select>
      </div>
      <div class="form-group">
        <label>Année</label>
        <select class="form-control" name="year">
            <?php for($i=2021; $i<2025;$i++){ ?>
                <option value="<?php echo $i; ?>" <?php if($i == $year){ echo "selected"; } ?>><?php echo $i; ?></option>
            <?php } ?>
        </select>
      </div>
      <button class="btn btn-dark" type="submit">Voir stats</button>

    </form>
    <table class="table">
        <thead class="thead-dark">
                <tr>
                    <th>IdItem</th>
                    <th>Référence</th>
                    <th>Nom</th>
                    <th>Quantité</th>
                    <th>Pourcentage (en %)</th>
                </tr>
        </thead>
        <tbody>
            <?php for($i=0; $i<count($stats);$i++){
                ?>
                <tr>
                    <td><?php echo $stats[$i]['idItem']; ?></td>
                    <td><?php echo $stats[$i]['Reference']; ?></td>
                    <td><?php echo $stats[$i]['Designation']; ?></td>
                    <td><?php echo $stats[$i]['Quantity']; ?></td>
                    <td><?php echo $stats[$i]['Percent']; ?></td>
                </tr>
            <?php } ?>
        </tbody>
    </table>
</div>
      <script src="<?php echo site_url('assets/bootstrap/js/jquery.min.js'); ?>"></script>
      <script src="<?php echo site_url('assets/bootstrap/js/bootstrap.min.js'); ?>"></script>

</body>

</html>