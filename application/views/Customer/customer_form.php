<!DOCTYPE html>
<html lang="en">

<head>
    <title>Formulaire client</title>
    <link rel="stylesheet" type="text/css" href="<?php echo site_url('assets/bootstrap/css/bootstrap.min.css'); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo site_url('assets/css/crud_style.css'); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo site_url('assets/css/font.css'); ?>">

</head>

<body>
    <?php require_once APPPATH . 'views/header.php';
    ?>
    <div class="main-content">
        <h1 class="display-4 title">Formulaire client <a href="<?= site_url('Customer'); ?>" class="btn btn-primary">Liste</a></h1>
        <form action="<?php if (isset($customer)) {
                            echo site_url("Customer/Update_Action");
                        } else {
                            echo site_url("Customer/Add_Action");
                        } ?>
" method="POST" class="form-wrap">
            <?php if (isset($customer)) { ?>
                <input type="hidden" name="idCustomer" value="<?= $customer['idCustomer'] ?>">
            <?php } ?>
            <div class="form-group">
                <label for="Name">Name</label>
                <input class="form-control" type="text" name="Name" id="Name" <?php if (isset($customer)) {
                                                                                    echo "value =\"" . $customer['Name'] . "\"";
                                                                                } ?>>
            </div>
            <div class="form-group">
                <label for="Address">Address</label>
                <input class="form-control" type="text" name="Address" id="Address" <?php if (isset($customer)) {
                                                                                        echo "value =\"" . $customer['Address'] . "\"";
                                                                                    } ?>>
            </div>
            <div class="form-group">
                <label for="Phone">Phone</label>
                <input class="form-control" type="text" name="Phone" id="Phone" <?php if (isset($customer)) {
                                                                                    echo "value =\"" . $customer['Phone'] . "\"";
                                                                                } ?>>
            </div>
            <div class="form-group">
                <label for="Email">Email</label>
                <input class="form-control" type="text" name="Email" id="Email" <?php if (isset($customer)) {
                                                                                    echo "value =\"" . $customer['Email'] . "\"";
                                                                                } ?>>
            </div>
            <button class="btn btn-dark" type="submit">Valider</button>
        </form>
    </div>
    <script src="<?php echo site_url('assets/bootstrap/js/jquery.min.js'); ?>"></script>
    <script src="<?php echo site_url('assets/bootstrap/js/bootstrap.min.js'); ?>"></script>

</body>

</html>