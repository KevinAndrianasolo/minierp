<?php
defined('BASEPATH') or exit('No direct script access allowed');
require APPPATH . 'controllers/BaseController.php';
require_once APPPATH . 'views/viewHelper.php';

class Employee extends BaseController
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('EmployeeModel');
		$this->load->model('EmployeeStatus');
		$this->load->model('DepartementModel');
		$this->load->model('MariageStatusModel');
		$this->load->model('Month');
		$this->load->model('BonusType');
	}
	public function index()
	{
		$this->List();
	}

	public function Add_Action()
	{
		$i = $this->input->post();
		// $Name, $CIN, $NumCnaps, $IdMariageStatus, $IdDepartement, $Status, $Fonction, $HiringDate
		$Name = $i['Name'];
		$CIN = $i['CIN'];
		$NumCnaps = $i['NumCnaps'];
		$IdMariageStatus = $i['IdMariageStatus'];
		$IdDepartement = $i['IdDepartement'];
		$EmployeeStatus = $i['EmployeeStatus'];
		$Fonction = $i['Fonction'];
		$HiringDate = $i['HiringDate'];
		$this->EmployeeModel->InsertEmployee($Name, $CIN, $NumCnaps, $IdMariageStatus, $IdDepartement, $EmployeeStatus, $Fonction, $HiringDate);
		redirect('/Employee');
	}
	public function Update_Action()
	{
		$i = $this->input->post();
		$Name = $i['Name'];
		$CIN = $i['CIN'];
		$NumCnaps = $i['NumCnaps'];
		$IdMariageStatus = $i['IdMariageStatus'];
		$IdDepartement = $i['IdDepartement'];
		$EmployeeStatus = $i['EmployeeStatus'];
		$Fonction = $i['Fonction'];
		$HiringDate = $i['HiringDate'];
		$IdEmployee = $i['IdEmployee'];
		$this->EmployeeModel->UpdateEmployee($IdEmployee, $Name, $CIN, $NumCnaps, $IdMariageStatus, $IdDepartement, $EmployeeStatus, $Fonction, $HiringDate);
		redirect('/Employee');
	}

	public function Add()
	{
		$data = array();
		$data['employeeStatus'] = $this->EmployeeStatus->GetAll();
		$data['Departement'] = $this->DepartementModel->GetAll();
		$data['MariageStatus'] = $this->MariageStatusModel->GetAll();
		$this->load->view('Employee/employee_form', $data);
	}
	public function Update($id)
	{
		$data['employee'] = $this->EmployeeModel->GetById($id);
		$data['employeeStatus'] = $this->EmployeeStatus->GetAll();
		$data['Departement'] = $this->DepartementModel->GetAll();
		$data['MariageStatus'] = $this->MariageStatusModel->GetAll();
		$this->load->view('Employee/employee_form', $data);
	}
	public function List()
	{
		$data['Employee'] = $this->EmployeeModel->GetList();
		$this->load->view('Employee/employee_list', $data);
	}
	public function Delete($id)
	{
		$this->EmployeeModel->DeleteEmployee($id);
		redirect('/Employee');
	}
	public function PayStubForm()
	{
		$i = $this->input->get();
		if (isset($i['IdEmployee'])) {
			$IdEmployee = $i['IdEmployee'];
		}
		$data = array();
		$data['Employees'] = $this->EmployeeModel->GetList();
		$data['Month'] = $this->Month->GetMonths();
		if (isset($IdEmployee)) {
			$data['employee'] = $this->EmployeeModel->GetById($IdEmployee);
		}
		$this->load->view('Employee/paie_form', $data);
	}
	public function PayStub()
	{
		$data = array();

		$i = $this->input->get();
		if (isset($i['IdEmployee'], $i['Month'])) {
			$IdEmployee = $i['IdEmployee'];
			$Month = $i['Month'];
			$months = $this->Month->GetMonths();
			$data['Month'] = $months[$i['Month']]['Name'];
			$data['employee'] = $this->EmployeeModel->GetById($IdEmployee);
			$data['basic_salary'] = $this->EmployeeModel->GetBasicSalary($IdEmployee);
			$data['base_imposable'] = $this->EmployeeModel->GetBaseImposable($IdEmployee, $Month);
			$data['cnaps'] = $this->EmployeeModel->GetCNAPSContribution();
			$data['cnapsAmount'] = $this->EmployeeModel->GetCnapsAmount($IdEmployee);
			$data['ostie'] = $this->EmployeeModel->GetOSTIEContribution();
			$data['ostieAmount'] = $this->EmployeeModel->GetOstieAmount($IdEmployee);
			$data['IRSA'] = $this->EmployeeModel->GetIRSAByRange($IdEmployee, $Month);
			$data['irsaTotal'] = $this->EmployeeModel->GetIRSATotal($IdEmployee, $Month);
			$data['salaireNet'] = $this->EmployeeModel->GetNetSalary($IdEmployee, $Month);
			$extraHour = $this->EmployeeModel->GetExtraHour($IdEmployee, $Month);
			$extraHourValue = $this->EmployeeModel->GetExtraHourValue($IdEmployee);
			$data['extra_hour_value'] = format_number($extraHourValue['ValuePerHour']);
			if (isset($extraHour)) {
				$data['extra_hour'] = $extraHour['Hour'];
			} else {
				$data['extra_hour'] = "Aucun";
			}
			$data['bonus'] = $this->EmployeeModel->GetBonus($IdEmployee, $Month);

			$this->load->view('Employee/paie_info', $data);
		}
	}
	public function ExtraHour()
	{
		$data['Employees'] = $this->EmployeeModel->GetList();
		$data['Month'] = $this->Month->GetMonths();
		$this->load->view('Employee/extra_hour_form', $data);
	}
	public function ExtraHour_Add()
	{
		$i = $this->input->post();
		$IdEmployee = $i['IdEmployee'];
		$Month = $i['Month'];
		$extraHour = $i['ExtraHour'];
		$this->EmployeeModel->InsertExtraHour($IdEmployee, $extraHour, $Month);
		redirect('/Employee');
	}
	public function ExtraHourValue()
	{
		$data['Employees'] = $this->EmployeeModel->GetList();
		$data['Month'] = $this->Month->GetMonths();
		$this->load->view('Employee/extra_hour_value_form', $data);
	}
	public function ExtraHourValue_Add()
	{
		$i = $this->input->post();
		$IdEmployee = $i['IdEmployee'];
		$dateValue = $i['dateValue'];
		$valuePerHour = $i['valuePerHour'];
		$this->EmployeeModel->InsertExtraHourValue($IdEmployee, $valuePerHour, $dateValue);
		redirect('/Employee');
	}
	public function Bonus()
	{
		$i = $this->input->get();

		if (isset($i['IdEmployee']) && isset($i['Month'])) {
			$IdEmployee = $i['IdEmployee'];
			$Month = $i['Month'];
			$data['employee'] = $this->EmployeeModel->GetById($IdEmployee);
			$classMonth = $this->Month->GetMonths();
			$data['Month'] = $classMonth[$Month];
			$data['bonus'] = $this->EmployeeModel->GetBonus($IdEmployee, $Month);
			$this->load->view('Employee/bonus_list', $data);
		} else {
			$data['Employees'] = $this->EmployeeModel->GetList();
			$data['Month'] = $this->Month->GetMonths();
			$data['BonusType'] = $this->BonusType->GetBonusType();

			$this->load->view('Employee/bonus_form', $data);
		}
	}
	public function Bonus_Add()
	{
		$i = $this->input->post();
		$IdEmployee = $i['IdEmployee'];
		$Month = $i['Month'];
		$BonusType = $i['BonusType'];
		$CashValue = $i['CashValue'];
		$this->EmployeeModel->InsertBonus($IdEmployee, $BonusType, $CashValue, $Month);
		redirect('/Employee/Bonus?IdEmployee=' . $i['IdEmployee'] . '&Month=' . $i['Month']);
	}
}
