<?php
defined('BASEPATH') or exit('No direct script access allowed');
require APPPATH . 'controllers/BaseController.php';
require APPPATH . 'classes/Stock.php';
class StockManagement extends BaseController
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('StockManagementModel');
		$this->load->model('ItemModel');

		date_default_timezone_set('Indian/Antananarivo');
	}
	public function index($idItem = "")
	{
		if ($idItem == "") {
			$idItem = 1;
		}
		$params = $this->input->get();
		$now = new DateTime();
		// $idItem = isset($params['idItem']) ? $params['idItem'] : 1;
		$Item = $this->ItemModel->GetById($idItem);
		$idStockManagementMethod = isset($params['idStockManagementMethod']) ? $params['idStockManagementMethod'] : 1;
		$Date = isset($params['Date']) ? $params['Date'] : $now->format('Y-m-d');
		$Stock = new Stock($idItem, $idStockManagementMethod, $Date, $this->db);
		$StockManagementMethod = $this->StockManagementModel->GetStockManagementList();
		$data = array();
		if (isset($params['error'])) $data['error'] = $params['error'];

		$data['Stock'] = $Stock;
		$data['idItem'] = $idItem;
		$data['Item'] = $Item;


		$data['idStockManagementMethod'] = $idStockManagementMethod;
		$data['Date'] = $Date;

		$data['StockManagementMethod'] = $StockManagementMethod;
		require_once APPPATH . 'views/header.php';
		$this->load->view('StockManagement/StockManagement',$data);
	}
	public function InsertEntree()
	{
		try {
			$params = $this->input->post();
			$idItem = $params['idItem'];
			$idStockManagementMethod = $params['idStockManagementMethod'];
			$DateStock = $params['DateStock'];

			$Quantity = $params['Quantity'];
			$UnitPrice = $params['UnitPrice'];
			$Date = $params['Date'] . ' ' . $params['Heure'];
			$this->StockManagementModel->InsertEntree($idItem, $Quantity, $UnitPrice, $Date);
			header('location:' . site_url('StockManagement/index/' . $idItem . '?idStockManagementMethod=' . $idStockManagementMethod . '&Date=' . $DateStock));
		} catch (Exception $e) {
			$error = $e->getMessage();
			header('location:' . site_url('StockManagement/index/' . $idItem . '?idStockManagementMethod=' . $idStockManagementMethod . '&Date=' . $DateStock . '&error=' . $error));
		}
	}
	public function InsertSortie()
	{
		try {
			$params = $this->input->post();
			$idItem = $params['idItem'];
			$idStockManagementMethod = $params['idStockManagementMethod'];
			$DateStock = $params['DateStock'];

			$Quantity = $params['Quantity'];
			$Date = $params['Date'] . ' ' . $params['Heure'];

			$this->StockManagementModel->InsertSortie($idItem, $Quantity, $Date, $idStockManagementMethod);
			header('location:' . site_url('StockManagement/index/' . $idItem . '?idStockManagementMethod=' . $idStockManagementMethod . '&Date=' . $DateStock));
		} catch (Exception $e) {
			$error = $e->getMessage();
			header('location:' . site_url('StockManagement/index/' . $idItem . '?idStockManagementMethod=' . $idStockManagementMethod . '&Date=' . $DateStock . '&error=' . $error));
		}
	}
}
