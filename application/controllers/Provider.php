<?php
defined('BASEPATH') or exit('No direct script access allowed');
require APPPATH . 'controllers/BaseController.php';

class Provider extends BaseController
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('ProviderModel');
    }
    public function index()
    {
        $this->List();
    }
    public function List()
    {
        $data = array();
        $data['providers'] = $this->ProviderModel->GetList();
        $this->load->view('Provider/provider_list', $data);
    }
    public function Add()
    {
        $this->load->view('Provider/provider_form');
    }
    public function Add_Action()
    {
        $input = $this->input->post();
        $Name = $input["Name"];
        $Address = $input["Address"];
        $Phone = $input["Phone"];
        $LeaderName = $input["LeaderName"];
        $Description = $input["Description"];
        $Email = $input["Email"];
        $data['provider'] = $this->ProviderModel->Insert($Name, $Address, $Phone, $LeaderName, $Email, $Description);
        redirect('/Provider');
    }
    public function Delete($id){
        $this->ProviderModel->Delete($id);
        redirect('/Provider');
    }
    public function Update($id){
        $data['provider']=$this->ProviderModel->GetById($id);
        $this->load->view('Provider/provider_form',$data);
    }
    public function Update_Action(){
        $input = $this->input->Post();
        $IdProvider = $input['idProvider'];
        $Name = $input["Name"];
        $Address = $input["Address"];
        $Phone = $input["Phone"];
        $LeaderName = $input["LeaderName"];
        $Description = $input["Description"];
        $Email = $input["Email"];
        $this->ProviderModel->Update($IdProvider, $Name, $Address, $Phone, $LeaderName, $Email, $Description);
        redirect('/Provider');
    }
}
