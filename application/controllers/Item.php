<?php
defined('BASEPATH') or exit('No direct script access allowed');
require APPPATH . 'controllers/BaseController.php';
class Item extends BaseController
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('ItemModel');
		$this->load->model('CustomerModel');
	}
	public function index()
	{
		$this->List();
	}

	public function Add_Action()
	{
		$input = $this->input->post();
		$Designation = $input["Designation"];
		$data['item'] = $this->ItemModel->Insert($Designation);
		redirect('/Item');
	}
	public function Update_Action()
	{
		$input = $this->input->post();
		$idCustomer = $input["idItem"];
		$designation = $input["Designation"];
		$data['item'] = $this->ItemModel->Update($idCustomer, $designation);
		redirect('Item');
	}

	public function Add()
	{
		$this->load->view('Item/item_form');
	}
	public function Update($id)
	{
		$data['item'] = $this->ItemModel->GetById($id);
		$this->load->view('Item/item_form', $data);
	}
	public function List()
	{
		$data['item'] = $this->ItemModel->GetList();
		$this->load->view('Item/item_list', $data);
	}
	public function Delete($id)
	{
		$this->ItemModel->Delete($id);
		redirect('/Item');
	}
}
