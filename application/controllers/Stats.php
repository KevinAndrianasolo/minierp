<?php
defined('BASEPATH') or exit('No direct script access allowed');
require APPPATH . 'controllers/BaseController.php';
require APPPATH . 'classes/PurchaseStats.php';


class Stats extends BaseController
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model("Month");
	}
	public function index()
	{
		require_once APPPATH . 'views/header.php';
	}
	public function Purchase()
	{
		$params = $this->input->get();

		$view = 'Stats/PurchaseStats';
		$month = isset($params["month"]) ? $params["month"] : 3;
		$year = isset($params["year"]) ? $params["year"] : 2021;
		$PurchaseStats = new PurchaseStats($this->db);
		$stats = $PurchaseStats->getPurchaseStatsOf($month, $year);
		$data = array();
		$data['months'] = $this->Month->GetMonths();
		$data['stats'] = $stats;
		$data['month'] = $month;
		$data['year'] = $year;

		require_once APPPATH . 'views/header.php';
		$this->load->view($view, $data);
	}
}
