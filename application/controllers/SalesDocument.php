<?php
defined('BASEPATH') or exit('No direct script access allowed');
require APPPATH . 'controllers/BaseController.php';

class SalesDocument extends BaseController
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('SalesDocumentModel');
		$this->load->model('SalesDocumentTypeModel');
		$this->load->model('SalesDocumentItemModel');
		$this->load->model('PurchaseModel');
		$this->load->model('CustomerModel');
		$this->load->model('ItemModel');
		$this->load->model('CurrencyModel');
		$this->load->model('UnitModel');
		$this->load->model('SaleModel');
		$this->load->model('EntrepriseInformationModel');
		$this->load->model("TVAModel");
	}
	public function index($id = -1)
	{
		$view = 'Sale/sales_document';
		$data = array();
		$result = array();
		$IdSalesDocument = $id;
		if ($id != -1) {
			$SalesDocument = $this->SalesDocumentModel->GetById($IdSalesDocument);
			$view = 'Sale/base';
			$result["SalesDocument"] = $SalesDocument;
			$result["Customer"] = $this->CustomerModel->GetById($SalesDocument["idCustomer"]);
			$result["EntrepriseInformation"] = $this->EntrepriseInformationModel->GetList();
			$result["DocumentItems"] = $this->SalesDocumentItemModel->GetItemsById($id);
			$result['SalesDocumentType'] = $this->SalesDocumentTypeModel->GetById($result['SalesDocument']['idSalesDocumentType']);
			$result['TVA'] = $this->TVAModel->CurrentTVA();
		}

		// Filtre
		$idSale = isset($_GET['idSale']) ? $_GET['idSale'] : -1;
		$idCustomer = isset($_GET['idCustomer']) ? $_GET['idCustomer'] : -1;
		$idTypeDocument = isset($_GET['idTypeDocument']) ? $_GET['idTypeDocument'] : -1;
		$documentDate = isset($_GET['DocumentDate']) ? $_GET['DocumentDate'] : "";
		$data['idSalesDocument'] = $IdSalesDocument;
		$data['SalesDocumentType'] = $this->SalesDocumentModel->GetSalesDocumentTypeList();
		$data['Customers'] = $this->CustomerModel->GetList();
		$data['Sales'] = $this->SaleModel->GetList();
		$data['SalesDocument'] = $this->SalesDocumentModel->get_salesDocumentById($idSale, $idCustomer, $idTypeDocument, $documentDate);
		if (count($result) != 0) {
			$data['result'] = $result;
		}
		if ($id == -1) {
			require_once APPPATH . 'views/header.php';
		}
		$this->load->view($view, $data);
	}
	public function Form()
	{
		$SalesDocumentType =  $this->SalesDocumentModel->GetSalesDocumentTypeList();
		$Customers = $this->CustomerModel->GetList();
		$Items = $this->ItemModel->GetList();
		$CurrencyList = $this->CurrencyModel->GetList();
		$UnitList = $this->UnitModel->GetList();
		$Sales = $this->SaleModel->GetList();

		$data = array();
		$input = $this->input->get();
		if (isset($input['error'])) $data['error'] = $input['error'];
		$data['SalesDocumentType'] = $SalesDocumentType;
		$data['Customers'] = $Customers;
		$data['CurrencyList'] = $CurrencyList;
		$data['Items'] = $Items;
		$data['UnitList'] = $UnitList;
		$data['Sales'] = $Sales;
		$data['TVA'] = $this->TVAModel->CurrentTVA();
		require_once APPPATH . 'views/header.php';
		$this->load->view('Sale/sales_document_new', $data);
	}
	public function Insert()
	{
		try {
			$datas = $this->input->post();
			var_dump($datas);
			$SalesDocument = array();
			$SalesDocument["idSalesDocumentType"] = $datas["idSalesDocumentType"];
			$SalesDocument["idCustomer"] = $datas["idCustomer"];
			$SalesDocument["DocumentDate"] = $datas["DocumentDate"];
			$SalesDocument["GlobalDiscount"] = $datas["GlobalDiscount"];
			$SalesDocument["idCurrency"] = $datas["idCurrency"];
			$SalesDocument["idSale"] = $datas["idSale"];

			$NbItems = $datas["NbItems"];
			$SalesDocument['Items'] = array();
			$iItem = 0;
			for ($i = 0; $i < $NbItems; $i++) {
				if (!isset($datas["idItem_" . $i])) continue;
				$SalesDocument['Items'][$iItem] = array();
				$SalesDocument['Items'][$iItem]["idItem"] = $datas["idItem_" . $i];
				$SalesDocument['Items'][$iItem]["Quantity"] = $datas["Quantity_" . $i];
				$SalesDocument['Items'][$iItem]["idUnit"] = $datas["idUnit_" . $i];
				$SalesDocument['Items'][$iItem]["UnitPrice"] = $datas["UnitPrice_" . $i];
				$SalesDocument['Items'][$iItem]["Reduction"] = $datas["Reduction_" . $i];
				$iItem++;
			}
			$this->SalesDocumentModel->Insert($SalesDocument);
			header("location:" . site_url('/SalesDocument'));
		} catch (Exception $e) {
			$error = $e->getMessage();
			header("location:" . site_url('/SalesDocument/Form?error=' . $error));
		}
	}
}
