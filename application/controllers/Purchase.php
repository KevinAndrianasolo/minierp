<?php
defined('BASEPATH') or exit('No direct script access allowed');
require APPPATH . 'controllers/BaseController.php';

class Purchase extends BaseController
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model("PurchaseModel");
		$this->load->model('ItemModel');
		$this->load->model('ServiceModel');
		$this->load->model('PurchaseStatusModel');
	}
	public function index()
	{
		$idService = isset($_GET['idService']) ? $_GET['idService'] : -1;
		$idStatus = isset($_GET['idStatus']) ? $_GET['idStatus'] : -1;
		$purchaseDate = isset($_GET['PurchaseDate']) ? $_GET['PurchaseDate'] : "";

		$Services = $this->ServiceModel->GetList();
		$PurchaseStatus = $this->PurchaseStatusModel->GetList();

		$data = array();
		$data["Services"] = $Services;
		$data["PurchaseStatus"] = $PurchaseStatus;
		$data["Purchases"] = $this->PurchaseModel->get_purchaseById($idService, $idStatus, $purchaseDate);
		require_once APPPATH . 'views/header.php';

		$this->load->view('Purchase/Purchase', $data);
	}
	public function Form()
	{

		$Items = $this->ItemModel->GetList();
		$Services = $this->ServiceModel->GetList();
		$data = array();
		$input = $this->input->get();
		if (isset($input['error'])) $data['error'] = $input['error'];

		$data["Items"] = $Items;
		$data["Services"] = $Services;
		require_once APPPATH . 'views/header.php';

		$this->load->view('Purchase/Purchase_new', $data);
	}

	public function Insert()
	{
		try {
			$datas = $this->input->post();
			var_dump($datas);
			$Purchase = array();
			$Purchase["idService"] = $datas["idService"];
			$Purchase["PurchaseDate"] = $datas["PurchaseDate"];

			$NbItems = $datas["NbItems"];
			$Purchase['Items'] = array();
			$iItem = 0;
			for ($i = 0; $i < $NbItems; $i++) {
				if (!isset($datas["idItem_" . $i])) continue;
				$Purchase['Items'][$iItem] = array();
				$Purchase['Items'][$iItem]["idItem"] = $datas["idItem_" . $i];
				$Purchase['Items'][$iItem]["Quantity"] = $datas["Quantity_" . $i];
				$iItem++;
			}
			$this->PurchaseModel->Insert($Purchase);
			//var_dump($Purchase);
			header("location:" . site_url('/Purchase'));
		} catch (Exception $e) {
			$error = $e->getMessage();
			header("location:" . site_url('/Purchase/Form?error=' . $error));
		}
	}
}
