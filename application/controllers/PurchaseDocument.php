<?php
defined('BASEPATH') or exit('No direct script access allowed');
require APPPATH . 'controllers/BaseController.php';

class PurchaseDocument extends BaseController
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('PurchaseDocumentModel');
		$this->load->model('PurchaseDocumentItemModel');
		$this->load->model('PurchaseModel');
		$this->load->model('ProviderModel');
		$this->load->model('ItemModel');
		$this->load->model('CurrencyModel');
		$this->load->model('UnitModel');
		$this->load->model('PurchaseDocumentTypeModel');
		$this->load->model('PurchaseModel');
		$this->load->model('EntrepriseInformationModel');
		$this->load->model("TVAModel");
	}
	public function index($idPurchaseDocument = -1)
	{
		$data = array();
		$result = array();
		$view = 'Purchase/purchase_document';
		$id = $idPurchaseDocument;
		if ($id != -1) {
			$PurchaseDocument = $this->PurchaseDocumentModel->GetById($id);
			$view = 'Purchase/base';
			$result["PurchaseDocument"] = $PurchaseDocument;
			$result["Provider"] = $this->ProviderModel->GetById($PurchaseDocument['idProvider']);
			$result["EntrepriseInformation"] = $this->EntrepriseInformationModel->GetList();
			$result["DocumentItems"] = $this->PurchaseDocumentItemModel->GetItemsById($id);
			$result['PurchaseDocumentType'] = $this->PurchaseDocumentTypeModel->GetById($result['PurchaseDocument']['idPurchaseDocumentType']);

			$result['TVA'] = $this->TVAModel->CurrentTVA();
		}
		//Filtre
		$idPurchase = isset($_GET['idPurchase']) ? $_GET['idPurchase'] : -1;
		$idProvider = isset($_GET['idProvider']) ? $_GET['idProvider'] : -1;
		$idTypeDocument = isset($_GET['idTypeDocument']) ? $_GET['idTypeDocument'] : -1;
		$documentDate = isset($_GET['DocumentDate']) ? $_GET['DocumentDate'] : "";

		$data['PurchaseDocumentType'] = $this->PurchaseDocumentModel->GetPurchaseDocumentTypeList();
		$data['Providers'] = $this->ProviderModel->GetList();
		$data['Purchasess'] = $this->PurchaseModel->GetList();
		$data['PurchaseDocument'] = $this->PurchaseDocumentModel->get_PurchaseDocumentById($idPurchase, $idProvider, $idTypeDocument, $documentDate);
		if (count($result) != 0) {
			$data['result'] = $result;
		}
		if ($id == -1) {
			require_once APPPATH . 'views/header.php';
		}
		$this->load->view($view, $data);
	}

	public function Form()
	{
		$PurchaseDocumentType = $this->PurchaseDocumentModel->GetPurchaseDocumentTypeList();
		$Providers = $this->ProviderModel->GetList();
		$Items = $this->ItemModel->GetList();
		$CurrencyList = $this->CurrencyModel->GetList();
		$UnitList = $this->UnitModel->GetList();
		$Purchases = $this->PurchaseModel->GetList();

		$data = array();
		$input = $this->input->get();
		if (isset($input['error'])) $data['error'] = $input['error'];
		$data['PurchaseDocumentType'] = $PurchaseDocumentType;
		$data['Providers'] = $Providers;
		$data['CurrencyList'] = $CurrencyList;
		$data['Items'] = $Items;
		$data['UnitList'] = $UnitList;
		$data['Purchases'] = $Purchases;
		$data['TVA'] = $this->TVAModel->CurrentTVA();

		require_once APPPATH . 'views/header.php';
		$this->load->view('Purchase/purchase_document_new', $data);
	}
	public function Insert()
	{
		try {
			$datas = $this->input->post();
			$PurchaseDocument = array();
			$PurchaseDocument["idPurchaseDocumentType"] = $datas["idPurchaseDocumentType"];
			$PurchaseDocument["idProvider"] = $datas["idProvider"];
			$PurchaseDocument["DocumentDate"] = $datas["DocumentDate"];
			$PurchaseDocument["GlobalDiscount"] = $datas["GlobalDiscount"];
			$PurchaseDocument["idCurrency"] = $datas["idCurrency"];
			$PurchaseDocument["idPurchase"] = $datas["idPurchase"];


			$NbItems = $datas["NbItems"];
			$iItem = 0;
			for ($i = 0; $i < $NbItems; $i++) {
				if (!isset($datas["idItem_" . $i])) continue;
				$PurchaseDocument['Items'][$iItem] = array();
				$PurchaseDocument['Items'][$iItem]["idItem"] = $datas["idItem_" . $i];
				$PurchaseDocument['Items'][$iItem]["Quantity"] = $datas["Quantity_" . $i];
				$PurchaseDocument['Items'][$iItem]["idUnit"] = $datas["idUnit_" . $i];
				$PurchaseDocument['Items'][$iItem]["UnitPrice"] = $datas["UnitPrice_" . $i];
				$PurchaseDocument['Items'][$iItem]["Reduction"] = $datas["Reduction_" . $i];
				$iItem++;
			}
			$this->PurchaseDocumentModel->Insert($PurchaseDocument);
			header("location:" . site_url('/PurchaseDocument'));
		} catch (Exception $e) {
			$error = $e->getMessage();
			header("location:" . site_url('/PurchaseDocument/Form?error=' . $error));
		}
	}
}
