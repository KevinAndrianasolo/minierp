<?php
defined('BASEPATH') or exit('No direct script access allowed');
require APPPATH . 'controllers/BaseController.php';
class Customer extends BaseController
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model("CustomerModel");
		$this->load->helper('url_helper');
	}
	public function index()
	{
		$this->List();
	}
	public function List()
	{
		$data = array();
		$data['customer'] = $this->CustomerModel->GetList();
		$this->load->view('Customer/customer_list', $data);
	}
	public function ListeID()
	{
		$this->load->helper('url_helper');
		$this->load->model('CustomerModel');
		$input = $this->input->get();
		$Id = $input["idCustomer"];
		$data = array();
		$data['customer'] = $this->CustomerModel->GetById($Id);
		$this->load->view('Customer/customer', $data);
	}

	public function Add_Action()
	{
		$input = $this->input->post();
		$this->load->helper('url_helper');
		$this->load->model('CustomerModel');
		$Name = $input["Name"];
		$Address = $input["Address"];
		$Phone = $input['Phone'];
		$Email = $input["Email"];
		$data['customer'] = $this->CustomerModel->Insert($Name, $Address, $Phone, $Email);
		redirect("/Customer");
	}

	public function Update_Action()
	{
		$input = $this->input->post();
		$idCustomer = $input['idCustomer'];
		$Name = $input["Name"];
		$Address = $input["Address"];
		$Email = $input["Email"];
		$Phone = $input["Phone"];
		$data['customer'] = $this->CustomerModel->Update($idCustomer, $Name, $Address, $Phone, $Email);
		redirect('/Customer');
	}
	public function Update($id = "")
	{
		$customer = $this->CustomerModel->GetById($id);
		$data['customer'] = $customer;
		$this->load->view('Customer/customer_form', $data);
	}
	public function Add()
	{
		$this->load->view('Customer/customer_form');
	}
	public function Delete($id = "")
	{
		$this->CustomerModel->Delete($id);
		$this->List();
	}
}
