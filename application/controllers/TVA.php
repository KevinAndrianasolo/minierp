<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH.'controllers/BaseController.php';

class TVA extends BaseController {
	public function __construct(){
		parent::__construct();
		$this->load->model("TVAModel");
	}
	public function index()
	{
        $data = array();
        $input = $this->input->get();
        if(isset($input['error'])) $data['error'] = $input['error'];
        $this->load->view('TVA/InsertTVA', $data);
    }
    public function InsertTVA(){
        $data = array();
        try{
            $TVA = $this->input->post();
            $this->TVAModel->Insert($TVA);
            redirect('TVA');
        }
        catch(Exception $e){
            $data['error'] = $e->getMessage();
            $this->load->view('TVA/InsertTVA', $data);
        }
    }
}
