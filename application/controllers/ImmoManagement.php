<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH.'controllers/BaseController.php';
require APPPATH.'classes/AmmortissementDegressif.php';
require APPPATH.'classes/AmmortissementLineaire.php';

require_once APPPATH.'classes/Periodicity.php';

class ImmoManagement extends BaseController {
	public function __construct(){
		parent::__construct();

		date_default_timezone_set('Indian/Antananarivo');
	}
	public function Index(){
		require_once APPPATH . 'views/header.php';
		$this->load->view('Immo/ImmoManagement.php');
	}
	public function Degressif()
	{
		$input = $this->input->get();
		$data = array();
		$data['Periodicity'] = new Periodicity();
		try{
			if(isset($input['init'])){
				$ValeurInitiale = $input['ValeurInitiale'];
				$DureeUtilisation = $input['DureeUtilisation'];
				$DatePremiereUtilisation = new DateTime( $input['DatePremiereUtilisation']);
				$Coefficient =  $input['Coefficient'];
				$Periodicity =$input['Periodicity'];
				/*
				$ValeurInitiale = 100000;
				$DureeUtilisation = 5;
				$DatePremiereUtilisation = new DateTime("2021-02-15");
				$Coefficient = 1.75;
				$PERIODICITY=  new Periodicity();
				$Periodicity =$PERIODICITY->MENSUEL; // ANNUEL, MENSUEL, JOURNALIER
		*/
				$AmmortissementDegressif = new AmmortissementDegressif($this->db, $ValeurInitiale, $DureeUtilisation, $DatePremiereUtilisation, $Coefficient, $Periodicity);
				$Ammortissements = $AmmortissementDegressif->Ammortissements();
				//var_dump($Ammortissements);
				
				$data['Ammortissements'] = $Ammortissements;
				$data['input'] = $input;
				
			}
			require_once APPPATH . 'views/header.php';
			$this->load->view('Immo/ImmoManagementDegressif.php', $data);
		}
		catch(Exception $e){
			$data['err_message'] = $e->getMessage();
			$this->load->view('Immo/ImmoManagementDegressif.php', $data);
		}
		
	}
	public function Lineaire()
	{
		$input = $this->input->get();
		$data = array();
		$data['Periodicity'] = new Periodicity();
		try{
			if(isset($input['init'])){
				$ValeurInitiale = $input['ValeurInitiale'];
				$DureeUtilisation = $input['DureeUtilisation'];
				$DatePremiereUtilisation = new DateTime( $input['DatePremiereUtilisation']);
				$Periodicity =$input['Periodicity'];
				/*
				$ValeurInitiale = 100000;
				$DureeUtilisation = 5;
				$DatePremiereUtilisation = new DateTime("2021-01-01");
				$PERIODICITY=  new Periodicity();
				$Periodicity =$PERIODICITY->ANNUEL; // ANNUEL, MENSUEL, JOURNALIER
		*/
				$AmmortissementLineaire = new AmmortissementLineaire($this->db, $ValeurInitiale, $DureeUtilisation, $DatePremiereUtilisation, $Periodicity);
				
				$Ammortissements = $AmmortissementLineaire->Ammortissements();
				//var_dump($Ammortissements);
				
				$data['Ammortissements'] = $Ammortissements;
				$data['input'] = $input;
				
			}
			require_once APPPATH . 'views/header.php';
			$this->load->view('Immo/ImmoManagementLineaire.php', $data);
		}
		catch(Exception $e){
			throw $e;
			$data['err_message'] = $e->getMessage();
			$this->load->view('Immo/ImmoManagementLineaire.php', $data);
		}
		
	}
}
