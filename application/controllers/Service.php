<?php
defined('BASEPATH') or exit('No direct script access allowed');
require APPPATH . 'controllers/BaseController.php';
class Service extends BaseController
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('ServiceModel');
	
	}
    public function index()
    {
        $data = array();
        $data['Service'] = $this->ServiceModel->GetList();
        $this->load->view('Service/service_list', $data);
    }
    public function Service_Form($id = "")
    {
        $data = array();
        $data['Service'] = $this->ServiceModel->GetById($id);
        
        $this->load->view('Service/service_form', $data);
    }
    public function update()
    {
        $input = $this->input->post();
        $idService = $input['idService'];
        $Name = $input["Name"];
		$this->ServiceModel->Update($Name, $idService);
        redirect('/Service');
    }
    public function add_form()
    {
        $input = $this->input->post();
        $Name = $input["Name"];
        $this->ServiceModel->insert($Name);
        redirect('/Service');
    }
    
    public function add()
    {
        
        $this->load->view('Service/service_form');
    }
    public function remove($id = "")
    {
        $this->ServiceModel->Delete($id);
        redirect('/Service');
    }
}