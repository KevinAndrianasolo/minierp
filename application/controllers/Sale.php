<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH.'controllers/BaseController.php';

class Sale extends BaseController {
	public function __construct(){
		parent::__construct();
		$this->load->model("SaleModel");
		$this->load->model("ItemModel");
		$this->load->model("SaleStatusModel");
	}
	public function index()
	{
		$saleDate = isset($_GET['SaleDate'])? $_GET['SaleDate'] : "";
		$idStatus = isset($_GET['idStatus'])? $_GET['idStatus'] : -1;

		$SaleStatus = $this->SaleStatusModel->GetList();
		$Sales = $this->SaleModel->get_saleById($idStatus,$saleDate);
		$data = array();
		$data["SaleStatus"] = $SaleStatus;
		$data["Sales"] = $Sales;

		require_once APPPATH . 'views/header.php';
		$this->load->view('Sale/Sale',$data);
	}
	public function Insert(){
		try{
			$datas = $this->input->post();
			var_dump($datas);
			$Sale = array();
			$Sale["SaleDate"] = $datas["SaleDate"];

			$NbItems = $datas["NbItems"];
			$Sale['Items'] = array();
			$iItem = 0;
			for($i=0;$i<$NbItems;$i++){
				if(!isset($datas["idItem_".$i])) continue;
				$Sale['Items'][$iItem] = array();
				$Sale['Items'][$iItem]["idItem"] = $datas["idItem_".$i];
				$Sale['Items'][$iItem]["Quantity"] = $datas["Quantity_".$i];
				$iItem ++;
			}
			$this->SaleModel->Insert($Sale);
			header("location:".site_url('/Sale'));
		}
		catch(Exception $e){
			$error = $e->getMessage();
			header("location:" . site_url('/Sale/Form?error='.$error));
		}

	}	
	public function Form(){
		
		$Items = $this->ItemModel->GetList();
		$data = array();
		$input = $this->input->get();
        if(isset($input['error'])) $data['error'] = $input['error'];
        
		$data["Items"] = $Items;
		require_once APPPATH . 'views/header.php';
		$this->load->view('Sale/Sale_new',$data);
	}
		
}
